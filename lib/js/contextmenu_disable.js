// Отключение контекстного меню для картинок
    ;(function(){
        function disablecontext(e) {
            var clickedEl = (e==null) ? event.srcElement.tagName : e.target.tagName;
            if (clickedEl == "IMG") {
                alert(errorMsg);
                return false;
            }
        }
        var errorMsg = "Вы не можете сохранять изображения с этого сайта.";
        document.oncontextmenu = disablecontext;

        document.addEventListener("contextmenu", (event)=>{
            if(event.target.tagName == 'IMG'){
                event.preventDefault();
                return false;
            }
        });
    }());