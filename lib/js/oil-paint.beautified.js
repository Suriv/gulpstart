/* Prod. Kenan Aivazov. vk.com/kenan_aivazov */
function OilPainting() {
    var a, t, e, i, n = {
        x: window.innerWidth / 2,
        y: window.innerHeight / 2
    }, r = {
        x: window.innerWidth / 2,
        y: 0
    }, o = {
        x: 0,
        y: 0
    }, s = "#" + Math.floor(16777215 * Math.random()).toString(16);//color
    console.log("#" + Math.floor(16777215 * Math.random()))
    console.log("#" + Math.floor(16777215 * Math.random()).toString(16))
    console.log("#" + Math.floor(16777215).toString(8))
    this.initialize = function() {
        a = document.getElementById("canvas"), t = a.getContext("2d"), e = window.innerWidth,
        i = window.innerHeight, a.width = e, a.height = i, a.addEventListener("mousemove", l, !1),
        a.addEventListener("click", d, !1), a.addEventListener("dblclick", c, !1);
    };
    var l = function(a) {
        var e = Math.sqrt(Math.pow(r.x - n.x, 2) + Math.pow(r.y - n.y, 2)), i = 10 * e * (Math.pow(Math.random(), 2) - .5), l = Math.random() - .5, d = 15 * Math.random() / e;
        o.x = (r.x - n.x) * Math.sin(.5) + n.x, o.y = (r.y - n.y) * Math.cos(.5) + n.y,
        n.x = r.x, n.y = r.y, r.x = a.layerX, r.y = a.layerY;
        var c = (Math.random() + 2 - .5) * d + (1 - Math.random() + 1.5 - .5) * d;
        t.lineWidth = c, t.strokeWidth = c, t.lineCap = "round", t.lineJoin = "round", t.beginPath(),
        t.moveTo(n.x, n.y), t.quadraticCurveTo(o.x, o.y, r.x, r.y), t.fillStyle = s, t.strokeStyle = s,
        t.moveTo(n.x + i, n.y + i), t.lineTo(n.x + l + i, n.y + l + i), t.stroke(), t.fill(),
        t.closePath();
    }, d = function(a) {
        a.preventDefault(), s = "#" + Math.floor(16777215 * Math.random()).toString(16),
        t.fillStyle = s, t.strokeStyle = s;
    }, c = function(a) {
        a.preventDefault(), t.clearRect(0, 0, e, i);
    };
}

var app = new OilPainting();

app.initialize(11);
