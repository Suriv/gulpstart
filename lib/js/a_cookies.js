(function() {
    // Cookie close
    // возвращает cookie с именем name, если есть, если нет, то undefined
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 3600);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }
        document.cookie = updatedCookie;
    }

    if ( getCookie("color") != undefined ) {
        document.documentElement.classList.add("color_" + getCookie("color"));
    } else {
        setCookie("color", "white");
        document.documentElement.classList.add("color_" + getCookie("color"));
    }
    if ( getCookie("fontsize") != undefined ) {
        document.documentElement.classList.add("fontsize_" + getCookie("fontsize"));
    } else {
        setCookie("fontsize", "normal");
        document.documentElement.classList.add("fontsize_" + getCookie("fontsize"));
    }
    if ( getCookie("img") != undefined ) {
        document.documentElement.classList.add("img_" + getCookie("img"));
    } else {
        setCookie("img", "on");
        document.documentElement.classList.add("img_" + getCookie("img"));
    }

    document.addEventListener("DOMContentLoaded", function(){
        var cookieClose = document.querySelectorAll(".js-cookie__close");
        if(cookieClose.length > 0){
            [].forEach.call(cookieClose, function(btn){
                btn.onclick = function(e){
                    document.querySelector(".g-cookie").classList.add("g-cookie_hide");
                    setCookie("cookiePane", "yes");
                };
            });
            // setCookie("cookiePane", "");
            // console.log(document.cookie);
            if ( getCookie("cookiePane") == undefined || getCookie("cookiePane") == "" ) {
                document.querySelector(".g-cookie").classList.add("g-cookie_open");
            }
        }


        var accessBtns = document.querySelectorAll(".js-access_btn");
        if(accessBtns.length > 0){
            [].forEach.call(accessBtns, function(btn){
                btn.onclick = function(e){
                    e.preventDefault();
                    var _rel = btn.rel;
                    if (_rel.split("_")[0] == "color") {
                        document.documentElement.classList.remove("color_black", "color_blue", "color_white");
                        document.documentElement.classList.add(_rel);
                        setCookie("color", _rel.split("_")[1]);
                    }
                    if (_rel.split("_")[0] == "fontsize") {
                        document.documentElement.classList.remove("fontsize_small", "fontsize_normal", "fontsize_big");
                        document.documentElement.classList.add(_rel);
                        setCookie("fontsize", _rel.split("_")[1]);
                    }
                    if (_rel.split("_")[0] == "img") {
                        if ( getCookie("img") == "off" ) {
                            setCookie("img", "on");
                            document.documentElement.classList.remove(_rel);
                            document.documentElement.classList.add("img_on");
                        } else {
                            setCookie("img", _rel.split("_")[1]);
                            document.documentElement.classList.remove("img_on");
                            document.documentElement.classList.add(_rel);
                        }
                    }
                };
            });
        }
    });
    // cookie close

})(window, document);
