// Swiper slider initial >>>>>>>>
;(function () {
    // client slider
        var clientSlider = document.querySelector('.g-client__slider');
        if (clientSlider != undefined) {
            var clientPrev = document.querySelector('.g-client__slider-btn_prev');
            var clientNext = document.querySelector('.g-client__slider-btn_next');
            var mySwiper = new Swiper(clientSlider, {
                slidesPerView: 5,
                slidesPerGroup: 5,
                spaceBetween: 0,
                watchSlidesVisibility: true,
                touchEventsTarget: "wrapper",
                simulateTouch: false,
                // Disable preloading of all images
                preloadImages: false,
                // Enable lazy loading
                lazy: {
                    loadPrevNext: true,
                },
                navigation:{
                    nextEl: clientNext,
                    prevEl: clientPrev
                },
                breakpoints: {
                // when window width is <= *px
                    434: {
                      slidesPerView: 1,
                      slidesPerGroup: 1
                    },
                    600: {
                      slidesPerView: 2,
                      slidesPerGroup: 2
                    },
                    700: {
                      slidesPerView: 3,
                      slidesPerGroup: 3
                    },
                    850: {
                      slidesPerView: 4,
                      slidesPerGroup: 4
                    }
                }
            });
            if (mySwiper.slides.length <=1 ) {
                mySwiper.navigation.nextEl.style.display = "none";
                mySwiper.navigation.prevEl.style.display = "none";
            }
        }

    // service cabinet slider
        var serviceCabSlider = document.querySelectorAll('.service-cab-slider');
        if (serviceCabSlider.length) {
            [].forEach.call(serviceCabSlider, function(slider) {
                var prev = slider.querySelector('.service-cab-slider__btn_prev');
                var next = slider.querySelector('.service-cab-slider__btn_next');
                var pag = slider.querySelector('.swiper-pagination');
                var mySwiper = new Swiper(slider, {
                    spaceBetween: 0,
                    // Disable preloading of all images
                    // preloadImages: false,
                    // lazyLoading: true,
                    touchEventsTarget: "wrapper",
                    simulateTouch: false,
                    lazy: {
                        loadPrevNext: true
                    },
                    navigation:{
                        nextEl: next,
                        prevEl: prev
                    },
                    pagination: {
                        el: pag,
                        clickable: true
                    },
                    autoHeight: true,
                    on: {
                        init: function () {
                        },
                        lazyImageLoad: function () {
                        },
                        lazyImageReady: function () {
                            var h = mySwiper.slides[mySwiper.realIndex].clientHeight;
                            mySwiper.wrapperEl.style.height = h + "px";
                        }
                    }
                });
                if (mySwiper.slides.length <= 1 ){
                    mySwiper.pagination.el.style.display = "none";
                } else mySwiper.pagination.el.style.display = "block";
                // document.querySelectorAll('.service-cab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                //     var $target = $(e.target)
                //     mySwiper.update();
                //     if (mySwiper.slides.length <= 1 ){
                //         mySwiper.pagination.el.style.display = "none"
                //     } else mySwiper.pagination.el.style.display = "block"
                // });
            });
        }

    // home slider
        var homeSlider = document.querySelector('.home-slider');
        if (homeSlider != undefined) {
            var prev = homeSlider.querySelector('.home-slider__nav_prev');
            var next = homeSlider.querySelector('.home-slider__nav_next');
            var mySwiper = new Swiper(homeSlider, {
                spaceBetween: 20,
                slidesPerView: 2,
                // Disable preloading of all images
                // preloadImages: false,
                loop: true,
                touchEventsTarget: "wrapper",
                simulateTouch: false,
                lazy: {
                    loadPrevNext: true
                },
                navigation:{
                    nextEl: next,
                    prevEl: prev
                }
            });
            if (mySwiper.activeIndex <=1 ) {
                mySwiper.destroy(false);
                mySwiper.navigation.nextEl.style.display = "none";
                mySwiper.navigation.prevEl.style.display = "none";
                mySwiper.params.loop = false;
                mySwiper.update();
            }
        }

}());
