;(function() {
	SCROLLBAR_Width = ""; //glob

	var $modal  = $(".modal"),
      $html   = $("html");

	// determined scroll width
	;(function() {
	  var scrollDiv = document.createElement('div'),
	  		$body = $(document.body);
	  scrollDiv.className = 'modal-scrollbar-measure';
	  $body.append(scrollDiv);
	  SCROLLBAR_Width = scrollDiv.offsetWidth - scrollDiv.clientWidth;
	  $body[0].removeChild(scrollDiv);
	  return SCROLLBAR_Width;
	}());
	// /* determined scroll width

	// indent for fix block
	$modal.on('show.bs.modal', function(){
		var $navFix = $(".nav.fixed");
		$navFix.css({
			right: SCROLLBAR_Width
		});
	});
	$modal.on('hidden.bs.modal', function(){
		var $navFix = $(".nav.fixed");
		$navFix.css({
			right: ""
		});
	});
	// /* indent for fix block
}(this.jQuery || this.$, this));