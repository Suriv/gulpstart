/*
 * aa-form-verification.js
 *
 * Верификация форм
 *
 * зависимости:
 *    /js/jquery.validate.js
 *    /js/localization/messages_ru.min.js
 *
 */

$(function(){
	// Привязка к кнопкам отправки форм, у которых нет собственного обработчика
	// onclick() стандартного обработчика плагина jquery.form
	// $("input[type='submit']:not([onclick]), button[type='submit']:not([onclick])").on("click", function(){
	// 	$(this).closest('form').submit();
	// 	return false;
	// });

	// Установка дефолтовых значений валидатора
	$.validator.setDefaults({
		// debug: true,
		ignore: [],
		focusInvalid: true,
		errorPlacement: function(error, element) {
			if ( element.parent(".g-form-item__field").length > 0 ){
	    	error.appendTo( element.parent(".g-form-item__field") );
			} else if ( element.parent("label").length > 0 ){
	    	error.appendTo( element.closest(".g-form-item__field") );
			} else {
				error.insertAfter( element );
			}
	  }
	});

	// метод для года с маской
	$.validator.addMethod("required_mask_date", function (value, element) {
		var arr = value.split("."),
				year = new Date().getFullYear();
			if(year === "" || year == "undefined" ||  year == "null" ){
		  	return true;
			} else if( (year + 1) - arr[2] < 0  || arr[2] - (year - 1) < 0 ){
		  	return false;
			}
		  return true;
	},
	"Заполните год правильно");
	//** метод для года с маской **/
	//
	//// метод для телефона с маской
	$.validator.addMethod("required_mask_phone", function (value, element) {
		if (value != "") {
	  	return value.replace(/\D+/g, '').length == element.getAttribute("data-mask-phone").replace(/\D+/g, '').length;
		} else return true;
	},
	"Заполните это поле полностью");
	//** метод для телефона с маской **/
    $.validator.addMethod("phoneRUS", function(phone_number, element) {
        phone_number = phone_number.replace(/\D+/g, '');
        console.log(phone_number);
        return this.optional(element) || phone_number.length > 9 ;
    }, "Пожалуйста, заполните поле корректно");
    // $.validator.addMethod("phoneRUS", function(phone_number, element) {
    //     phone_number = phone_number.replace(/\s+/g, "");
    //     return this.optional(element) || phone_number.length > 9 &&
    //     phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    // }, "Пожалуйста, заполните поле корректно");
    //// метод для телефона с маской
    // $.validator.addMethod("required_mask_phone", function (value, element) {
    //  if (value != "") {
    //      return value.replace(/\D+/g, '').length == element.getAttribute("data-mask-phone").replace(/\D+/g, '').length;
    //  } else return true;
    // },
    // "Заполните это поле полностью");([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})
    //** метод для телефона с маской **/

	// Валидация формы "*"
	$(".js-form_*").validate({
		rules: {
			name: {
				required: true,
				minlength: 2
			},
			phone: {
				required: true,
				required_mask_phone: true
			},
			date: {
				required: true,
				required_mask_date: true
			},
			mail: {
				required: true,
				email: true
			},
			one_from_group: {
				require_from_group: [1, ".js-form_feedback-group"],
				required_phone_mask: true
			},
			one_from_group: {
				require_from_group: [1, ".js-form_feedback-group"],
				email: true
			},
			one_from_group: {
				require_from_group: [1, ".js-form_feedback-group"],
				minlength: 2
			},
			text: {
				required: true,
				minlength: 2
			},
            form_agree: {
                required: true
            },
            "hiddenRecaptcha": {
               required: function() {
                   if(grecaptcha.getResponse() == '') {
                       return true;
                   } else {
                       return false;
                   }
               }
            }
		}
	});

	$('.checkbox__input').each( function(){
		$(this).rules('add', {
	  	required: true,
	  	messages: {
	  		required: "Одно или несколько полей должно быть выбрано"
	  	}
		});
	});
	$('.radio__input').each( function(){
		$(this).rules('add', {
	  	required: true,
	  	messages: {
	  		required: "Одно из этих полей должно быть выбрано"
	  	}
		});
	});

  // welldone message
  $(".js-welldone__btn").on("click", function(){
    var $this = $(this),
    		$form = $this.closest("form"),
    		$modal = $this.closest(".modal"),
    		$header = $modal.find(".modal-header"),
    		$body = $modal.find(".modal-body"),
    		$answer = $modal.find(".modal-answer");
    if( $form.valid() ){
	    $header.hide();
	    $body.hide();
	    $answer.show();
	    setTimeout(function(){
	    	$modal.modal('hide');
	    }, 3000);
	    $modal.on("hidden.bs.modal", function(){
	    	$header.show();
	    	$body.show();
	    	$answer.hide();
	    });
	  	return false;
    }
  });// /*  welldone message

});
