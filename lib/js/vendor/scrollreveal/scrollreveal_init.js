(function(window, document, undefined) {
  var nav    = document.getElementById('nav'),
      header  = document.getElementById('header');

  if ( typeof ScrollReveal != undefined ){

    if( window.innerWidth > 767){

      window.sr = ScrollReveal({
        duration: 1000,
        origin: 'left',
        distance: '10em',
        viewFactor: 0.7,
        mobile: false,
        viewOffset: { top: 56 }
      });

      // Add class to <html> if ScrollReveal is supported
      // Note: this method is deprecated, and only works in version 3
      if (sr.isSupported()) {
        document.documentElement.classList.add('sr');
      }
      //
      // Header bloc
        sr.reveal(".header-logo .js-sr-item", {
          origin: 'top',
          distance: '100%',
          scale: "1",
          viewFactor: 0.2,
          viewOffset: { top: 0 }
        },150);
        sr.reveal(".header-top .js-sr-item", {
          origin: 'right',
          distance: '100%',
          scale: "1",
          viewFactor: 0.2,
          viewOffset: { top: 0 }
        });
        sr.reveal(".header-bottom__right .js-sr-item", {
          origin: 'right',
          distance: '100%',
          scale: "1",
          viewOffset: { top: 0 }
        },100);
      // * Header
      //
      // Navigation bloc
        sr.reveal(".nav .js-sr-item", {
          delay: 300,
          origin: 'top',
          distance: '100%',
          scale: "1",
          viewFactor: 0.1,
          viewOffset: { top: 0 }
        },100);
      // * Navigation
      //
      // Costing block
        sr.reveal(".costing-text__title", {
          delay: 300,
          origin: 'left',
          distance: '30em'
        });
        sr.reveal(".costing-text__desc", {
          delay: 500,
          origin: 'left',
          distance: '30em'
        });
        sr.reveal(".costing-text__desc-dop", {
          delay: 1000,
          origin: 'left',
          distance: '30em'
        });
        sr.reveal(".costing-form", {
          origin: 'right',
          distance: '50em'
        });
      // *Costing block
      //
      // Dignity block
        sr.reveal(".dignity-el", {
          origin: 'bottom',
          distance: '2em'
        });
      // * Dignity block
      //
      // Tabs text block
        sr.reveal(".tab-text", {
          distance: '50em'
        });
      // * Tabs text block
      //
      // Kit bloc
        sr.reveal(".kit__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".kit__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".tab-content__pane .goods-el", {
          origin: 'bottom',
          distance: '0',
          scale: "0.2",
          viewFactor: 0.6
        }, 100);
      // * Kit bloc
      //
      // Info bloc
        sr.reveal(".info__desc", {
          origin: 'bottom',
          distance: '1em'
        });
        sr.reveal(".info__ico-cont.abs_l", {
          origin: 'left',
          distance: '3em'
        });
        sr.reveal(".info__ico-cont.abs_r", {
          origin: 'right',
          distance: '3em'
        });
        sr.reveal(".info__btn", {
          origin: 'right',
          distance: '10em'
        });
      // * Info bloc
      //
      // Implemented bloc
        function count(val,el,timeout,step) {
          var i=0;
          (function(){
            if(i<=val){
            setTimeout(arguments.callee,timeout);
            document.getElementById(el).innerHTML=i;
            i=i+step;
            }else{
              document.getElementById(el).innerHTML=val;
            }
          })();
        }
        sr.reveal(".implemented__in", {
          origin: 'left',
          distance: '50%',
          beforeReveal: function (domEl) {
            var implEl = document.getElementById("implemented_count");
                if (implEl) {
                  var implElVal = implEl.innerHTML,
                      implElvalCh = Math.ceil(implElVal/100),
                      valWidth = implEl.offsetWidth;
                      implEl.style.display = "block";
                      implEl.style.width = valWidth +"px";
                      implEl.innerHTML = 1;
                  count(implElVal,'implemented_count', 10, implElvalCh);
                }
          }
        });
      // * Implemented bloc
      //
      // Object bloc
        sr.reveal(".object__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".object__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".object-el", {
          origin: 'bottom',
          distance: '3em',
          scale: "1"
        }, 200);
      // * Object bloc
      //
      // Include bloc
        sr.reveal(".include__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".include__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".include-el", {
          origin: 'bottom',
          distance: '3em',
          scale: "1"
        }, 100);
      // * Include bloc
      //
      // Advantage bloc
        sr.reveal(".advantage__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".advantage__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".advantage-el", {
          origin: 'bottom',
          distance: '3em'
        }, 100);
      // * Advantage bloc
      //
      // Order bloc
        sr.reveal(".order-text .js-sr-item", {
          origin: 'left',
          distance: '50%'
        }, 200);
        sr.reveal(".order-form .js-sr-item", {
          delay: 500,
          origin: 'right',
          distance: '70%',
          scale: "1",
          afterReveal: function (domEl) {
            domEl.style = "";
            domEl.style.visibility = "visible";
          }
        }, 200);
      // * Order bloc
      //
      // Step bloc
        var step_el_odd = $(".step-el:odd .step-el__text"),
            step_el_even = $(".step-el:even .step-el__text");
        sr.reveal(".step__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".step__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".step-el .step-el__ico-cont", {
          origin: 'bottom',
          distance: '3em'
        });
        sr.reveal(".step-el .step-el__ico-cont-line", {
          delay: 400,
          origin: 'bottom',
          distance: '3em'
        });
        sr.reveal(step_el_even, {
          delay: 400,
          origin: 'right',
          distance: '5em'
        });
        sr.reveal(step_el_odd, {
          delay: 400,
          origin: 'left',
          distance: '5em'
        });
      // * Step bloc
      //
      // Help bloc
        sr.reveal(".help-img", {
          origin: 'left',
          distance: '0',
          scale: "0.1"
        });
        sr.reveal(".help-text .js-sr-item", {
          delay: 500,
          origin: 'right',
          distance: '70%',
          scale: "1"
        }, 200);
      // * Help
      //
      // Faq bloc
        sr.reveal(".faq .js-sr-item", {
          delay: 300,
          origin: 'bottom',
          distance: '70%',
          scale: "1"
        });
      // * Faq
      //
      // Store bloc
        sr.reveal(".store-img", {
          origin: 'left',
          distance: '0',
          scale: "0.1"
        });
        sr.reveal(".store-text .js-sr-item", {
          delay: 500,
          origin: 'right',
          distance: '70%',
          scale: "1"
        }, 200);
      // * Store
      //
      // Stage bloc
        sr.reveal(".stage__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".stage__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".stage__list-line", {
          origin: 'right',
          distance: '50%',
          scale: "1",
          viewFactor: 0.8
        });
        sr.reveal(".stage .js-sr-item", {
          delay: 500,
          origin: 'bottom',
          distance: '50%',
          scale: "0.5"
        }, 200);
      // * Stage block
      //
      // Contact bloc
        sr.reveal(".contact__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".contact__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".contact-map-data", {
          delay: 300,
          origin: 'left',
          distance: '10em',
          viewFactor: 0.6
        });
      // * Contact bloc
      //
      // Call bloc
        sr.reveal(".call-text__in", {
          origin: 'left',
          distance: '15em'
        });
        sr.reveal(".call-form .js-sr-item", {
          delay: 400,
          origin: 'right',
          distance: '100%',
          scale: "1"
        }, 150);
      // * Call bloc
      //
      // Footer bloc
        sr.reveal(".footer__in", {
          origin: 'bottom',
          distance: '100%',
          scale: "1",
          viewFactor: 0.8
        });
      // * Footer bloc
    } else {
      window.sr = ScrollReveal({
        duration: 1000,
        origin: 'left',
        distance: '5em',
        viewFactor: 0.4,
        mobile: false,
        viewOffset: { top: 56 }
      });

      // Add class to <html> if ScrollReveal is supported
      // Note: this method is deprecated, and only works in version 3
      if (sr.isSupported()) {
        document.documentElement.classList.add('sr');
      }
      //
      // Header bloc
        if (window.scrollY == 0) {
          sr.reveal(".header-logo .js-sr-item", {
            origin: 'top',
            distance: '100%',
            scale: "1",
            viewFactor: 0.2,
            viewOffset: { top: 0 }
          },150);
          sr.reveal(".header-burger", {
            origin: 'right',
            distance: '100%',
            scale: "1",
            viewOffset: { top: 0 }
          });
        }
      // * Header
      //
      // Navigation bloc
        sr.reveal(".nav .js-sr-item", {
          origin: 'left',
          distance: '100%',
          scale: "1"
        },150);
      // * Navigation
      //
      // Costing block
        sr.reveal(".costing-text__title", {
          origin: 'left',
          distance: '100%'
        });
        sr.reveal(".costing-text__desc", {
          delay: 300,
          origin: 'left',
          distance: '100%'
        });
        sr.reveal(".costing-text__desc-dop", {
          delay: 500,
          origin: 'left',
          distance: '100%'
        });
        sr.reveal(".costing-form", {
          origin: 'right',
          distance: '100%'
        });
      // *Costing block
      //
      // Dignity block
        sr.reveal(".dignity-el", {
          origin: 'bottom',
          distance: '2em'
        });
      // * Dignity block
      //
      // Tabs text block
        sr.reveal(".tab-text", {
          distance: '100%'
        });
      // * Tabs text block
      //
      // Kit bloc
        sr.reveal(".kit__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".kit__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%',
          viewFactor: 0.8
        });
        sr.reveal(".tab-content__pane .goods-el", {
          origin: 'bottom',
          distance: '2em',
          scale: "0.2",
          viewFactor: 0.2
        });
      // * Kit bloc
      //
      // Info bloc
        sr.reveal(".info__desc", {
          origin: 'bottom',
          distance: '1em'
        });
        sr.reveal(".info__ico-cont.abs_l", {
          origin: 'left',
          distance: '100%'
        });
        sr.reveal(".info__btn", {
          origin: 'right',
          distance: '100%'
        });
      // * Info bloc
      //
      // Include bloc
        sr.reveal(".include__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".include__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%',
          viewFactor: 0.8
        });
        sr.reveal(".include-el", {
          origin: 'bottom',
          distance: '3em',
          scale: "1"
        });
      // * Include bloc
      //
      // Object bloc
        sr.reveal(".object__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".object__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%',
          viewFactor: 0.8
        });
        sr.reveal(".object-el", {
          origin: 'bottom',
          distance: '3em',
          scale: "1"
        });
      // * Object bloc
      //
      // Implemented bloc
        function count(val,el,timeout,step) {
          var i=0;
          (function(){
            if(i<=val){
            setTimeout(arguments.callee,timeout);
            document.getElementById(el).innerHTML=i;
            i=i+step;
            }else{
              document.getElementById(el).innerHTML=val;
            }
          })();
        }
        sr.reveal(".implemented__in", {
          origin: 'left',
          distance: '50%',
          viewFactor: 0.8,
          beforeReveal: function (domEl) {
            var implEl = document.getElementById("implemented_count");
                if (implEl) {
                  var implElVal = implEl.innerHTML,
                      implElvalCh = Math.ceil(implElVal/100),
                      valWidth = implEl.offsetWidth;
                      implEl.style.display = "block";
                      implEl.style.width = valWidth +"px";
                      implEl.innerHTML = 1;
                  count(implElVal,'implemented_count', 10, implElvalCh);
                }
          }
        });
      // * Implemented bloc
      //
      // Advantage bloc
        sr.reveal(".advantage__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".advantage__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%'
        });
        sr.reveal(".advantage-el", {
          origin: 'bottom',
          distance: '3em'
        });
      // * Advantage bloc
      //
      // Order bloc
        sr.reveal(".order-text .js-sr-item", {
          origin: 'left',
          distance: '50%',
          scale: "1"
        }, 200);
        sr.reveal(".order-form .js-sr-item", {
          origin: 'right',
          distance: '70%',
          scale: "1",
          afterReveal: function (domEl) {
            domEl.style = "";
            domEl.style.visibility = "visible";
          }
        }, 200);
      // * Order bloc
      //
      // Step bloc
        sr.reveal(".step__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".step__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%',
          viewFactor: 0.8
        });
        sr.reveal(".step-el .step-el__ico-cont", {
          origin: 'bottom',
          distance: '3em'
        });
        sr.reveal(".step-el .step-el__ico-cont-line", {
          delay: 200,
          origin: 'bottom',
          distance: '3em'
        });
        sr.reveal(".step-el .step-el__text", {
          delay: 200,
          origin: 'right',
          distance: '5em'
        });
      // * Step bloc
      //
      // Help bloc
        sr.reveal(".help-img", {
          origin: 'left',
          distance: '0',
          scale: "0.1"
        });
        sr.reveal(".help-text .js-sr-item", {
          origin: 'right',
          distance: '70%',
          scale: "1"
        }, 200);
      // * Help
      //
      // Faq bloc
        sr.reveal(".faq .js-sr-item", {
          origin: 'bottom',
          distance: '70%',
          scale: "1"
        });
      // * Faq
      //
      // Store bloc
        sr.reveal(".store-img", {
          origin: 'bottom',
          distance: '0',
          scale: "0.1"
        });
        sr.reveal(".store-text .js-sr-item", {
          origin: 'right',
          distance: '70%',
          scale: "1"
        }, 200);
      // * Store
      //
      // Stage bloc
        sr.reveal(".stage__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".stage__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%',
          viewFactor: 0.8
        });
        sr.reveal(".stage__list-line", {
          origin: 'bottom',
          distance: '10%',
          scale: "0.2",
          viewFactor: 0.1
        });
        sr.reveal(".stage .js-sr-item", {
          origin: 'bottom',
          distance: '30%',
          scale: "1",
          viewFactor: 0.2
        });
      // * Stage block
      //
      // Contact bloc
        sr.reveal(".contact__title-txt", {
          origin: 'left',
          distance: '50%'
        });
        sr.reveal(".contact__title-line", {
          delay: 400,
          origin: 'bottom',
          distance: '50%',
          viewFactor: 0.8
        });
        sr.reveal(".contact-map-data", {
          origin: 'left',
          distance: '100%',
          scale: "1",
          viewFactor: 0.3
        });
      // * Contact bloc
      //
      // Call bloc
        sr.reveal(".call-text__in", {
          origin: 'left',
          distance: '15em',
          scale: "1"
        });
        sr.reveal(".call-form .js-sr-item", {
          origin: 'right',
          distance: '100%',
          scale: "1"
        }, 150);
      // * Call bloc
      //
      // Footer bloc
        sr.reveal(".footer__in", {
          origin: 'left',
          distance: '60%',
          scale: "1",
          viewFactor: 0.1
        });
      // * Footer bloc
    }

  } else document.documentElement.classList.add('no-sr');

})(window, document);