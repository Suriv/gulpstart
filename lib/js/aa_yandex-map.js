// yandex map in contact page initial >>>>>>>>
;(function() {
    var _mapO = 'js-contact-map';
    if (document.getElementById(_mapO)) {
        ymaps.ready(init);
    }

    function init() {
        // Создание экземпляра карты.
        var _mapID = _mapO,
            _mapCenter = myPoints[0].coords || [51.722274, 39.163854],
            myMap = new ymaps.Map(_mapID, {
                center: _mapCenter,
                zoom: 15,
                controls: ["zoomControl"]
            }, {
                minZoom: 3,
                maxZoom: 17,
                searchControlProvider: 'yandex#search',
                checkZoomRange: true,
                zoomMargin: 50
            });

        // Отключаем масштаб по скролу
        myMap.behaviors.disable("scrollZoom");

        for (var i = 0, l = myPoints.length; i < l; i++) {
            createMenuItem(myPoints[i]);
        }

        function createMenuItem (item) {
            var phone = (item.phone != undefined) ? item.phone.replace(/\s/g,"") : "";
            var placemark = new ymaps.Placemark(
                item.coords,
                {
                    id: item.idPlace,
                    balloonContent: ["<div class='map__marker'>" +
                                        "<div class='map__marker-title'>" + item.title + "</div>" +
                                        "<div class='map__marker-txt'>" + item.address + "</div>" +
                                        "<div class='map__marker-txt'><a href=\"tel:" + phone + "\">" + item.phone + "</a></div>" +
                                        "<div class='map__marker-txt'>" + item.time + "</div>" +
                                    "</div>"],
                    hintContent: item.title
                },
                {
                    // preset: 'islands#darkGreenDotIcon'
                    // balloonOffset: [3, -40],
                    // iconLayout: 'default#image',
                    // iconImageHref: "images/map__marker.png",
                    // iconImageSize: [66, 71],
                    // iconImageOffset: [-33, -50]
                }
            );
            if(myPoints.length > 1){
                placemark.events.add("click", function(e){
                    var coords = e.get('target').geometry.getCoordinates();
                    myMap.setCenter(coords, 15);
                });
            }
            // Добавляем маркеры на карту
            myMap.geoObjects.add(placemark);
        }
        // Выставляем масштаб карты чтобы были видны все группы.
        // if(myPoints.length > 1){
        //     myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true, zoomMargin: 50});
        // }
        //
        // смещение центра карты
        // if ( window.innerWidth > 768 ) {
        //     var pixelCenter = myMap.getGlobalPixelCenter(_mapCenter);
        //     pixelCenter = [
        //         pixelCenter[0] - 190,
        //         pixelCenter[1]
        //     ];
        //     var geoCenter = myMap.options.get('projection').fromGlobalPixels(pixelCenter, myMap.getZoom());
        //     myMap.setCenter(geoCenter);
        //     }
        // * смещение центра карты

        var head = document.head;
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = [
            '.map__marker{'
                + 'min-width: 150px;'
                + 'padding-right: 10px;}'
            + '.map__marker-title{'
                    + 'display: block;'
                    + 'font-size: 14px;'
                    + 'line-height: 1.4;'
                    + 'font-weight: 500;'
                    + 'margin-bottom: 3px;'
                    + 'margin-bottom: 0.3rem;}'
            + '.map__marker-txt{'
                    + 'display: block;'
                    + 'font-size: 13px;'
                    + 'line-height: 1.6;}'
            + '.map__marker-title:empty,'
                + '.map__marker-txt:empty,'
                + '.map__marker a:empty{'
                    + 'display: none;}'
        ];
        head.append(style);
    }
}());
