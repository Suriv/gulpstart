// Loading Begin  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
(function() {
    // polyfill remove
    if (!Element.prototype.remove) {
        Element.prototype.remove = function remove() {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }
    // * polyfill remove


// from http://www.sberry.me/articles/javascript-event-throttling-debouncing
  function throttle(fn, delay) {
    var allowSample = true;

    return function(e) {
      if (allowSample) {
        allowSample = false;
        setTimeout(function() { allowSample = true; }, delay);
        fn(e);
      }
    };
  }

    // check support for css properties
    function supportCSS(prop) {
        var yes = false; // по умолчанию ставим ложь
        var prop1 = prop.replace(prop[0], prop[0].toUpperCase());
        if ('Moz' + prop1 in document.body.style) {
            yes = true; // если FF поддерживает, то правда
        }
        if ('webkit' + prop1 in document.body.style) {
            yes = true; // если Webkit поддерживает, то правда
        }
        if ('ms' + prop1 in document.body.style) {
            yes = true; // если IE поддерживает, то правда
        }
        if (prop in document.body.style) {
            yes = true; // если поддерживает по умолчанию, то правда
        }
        return yes; // возращаем ответ
    }
    if (!(supportCSS('pointerEvents'))) {
        document.documentElement.classList.add('no-pointer_events');
    }
    if (!(supportCSS('transform'))) {
        document.documentElement.classList.add('no-transform');
    }
    // /* check support for css properties

    // check support for placeholder
    if (!("placeholder" in document.createElement('input'))) {
        var _root = document.documentElement;
        var _head = document.head;
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.async = true;
        script.src = "https://cdnjs.cloudflare.com/ajax/libs/placeholders/4.0.1/placeholders.min.js";
        _head.appendChild(script);
        _root.classList.add('no-placeholder');
    }
    // /* check support for placeholder

    // determined scroll width
    window.SCROLLBAR_Width = 0; // glob var
    ;(function() {
        var scrollDiv = document.createElement('div'),
            body = document.body;
        scrollDiv.style = 'position:absolute;top:-9999px;width:50px;height:50px;overflow:scroll;';
        body.append(scrollDiv);
        SCROLLBAR_Width = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        scrollDiv.remove(scrollDiv);
        return window.SCROLLBAR_Width;
    })();
    // /* determined scroll width

    // navigation for mobile
    if (document.getElementById("js-burger")) {
        document.getElementById("js-burger").addEventListener("click", function() {
            if ((window.innerWidth - document.body.clientWidth) != 0) {
                documentElement.style.paddingRight = SCROLLBAR_Width + "px";
            }
            if (documentElement.classList.contains("nav-open")) {
                documentElement.style.paddingRight = "";
                documentElement.classList.remove("is-lock");
                documentElement.classList.remove("nav-open");
            } else {
                documentElement.classList.add("is-lock");
                documentElement.classList.add("nav-open");
            }
        });
        var navClose = document.querySelectorAll(".js-nav-mob__close");
        if (navClose.length > 0) {
            [].forEach.call(navClose, function(item) {
                item.addEventListener("click", function() {
                    documentElement.style.paddingRight = "";
                    documentElement.classList.remove("is-lock");
                    documentElement.classList.remove("nav-open");
                });
            });
        }
    }
    // /* navigation for mobile

    // init for FastClick
    if ('addEventListener' in document) {
        document.addEventListener('DOMContentLoaded', function() {
            FastClick.attach(document.body);
        }, false);
    }
    // /* init for FastClick

    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

    // MIT license
    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                       || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                  timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

    // to top anim
    function toTop() {
        var aa_body = document.body,
            aa_height = isNaN(aa_body.innerHeight) ? aa_body.clientHeight : aa_body.innerHeight,
            time = (aa_height*(-0.02)).toFixed();
        window.scrollBy(0,time); // чем меньше значение (цифра -10), тем выше скорость перемещения
        if (window.pageYOffset > 0) {requestAnimationFrame(toTop);} // если значение прокрутки больше нуля, то функция повториться
    }
    if (document.querySelectorAll('.js-to-top').length > 0) {
        var trigger = document.querySelectorAll(".js-to-top");
        [].forEach.call(trigger, function(link){
            link.addEventListener('click', function(e) {
                e.preventDefault();
                toTop();
            }, false);
        });
    }
    function scrollY() { return window.pageYOffset || docElem.scrollTop; }

    var WindowHeight = window.innerHeight || document.documentElement.clientHeight;
    if(window.scrollY > WindowHeight) {
        document.querySelector('.js-to-top_btn').classList.add('active');
    }
    window.onscroll = function() {
        if(window.scrollY > WindowHeight) {
            document.querySelector('.js-to-top_btn').classList.add('active');
        }
        if(window.scrollY < WindowHeight) {
            document.querySelector('.js-to-top_btn').classList.remove('active');
        }
    };
    // /* to top anim

    // magnific gallery in object
    // $('.js-magnific-cont').each( function(){
    //  $(this).magnificPopup({
    //    delegate: '.js-magnific__trigger', // child items selector, by clicking on it popup will open
    //    type: 'image',
    //    gallery: {
    //        enabled: true,
    //        navigateByImgClick: true,
    //        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    //      },
    //      zoom: {
    //        enabled: true,
    //        duration: 300, // don't foget to change the duration also in CSS
    //        opener: function(element) {
    //          return element.closest(".js-magnific-cont").find('.js-magnific__preview');
    //        }
    //      }
    //  });
    // });

    // $('.js-magnific__trigger_media').magnificPopup({
    //  type: 'iframe',
    //  mainClass: 'mfp-fade',
    //  removalDelay: 160,
    //  preloader: false,
    //  fixedContentPos: false,
    //  iframe: {
    //      patterns: {
    //      youtube: {
    //        src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0' // URL that will be set as a source for iframe.
    //      }
    //    }
    //  }
    // });
    // / * magnific gallery

    // init jquery.inputmask.bundle
    // $("[data-mask-phone]").each( function(){
    //  var $this = $(this),
    //          $thisData = $this.data("mask-phone");
    //  $this.inputmask({"mask": $thisData}); //specifying options
    // });
    // /* init jquery.inputmask.bundle

    // nice-select init
    // $('select').niceSelect();
    // $("select").on("change", function(){
    //  $(this).trigger("focusout"); /*это нужно для валидации чтоб запустить проверку этого поля*/
    // });
    // /* nice-select init

    // dropdown menu is connected to the tab
    // $(".dropdown .nav-tabs-li__a").on("show.bs.tab", function() {
    //     var $this = $(this),
    //         name = $this.text();
    //     dropBtnTxt = $this.closest(".dropdown").find(".dropdown__btn-txt");
    //     dropBtnTxt.text(name);
    // });
    // /* dropdown menu is connected to the tab

    // обертка для table
    (function(){
        var tables = document.querySelectorAll(".editor table");
        [].forEach.call(tables, function(table) {
            var div = document.createElement("div");
            div.className = "table__cont";
            table.parentNode.insertBefore(div, table);
            div.appendChild(table);
        });
    })(window, document);
    // * обертка для table

    (function() {
        // форма инпут анимация
        function form() {
            if ( this.value != '') {
                this.classList.add("is-done");
            } else { this.classList.remove("is-done"); }
        }

        var fields = document.querySelectorAll(".js-input-item__field");
        [].forEach.call(fields, function(input){
            input.onchange = form;
        });

        // автосайз для textarea
        autosize(document.querySelectorAll('.js_autosize'));

    })(window, document);

    var buttons  = document.querySelectorAll(".g-btn");
    [].forEach.call(buttons, function (button){
        button.addEventListener("click", function once(){
            button.classList.add("catch_me");
            button.removeEventListener("click", once, false);
        }, false);
    });


})(window, document);
// Loading End >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
