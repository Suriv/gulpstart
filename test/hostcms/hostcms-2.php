<!DOCTYPE html>
<html       xmlns='http://www.w3.org/1999/xhtml' xml:lang='ru' lang='ru'>
<head>
    <title><?php Core_Page::instance()->showTitle();?></title>
    <meta name="google-site-verification" content="_-gcmUTzKtx9FpWBJdDIWj_Paz7aCZdki13N9WfW50M" />
    <meta name="keywords" content="<?php Core_Page::instance()->showKeywords();?>" />
    <meta name="description" content="<?php Core_Page::instance()->showDescription();?>" />
    <meta content="text/html; charset=<?php echo SITE_CODING;?>" http-equiv="content-type" />
    <meta name="yandex-verification" content="d06982381b06b9a4" />
    <meta name="yandex-verification" content="392365e8852b0975" />
    <meta name="yandex-verification" content="1e5ca8d7eef8b9ee" />
    <meta http-equiv='imagetoolbar' content='no'                                                                    />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"                                                    />
    <meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport" />
    <meta name = "format-detection" content = "telephone=no"        />
    <meta name = "format-detection"     content = "address=no"      />
    <meta http-equiv="x-rim-auto-match" content="none"              />
    <meta property="og:image" content='/assets/images/logo-200-vk.png'  />
    <meta name="apple-mobile-web-app-capable" content="yes"         />
    <link type='image/x-icon'           rel='icon'                  href='/assets/images/favicon/favicon.ico'   />
    <link type='image/x-icon'           rel='shortcut icon' href='/assets/images/favicon/favicon.ico'   />
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/images/favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon/favicon-16x16.png" />
    <link rel="manifest" href="/assets/images/favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <!-- errorception.com js bug tracker -->
    <!--script>
    (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
    c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
    c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
    _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
    (window,document,"script","5c36f4b1998a523571e3b6d4");
    </script-->

    <?
        $oConst = new Artatom_Informationsystem_Constants();
        $oObject = Core_Page::instance()->object;
        $oStructure = Core_Page::instance()->structure;
        $sPath = $oStructure->path;
        $bIsShopItem = $oObject instanceof Shop_Controller_Show && $oObject->item;

        Core_Page::instance()
            ->fileTimestamp(true)
            ->clearCss()
            ->css("/assets/css/style_diz.css")
            ->css("/assets/css/suggestions.css")
            ->css("/assets/css/aa-geo.css")
            ->css("/assets/css/aa-form-verification.css")
            ->showCss();

        Core_Page::instance()
            ->clearJs()
            //->js("/assets/js/lib/jquery.1.8.2.js")
            ->js("/assets/js/lib/jquery.1.8.2.min.js");

        if ($sPath == 'info')
        {
            Core_Page::instance()
                ->js("/assets/js/aa-delivery-info.js");
        }

        Core_Page::instance()
            ->showJs();
    ?>
    <!-- Отключаем вывод в консоль если не залогинены в админке -->
    <!--    <script type="text/javascript">
        $(function(){ <?
            if (!Core_Auth::logged())
            {
                // echo '$.disableConsole();';
            }?>
        });
    </script> -->
    <script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOYJLpvYhQ-KhRtDuMxCxRukjYT0XMXU&libraries=geometry,places"></script>

    <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-111101801-1', 'auto');
            ga('send', 'pageview', 'zakaz', 'event', 'korzina');
        </script>
    <!-- Google Analytics -->
    <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1071969036206554');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=1071969036206554&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    </head>

<body>

    <div id="menu" class="menu  js-box_popup">
        <div class="menu-wrapper">
            <div class="menu-cancel js-box_popup-cancel">
                <img class="menu-cancel__img" src="/assets/images/menu__cancel.svg" alt="cancel" />
                <img class="menu-cancel__img menu-cancel__img_h" src="/assets/images/menu__cancel_h.svg" alt="cancel" />
            </div>
            <?php
                // Меню сайта Markus
                $oSite = Core_Entity::factory('Site', CURRENT_SITE);
                $oStructureControllerShow = new Structure_Controller_Show($oSite);
                $oStructureControllerShow
                    ->xsl(Core_Entity::factory('Xsl')->getByName('Меню сайта Markus'))
                    ->showShopGroups(true)
                    ->level(2)
                    ->menu(4)
                    ->show();
            ?>
            <div class="menu-tel">
                <div class="menu-tel-el">
                    <img class="menu-tel-el__ico" src="/assets/images/menu__tel-ico.svg" alt="telephone">
                    <a href="tel:<?= preg_replace("/[^0-9]/", "", $oConst->find('Телефон сайта')->allowedTags()->value); ?>" class="menu-tel-el__txt js-tel"><?= $oConst->value; ?></a>
                </div>
            </div>
            <div class="menu-soc">
                <?php if ($oConst->allowedTags()->find('facebook')->loaded()) { ?>
                    <a href="http://<?= trim($oConst->value) ?>"  class="menu-soc-el" title="facebook">
                        <img class="menu-soc-el__img" src="/assets/images/menu-soc__f.png"  alt="facebook" />
                        <img class="menu-soc-el__img menu-soc-el__img_h" src="/assets/images/menu-soc__f_h.png"  alt="facebook" />
                    </a>
                <?php } ?>
                <?php if ($oConst->find('twitter')->loaded()) { ?>
                <a href="http://<?= trim($oConst->value) ?>"  class="menu-soc-el" title="twitter">
                    <img class="menu-soc-el__img" src="/assets/images/menu-soc__t.png"  alt="twitter" />
                    <img class="menu-soc-el__img menu-soc-el__img_h" src="/assets/images/menu-soc__t_h.png"  alt="twitter" />
                </a>
                <?php } ?>
                <?php if ($oConst->find('vk')->loaded()) { ?>
                <a href="http://<?= trim($oConst->value) ?>"  class="menu-soc-el" title="vk">
                    <img class="menu-soc-el__img" src="/assets/images/menu-soc__v.png"  alt="vk" />
                    <img class="menu-soc-el__img  menu-soc-el__img_h" src="/assets/images/menu-soc__v_h.png"  alt="vk" />
                </a>
                <?php } ?>
                <?php if ($oConst->find('youtube')->loaded()) { ?>
                <a href="http://<?= trim($oConst->value) ?>"  class="menu-soc-el" title="youtube">
                    <img class="menu-soc-el__img" src="/assets/images/menu-soc__u.png"  alt="youtube" />
                    <img class="menu-soc-el__img menu-soc-el__img_h" src="/assets/images/menu-soc__u_h.png"  alt="youtube" />
                </a>
                <?php } ?>
                <?php if ($oConst->find('google+')->loaded()) { ?>
                <a href="http://<?= trim($oConst->value) ?>"  class="menu-soc-el" title="google+">
                    <img class="menu-soc-el__img" src="/assets/images/menu-soc__g.png"  alt="google+" />
                    <img class="menu-soc-el__img menu-soc-el__img_h" src="/assets/images/menu-soc__g_h.png"  alt="google+" />
                </a>
                <?php } ?>
                <?php if ($oConst->find('instagram')->loaded()) { ?>
                <a href="http://<?= trim($oConst->value) ?>" target="_blank" class="menu-soc-el" title="instagram">
                    <img class="menu-soc-el__img" src="/assets/images/menu-soc_i.png"  alt="instagram" />
                    <img class="menu-soc-el__img menu-soc-el__img_h" src="/assets/images/menu-soc_i_h.png"  alt="instagram" />
                </a>
                <?php } ?>
            </div>
            <div class="null"></div>
        </div>
        <i class="menu-diz"></i>
    </div>

    <div class='body'>
        <div class='body-wrap'>

            <header class="header">
                <div class="header-fix">
                <div class="content">
                    <div class="header-half header-half_l">

                        <a href="#menu" class="header-menu-btn  js-box-btn">
                            <i class="header-menu-btn__burger">
                                <i class="line line_t"></i>
                                <i class="line line_c"></i>
                                <i class="line line_b"></i>
                            </i>
                            <span class="header-menu-btn__txt">
                                <span class="txt">Меню</span>
                            </span>
                        </a>

                        <div class="header-tel">
                            <div class="editor">
                                    <p><a href="tel:<?= preg_replace("/[^0-9]/", "", $oConst->find('Телефон сайта')->allowedTags()->value); ?>" class="js-tel"><?= $oConst->value; ?></a></p>
                            </div>
                        </div>

                    </div>

                    <div class="header-half header-half_r">
                        <a href="/about" class="header-about">
                            <img class="header-about__img" src="/assets/images/associations/best-kids-goods.png" alt="на страницу о нас" role="presentation" />
                        </a>
                        <?php
                            // Краткая корзина
                            $oShopCartControllerShow = new Shop_Cart_Controller_Show(
                                Core_Entity::factory('Shop', 4)
                            );
                            $oShopCartControllerShow
                                ->xsl(
                                    Core_Entity::factory('Xsl')->getByName(
                                        'МагазинКорзинаКраткая Маркус'
                                    )
                                )
                                ->show();
                        ?>
                        <div class="null"></div>
                    </div>

                    <div class="null"></div>
                    </div>
                </div>

                <div class="content">
                    <?php
                        $dateTime = Core_Date::timestamp2sql(time());
                        $oNotification = Core_Entity::factory('informationsystem_item');
                        $oNotification->queryBuilder()
                            ->where('active', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('informationsystem_id', '=', 25)
                            ->open()
                            ->where('start_datetime', '<', $dateTime)
                            ->setOr()
                            ->where('start_datetime', '=', '0000-00-00 00:00:00')
                            ->close()
                            ->setAnd()
                            ->open()
                            ->where('end_datetime', '>', $dateTime)
                            ->setOr()
                            ->where('end_datetime', '=', '0000-00-00 00:00:00')
                            ->close()
                            ->limit(1)
                            ->orderBy('datetime', 'DESC');
                        if ($oNotification->find()->id)
                        {
                            ?>
                            <div class="header-notice g-notice">
                                <img  class="g-notice__ico" src="/assets/images/notice_ico.svg" alt="notice" role="presentation" />
                                <div class="editor editor_bold">
                                    <?= $oNotification->description ?>
                                </div>
                            </div>
                            <?php
                        }
                    ?>

                    <div class="header-logo">
                        <a href="/" class="header-logo-btn" title="Маркус">
                            <img  class="header-logo-btn__ico" src="/assets/images/logo.png" alt="Маркус" />
                        </a>
                    </div>

                </div>
            </header>

            <?php
                Core_Page::instance()->execute();
            ?>

            <footer class="footer">
                <div class="content">

                    <div class="footer-soc">
                        <div class="footer-soc-title">
                            <span class="footer-soc-title__txt"><?= $oConst->find('Присоединяйтесь')->value ?></span>
                        </div>
                        <div  class="footer-soc-list">
                            <?php if ($oConst->allowedTags()->find('facebook')->loaded()) { ?>
                                <a href="http://<?= trim($oConst->value) ?>"  class="footer-soc-el" title="facebook">
                                    <img class="footer-soc-el__img" src="/assets/images/footer-soc__f.png"  alt="facebook" />
                                    <img class="footer-soc-el__img footer-soc-el__img_h" src="/assets/images/footer-soc__f_h.png"  alt="facebook" />
                                </a>
                            <?php } ?>
                            <?php if ($oConst->find('twitter')->loaded()) { ?>
                                <a href="http://<?= trim($oConst->value) ?>"  class="footer-soc-el" title="twitter">
                                    <img class="footer-soc-el__img" src="/assets/images/footer-soc__t.png"  alt="twitter" />
                                    <img class="footer-soc-el__img footer-soc-el__img_h" src="/assets/images/footer-soc__t_h.png"  alt="twitter" />
                                </a>
                            <?php } ?>
                            <?php if ($oConst->find('vk')->loaded()) { ?>
                                <a href="http://<?= trim($oConst->value) ?>"  class="footer-soc-el" title="vk">
                                    <img class="footer-soc-el__img" src="/assets/images/footer-soc__v.png"  alt="vk" />
                                    <img class="footer-soc-el__img footer-soc-el__img_h" src="/assets/images/footer-soc__v_h.png"  alt="vk" />
                                </a>
                            <?php } ?>
                            <?php if ($oConst->find('youtube')->loaded()) { ?>
                                <a href="http://<?= trim($oConst->value) ?>"  class="footer-soc-el" title="youtube">
                                    <img class="footer-soc-el__img" src="/assets/images/footer-soc__u.png"  alt="youtube" />
                                    <img class="footer-soc-el__img footer-soc-el__img_h" src="/assets/images/footer-soc__u_h.png"  alt="youtube" />
                                </a>
                            <?php } ?>
                            <?php if ($oConst->find('google+')->loaded()) { ?>
                                <a href="http://<?= trim($oConst->value) ?>"  class="footer-soc-el" title="google+">
                                    <img class="footer-soc-el__img" src="/assets/images/footer-soc__g.png"  alt="google+" />
                                    <img class="footer-soc-el__img footer-soc-el__img_h" src="/assets/images/footer-soc__g_h.png"  alt="google+" />
                                </a>
                            <?php } ?>
                            <?php if ($oConst->find('instagram')->loaded()) { ?>
                                <a href="http://<?= trim($oConst->value) ?>" target="_blank" class="footer-soc-el" title="instagram">
                                    <img class="footer-soc-el__img" src="/assets/images/footer-soc__i.png"  alt="instagram" />
                                    <img class="footer-soc-el__img footer-soc-el__img_h" src="/assets/images/footer-soc__i_h.png"  alt="instagram" />
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="footer-copyright">
                        <div class="editor editor_t">
                        <p><span>©</span><?=" " . date('Y') . " " . $oConst->find('Копирайт')->allowedTags(true)->value ?></p>
                        </div>
                        <div class="editor editor_b">
                            <p><?= $oConst->text ?></p>
                            <a href="/upload/docs/Markus-shop-policy-2017-07.pdf" target="_blank" rel="nofollow noopener">Политика обработки персональных данных</a>
                        </div>
                    </div>

                    <div class="footer-assoc">
                        <a href="https://acgi.ru" class="footer-assoc-el" rel="noopener noreferrer nofollow" target="_blank">
                            <img class="footer-assoc-el__img" src="/assets/images/associations/aidt.png" alt="на страницу о нас" role="presentation" />
                        </a>
                    </div>

                    <div class="footer-developer">
                        <a class="footer-developer__btn" href="http://artatom.ru/" target="_blank" title="Дизайн и создание сайтов"><span style="color: #ed496e; font-size: 13px;">Сайт разработан в Артатом</span>
                        </a>
                    </div>
                </div>
            </footer>

        </div>
    </div>

    <i class="body-menu-bg"></i>

    <!-- Личного кабинета для дистрибьютеров пока нет -->
    <?php   if ($sPath == 'partnership') : ?>
        <div id="box-form" class="box-form js-box_popup">
            <div class="box-form-wrapper">
                <div class="box-form-cancel js-box_popup-cancel">
                    <img class="box-form-cancel__img" src="/assets/images/menu__cancel.svg" alt="cancel" />
                    <img class="box-form-cancel__img box-form-cancel__img_h" src="/assets/images/menu__cancel_h.svg" alt="cancel" />
                </div>
                <!--
                ....форма авторизации дистрибьютера на сайте
                <div id="box-form__enter" class="box-form-wrap js-box-form-wrap_f">
                    <div class="box-form-title">
                        <img src="/assets/images/cab__ico_b.svg" class="box-form-title__img" alt="cab" />
                        <span class="box-form-title__txt">Вход в личный кабинет</span>
                        <div class="box-form-title__desc">
                            <div class="editor">
                                <p>только для дистрибьютеров</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-form-in">
                        <form action="/">
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Логин</span>
                                </div>
                                <div class="box-form-item-input">
                                    <input class="input" type="text" value="" />
                                </div>
                            </div>
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Пароль</span>
                                </div>
                                <div class="box-form-item-input">
                                    <input class="input" type="password" value="" />
                                </div>
                            </div>
                            <div class="box-form-item box-form-item_all">
                                <div class="checkbox">
                                    <input class="checkbox__input" type="checkbox" name="ch-s" id="ch-s" value="" />
                                    <label class="checkbox__label" for="ch-s">
                                        <span class="checkbox__txt">Запомнить пароль</span>
                                        <i class="diz">
                                            <i class="checkbox__bg">
                                                <img class="checkbox__ico" src="/assets/images/checkbox__ico.svg" alt="checkbox" />
                                            </i>
                                        </i>
                                    </label>
                                </div>

                                <a href="#box-form__forgot" class="box-form-item-btn box-form-item-btn_r">
                                    <span class="box-form-item-btn__txt">Забыли пароль?</span>
                                </a>

                                <a href="#box-form__registration" class="box-form-item-btn">
                                    <span class="box-form-item-btn__txt">Стать новым дистрибьютором</span>
                                </a>
                                <div class="null"></div>
                            </div>
                            <button class="button">
                                <span class="button__txt">войти</span>
                            </button>
                        </form>
                    </div>
                </div>

                <div id="box-form__forgot" class="box-form-wrap">
                    <div class="box-form-title">
                        <img src="/assets/images/cab__ico_b.svg" class="box-form-title__img" alt="cab" />
                        <span class="box-form-title__txt">Забыли пароль?</span>
                    </div>

                    <div class="box-form-in">
                        <form action="/">
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Имя <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="name" class="input" type="text" value="" />
                                </div>
                            </div>
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">E-mail <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="email" class="input" type="email" value="" />
                                </div>
                            </div>
                            <div class="box-form-item box-form-item_all">
                                <div class="editor">
                                    <p>Логин и пароль для входа в личный кабинет Вы получите по электронной почте после согласования с нашим менеджером.</p>
                                </div>
                                <div class="null"></div>
                            </div>
                            <button class="button  js-button_message">
                                <span class="button__txt">отправить</span>
                            </button>
                        </form>
                    </div>
                </div>
                -->
                <div id="box-form__registration" class="box-form-wrap js-box-form-wrap_f">
                    <div class="box-form-title">
                        <img src="/assets/images/cab__ico_b.svg" class="box-form-title__img" alt="cab" />
                        <span class="box-form-title__txt">стать новым дистрибьютером</span>
                    </div>

                    <div class="box-form-in">
                        <form name="reg_distributer" action="/partnership/">
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Имя <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="name" class="input" type="text" value="" />
                                </div>
                            </div>
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Название компании <span class="mustbe">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="firm" class="input" type="text" value="" />
                                </div>
                            </div>
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">E-mail <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="email" class="input" type="mail" value="" />
                                </div>
                            </div>
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Телефон</span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="phone" class="input" type="tel" value="" placeholder="89031112233"/>
                                </div>
                            </div>
                            <!-- <div class="box-form-item box-form-item_all">
                                <div class="editor">
                                    <p>Логин и пароль для входа в личный кабинет Вы получите по электронной почте после согласования с нашим менеджером.</p>
                                </div>
                                <div class="null"></div>
                            </div> -->
                            <input name="reg_distributer" type="hidden" value="1"/>
                            <button onclick="$(this).closest('form').submit(); return false;" class="button js-button_message">
                                <span class="button__txt">отправить</span>
                            </button>
                        </form>
                    </div>
                </div>

                <div class="box-form-wrap js-box-form-wrap__message">
                    <div class="box-form-title">
                        <span class="box-form-title__txt">Заявка отправлена</span>
                    </div>

                    <div class="box-form-in">
                        <div class="editor editor_b">
                            <p>Наш менеджер свяжется с вами в ближайшее время</p>
                        </div>
                    </div>
                </div>
            </div>
            <i class="box-form-diz"></i>
        </div>
    <?php endif ?>

    <?php   if ($bIsShopItem) : ?>
        <!-- Форма добавления КОММЕНТАРИЯ к товару -->
        <div id="box-comment" class="box-form js-box_popup">
            <div class="box-form-wrapper">
                <div class="box-form-cancel js-box_popup-cancel">
                    <img class="box-form-cancel__img" src="/assets/images/menu__cancel.svg" alt="cancel" />
                    <img class="box-form-cancel__img box-form-cancel__img_h" src="/assets/images/menu__cancel_h.svg" alt="cancel" />
                </div>

                <div id="box-comment__in" class="box-form-wrap js-box-form-wrap_f">
                    <div class="box-form-title">
                        <span class="box-form-title__txt">Оставить отзыв</span>
                    </div>
                    <div class="box-form-in">
                        <form name="add_comment">

                            <input name="add_shop_item_comment" type="hidden" value="1"/>

                            <input name="shop_item_id" type="hidden" value=""/>

                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Имя <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <!-- Заголовок (subject) комментария хранит имя автора отзыва -->
                                    <input name="name" class="input" type="text" value="" />
                                </div>
                            </div>

                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Опыт использования <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <!-- Поле author в комментарии хранит время использования товара -->
                                    <input name="author" class="input" type="text" value="" />
                                </div>
                            </div>

                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">E-mail <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="email" class="input" type="email" value="" />
                                </div>
                            </div>

                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Текст отзыва <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-textarea">
                                    <textarea name="text" class="textarea"></textarea>
                                </div>
                            </div>
                            <button onclick="$(this).closest('form').submit(); return false;" class="button js-button_message">
                                <span class="button__txt">отправить</span>
                            </button>
                        </form>
                    </div>
                </div>

                <div class="box-form-wrap js-box-form-wrap__message">
                    <div class="box-form-title">
                        <span class="box-form-title__txt"></span>
                    </div>

                    <div class="box-form-in">
                        <div class="editor editor_b"></div>
                    </div>
                </div>
            </div>
            <i class="box-form-diz"></i>
        </div>

        <!-- Форма БЫСТРОГО ЗАКАЗА -->
        <div id="box-indent" class="box-form js-box_popup">
            <div class="box-form-wrapper">
                <div class="box-form-cancel js-box_popup-cancel">
                    <img class="box-form-cancel__img" src="/assets/images/menu__cancel.svg" alt="cancel" />
                    <img class="box-form-cancel__img box-form-cancel__img_h" src="/assets/images/menu__cancel_h.svg" alt="cancel" />
                </div>

                <div id="box-indent__in" class="box-form-wrap js-box-form-wrap_f">
                    <div class="box-form-title">
                        <span class="box-form-title__txt">Быстрый заказ</span>
                    </div>
                    <div class="box-form-in">
                        <form action="/cart/" name="fast_order">
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Имя <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="name" class="input" type="text" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="box-form-item">
                                <div class="box-form-item-name">
                                    <span  class="box-form-item-name__txt">Телефон <span class="mustbe__el">*</span></span>
                                </div>
                                <div class="box-form-item-input">
                                    <input name="phone" class="input" type="tel" value="" placeholder="89031112233" />
                                </div>
                            </div>
                            <input type="hidden" name="shop_item_id" value="0"/>
                            <input type="hidden" name="fast_order" value="1"/>
                            <input type="hidden" name="city"/>
                            <button onclick="ga('send', 'event', 'button', 'click', 'zakaz')" onclick="yaCounter41311649.reachGoal('bystryi_zakaz'); return true;" class="button js-button_message" onclick="$(this).closest('form').submit(); return false;">
                                <span class="button__txt">Заказать</span>
                            </button>
                        </form>
                    </div>
                </div>

                <div class="box-form-wrap js-box-form-wrap__message">
                    <div class="box-form-title">
                        <span class="box-form-title__txt">Быстрый заказ оформлен</span>
                    </div>

                    <div class="box-form-in">
                        <div class="editor editor_b">
                            <p>С вами свяжется менеджер для подтверждения и уточнения заказа</p>
                        </div>
                    </div>
                </div>
            </div>
            <i class="box-form-diz"></i>
        </div>
    <?php endif ?>

    <?php if ($sPath == 'info') : ?>
        <!-- Форма выбора города -->
        <div id="box-city" class="box-form js-box_popup">
            <div class="box-form-wrapper">
                <div class="box-form-cancel js-box_popup-cancel">
                    <img class="box-form-cancel__img" src="/assets/images/menu__cancel.svg" alt="cancel" />
                    <img class="box-form-cancel__img box-form-cancel__img_h" src="/assets/images/menu__cancel_h.svg" alt="cancel" />
                </div>
                <div id="box-city__in" class="box-form-wrap js-box-form-wrap_f">
                    <div class="box-form-title">
                        <span class="box-form-title__txt">Выбрать другой город</span>
                    </div>
                    <div class="box-form-in">
                        <div class="box-form-item">
                            <div class="box-form-item-name">
                                <span  class="box-form-item-name__txt">Город</span>
                            </div>
                            <div class="box-form-item-input">
                                <input class="input" type="text" value="" />
                            </div>
                        </div>
                        <button class="button js-button_message">
                            <span class="button__txt">Выбрать</span>
                        </button>
                    </div>
                </div>
            </div>
            <i class="box-form-diz"></i>
        </div>
    <?php endif ?>

    <?
        // if ($sPath != 'info')
        // {
            Core_Page::instance()
                ->clearJs()
                ->js("/assets/js/artatom.js");
        // }

        // Для товаров магазина
        if ($bIsShopItem)
        {
            Core_Page::instance()
                // Верификация форм
                ->js("/assets/js/lib/jquery/validation/jquery.validate.min.js")
                ->js("/assets/js/lib/jquery/validation/localization/messages_ru.min.js")
                // Отправка форм
                ->js("/assets/js/lib/jquery/jquery.form.min.js")
                // Фото-галерея
                ->js("/assets/js/lib/jquery/jquery.photobox.min.js")
                ->js("/assets/js/lib/jquery/jquery.tmpl.js")
                ->js("/assets/js/aa-common.js")
                ->js("/assets/js/aa-ajax.js")
                ->js("/assets/js/aa-form-verification.js")
                ->js("/assets/js/aa-shop-item.js");
        }

        // Для оформления дистрибьютера или комментария к товару или быстрого заказа
        if ($sPath == 'partnership')
        {
            Core_Page::instance()
                // Верификация форм
                ->js("/assets/js/lib/jquery/validation/jquery.validate.min.js")
                ->js("/assets/js/lib/jquery/validation/localization/messages_ru.min.js")
                // Отправка форм
                ->js("/assets/js/aa-common.js")
                ->js("/assets/js/aa-ajax.js")
                ->js("/assets/js/lib/jquery/jquery.form.min.js")
                ->js("/assets/js/aa-form-verification.js");
        }

        // Для Как доставляем?
        if ($sPath == 'info')
        {
            Core_Page::instance()
                ->js("/assets/js/aa-common.js")
                ->js("/assets/js/aa-ajax.js")
                // Подсказки DaData
                //->js("/assets/js/lib/jquery/jquery.suggestions.js")
                ->js("/assets/js/lib/jquery/jquery.suggestions.min.js");
        }

        // Где купить?
        if ($sPath == 'where-buy')
        {
            Core_Page::instance()
                // ->js("/assets/js/lib/vue.js")
                ->js("/assets/js/lib/vue.min.js")
                //->js("/assets/js/lib/axios.js")
                ->js("/assets/js/lib/axios.min.js")
                ->js("/assets/js/aa-where-buy.js");
        }

        // Контакты
        if ($sPath == 'contacts')
        {
            Core_Page::instance()
                ->js("/assets/js/aa-contacts-info.js");
        }

        // Для корзины
        if ($sPath == 'cart' && !(
            isset($_GET['order_view']) || isset($_GET['guid']) ||
            isset($_GET['order_payment']) || isset($_GET['order_invoice']) ||
            isset($_REQUEST['orderNumber']) || isset($_REQUEST['payment'])
        ))
        {
            // для локального домена используем полную версию скриптов
            $site = Core_Entity::factory('Site', CURRENT_SITE)->getCurrentAlias();
            $minjs = substr($site->name, -strlen(".local")) == ".local" ? '' : '.min';

            Core_Page::instance()
                // Верификация форм
                ->js("/assets/js/lib/jquery/validation/jquery.validate.min.js")
                ->js("/assets/js/lib/jquery/validation/localization/messages_ru.min.js")
                ->js("/assets/js/lib/vue${minjs}.js")
                //->js("/assets/js/lib/axios.js")
                ->js("/assets/js/lib/axios.min.js")
                // Подсказки DaData
                //->js("/assets/js/lib/jquery/jquery.suggestions.js")
                ->js("/assets/js/lib/jquery/jquery.suggestions.min.js")
                // Автоподстройка шрифта для размещения строки в input-е. https://github.com/vxsx/jquery.inputfit.js
                ->js("/assets/js/lib/jquery/jquery.inputfit.js")
                // Прилипание при скроллинге. http://leafo.net/sticky-kit
                ->js("/assets/js/lib/jquery/jquery.sticky-kit.min.js")
                // Маска для ввода телефона
                ->js("/assets/js/lib/jquery/jquery.maskedinput.min.js")
                // Мониторинг изменения размеров. https://github.com/marcj/css-element-queries
                // ->js("/assets/js/lib/css-element-queries/src/ResizeSensor.js")
                ->js("/assets/js/aa-cart.js");
        }

        Core_Page::instance()
            ->showJs();
    ?>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                    try {
                            w.yaCounter41311649 = new Ya.Metrika({
                                    id:41311649,
                                    clickmap:true,
                                    trackLinks:true,
                                    accurateTrackBounce:true,
                                    webvisor:true,
                                    trackHash:true
                            });
                    } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <!-- /Yandex.Metrika counter -->

    <!-- HostCMS Counter -->
    <script type="text/javascript">document.cookie="hostmake=1; path=/";document.write("<a href=\"//www.hostcms.ru/\"><img src=\"//<?=Core::$url['host']?>/counter/counter.php?rand="+Math.random()+"&id=2&refer="+escape(document.referrer)+"&amp;current_page="+escape(window.location.href)+"&cookie="+(document.cookie?"Y":"N")+"&java="+(navigator.javaEnabled()?"Y":"N")+"&screen="+screen.width+'x'+screen.height+"&px="+(((navigator.appName.substring(0,9)=="Microsoft"))?screen.colorDepth:screen.pixelDepth)+"&js_version=1.6&counter=0\" alt=\"HostCMS Counter\" width=\"1\" height=\"1\" /></a>")</script>
    <!-- HostCMS Counter -->
</body>
</html>