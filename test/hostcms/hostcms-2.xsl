<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:hostcms="http://www.hostcms.ru/"
    exclude-result-prefixes="hostcms">
    <xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

    <!-- МагазинТоварProRiderz -->

    <xsl:decimal-format name="my" decimal-separator="," grouping-separator=" "/>

    <xsl:template match="/shop">
        <xsl:apply-templates select="shop_item" mode="proriderz"/>
    </xsl:template>

    <xsl:template match="shop_item" mode="proriderz">

        <xsl:variable name="group" select="/shop/group"/>
        <xsl:variable name="shop_item_id" select="@id"/>

        <xsl:variable name="dir" select="dir"/>

        <xsl:variable name="item_to_cart_id">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/@id"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@id"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_to_cart_rest">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:choose>
                        <xsl:when test="modifications/shop_item[position() = 1]/rest != ''"><xsl:value-of select="modifications/shop_item[position() = 1]/rest"/></xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="rest != ''"><xsl:value-of select="rest"/></xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_to_cart_price">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/price"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="price"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_to_cart_discount">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/discount"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="discount"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="main_shop_item_bonus">
            <xsl:choose>
                <xsl:when test="count(shop_bonuses/shop_bonus) &#62; 0">
                    <xsl:value-of select="shop_bonuses/total"/>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="main_shop_item_bonus_percent">
            <xsl:choose>
                <xsl:when test="bonus_percent"><xsl:value-of select="bonus_percent"/></xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="bonus_for_show_first">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:choose>
                        <xsl:when test="count(modifications/shop_item[position() = 1]/shop_bonuses/shop_bonus) &#62; 0">
                            <xsl:value-of select="modifications/shop_item[position() = 1]/shop_bonuses/total"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$main_shop_item_bonus &#62; 0">
                                    <xsl:value-of select="$main_shop_item_bonus"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="round(($main_shop_item_bonus_percent * $item_to_cart_price) div 100)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$main_shop_item_bonus &#62; 0">
                            <xsl:value-of select="$main_shop_item_bonus"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="round(($main_shop_item_bonus_percent * $item_to_cart_price) div 100)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="g-inner-page__heading">
            <div class="g-content">
                <h1 class="g-inner-page__heading-txt Ff(din)"><xsl:value-of select="name"/></h1>
            </div>
        </div>

        <div class="goods">
            <div class="g-content">
                <div class="goods-top">
                    <div class="goods-top__left">
                        <xsl:if test="marking != ''">
                            <div class="goods-art">
                                <div class="goods-art__txt t-h5">
                                    <span class="g-grey">Арт.</span>
                                    <span class="Fw(sb)"><xsl:value-of select="marking"/></span>
                                </div>
                            </div>
                        </xsl:if>
                        <div class="goods-label">
                            <div class="goods-label-el goods-label-el_red" id="discount_label">
                                <xsl:if test="$item_to_cart_discount = 0"><xsl:attribute name="style">display:none;</xsl:attribute></xsl:if>
                                <span class="goods-label-el__txt">-&#160;<xsl:value-of select="format-number($item_to_cart_discount, '#####0', 'my')"/>&#160;руб.</span>
                            </div>
                            <xsl:if test="property_value[tag_name='sales_hit']/value &#62; 0">
                                <div class="goods-label-el goods-label-el_red">
                                    <span class="goods-label-el__txt">Хит продаж</span>
                                </div>
                            </xsl:if>
                            <xsl:if test="property_value[tag_name='advice']/value &#62; 0">
                                <div class="goods-label-el goods-label-el_green">
                                    <span class="goods-label-el__txt">Советуем</span>
                                </div>
                            </xsl:if>
                            <xsl:if test="property_value[tag_name='new_item']/value &#62; 0">
                                <div class="goods-label-el goods-label-el_green">
                                    <span class="goods-label-el__txt">Новинка</span>
                                </div>
                            </xsl:if>
                            <xsl:if test="count(property_value[tag_name='shop_item_offer']/informationsystem_item[actual_offer = 1]) &#62; 0">
                                <div class="goods-label-el goods-label-el_green">
                                    <span class="goods-label-el__txt">Акция</span>
                                </div>
                            </xsl:if>
                        </div>
                    </div>
                    <div class="goods-top__right">
                        <div class="goods-cat">
                            <xsl:apply-templates select="/shop//shop_group[@id=$group]" mode="breadCrumbs"/>
                            <!-- вывести тип мотоцикла, если есть в группе это свойство -->
                            <xsl:if test="count(property_value[tag_name='mototype_flag'][value != '']) &#62; 0">
                                <xsl:for-each select="property_value[tag_name='mototype_flag'][value != '']">
                                    <xsl:variable name="val" select="value"/>
                                    <xsl:variable name="property_id" select="property_id"/>
                                    <xsl:variable name="value_id" select="/shop/shop_item_properties/.//property[@id = $property_id]/list/list_item[value = $val]/@id"/>
                                    <a href="{/shop/.//shop_group[@id = $group]/url}?filter=1&#38;property_{property_id}[]={$value_id}" class="goods-cat-el g-vac">
                                        <span class="goods-cat-el__txt g-vac__el"><xsl:value-of select="value"/></span>
                                    </a>
                                </xsl:for-each>
                            </xsl:if>
                        </div>
                    </div>
                </div>
                <div class="goods-in">
                    <div class="goods-in-grid">
                        <div class="goods-in-grid__img">
                            <div class="goods-slider__cont">
                                <div class="goods-slider">
                                    <div class="goods-slider__wrapper swiper-wrapper">
                                        <div class="goods-slider__slide swiper-slide">
                                            <a class="goods-slider__gal" data-fancybox="gallery" href="{dir}{image_large}">
                                                <xsl:choose>
                                                    <xsl:when test="image_large != ''">
                                                        <img class="goods-slider__img swiper-lazy" src="{dir}{image_large}" alt="{name}" />
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <img class="goods-slider__img swiper-lazy" src="/templates/template13/images/default_img_big.jpg" alt="{name}" />
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </a>
                                            <div class="swiper-lazy-preloader"></div>
                                        </div>
                                        <xsl:if test="count(property_value[tag_name = 'more_image'][file != '']) &#62; 0">
                                            <xsl:variable name="img_name" select="name"/>
                                            <xsl:for-each select="property_value[tag_name = 'more_image'][file != '']">
                                                <div class="goods-slider__slide swiper-slide">
                                                    <a class="goods-slider__gal" data-fancybox="gallery" href="{$dir}{file}">
                                                        <img class="goods-slider__img swiper-lazy" data-src="{$dir}{file}" alt="{$img_name}" />
                                                    </a>
                                                    <div class="swiper-lazy-preloader"></div>
                                                </div>
                                            </xsl:for-each>
                                        </xsl:if>
                                    </div>
                                </div>
                                <div class="goods-slider-thumb__cont">
                                    <div class="goods-slider-thumb">
                                        <div class="swiper-wrapper">
                                            <div class="goods-slider-thumb__slide swiper-slide">
                                                <img class="goods-slider-thumb__img g-abs swiper-lazy" data-src="{dir}{image_large}" alt="{name}" />
                                                <div class="swiper-lazy-preloader"></div>
                                            </div>
                                            <xsl:if test="count(property_value[tag_name = 'more_image'][file != '']) &#62; 0">
                                                <xsl:variable name="img_name_small" select="name"/>
                                                <xsl:for-each select="property_value[tag_name = 'more_image'][file != '']">
                                                     <div class="goods-slider-thumb__slide swiper-slide">
                                                        <img class="goods-slider-thumb__img g-abs swiper-lazy" data-src="{$dir}{file}" alt="{$img_name_small}" />
                                                        <div class="swiper-lazy-preloader"></div>
                                                    </div>
                                                </xsl:for-each>
                                            </xsl:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="goods-in-grid-data">
                            <div class="goods-in-grid-data__ctrl">
                                <div class="goods-data">
                                    <div class="goods-data-info">
                                        <xsl:if test="count(modifications/shop_item)">
                                            <div class="goods-data-info-item">
                                                <div class="goods-data-info__left">
                                                    <div class="goods-data-info-item__name Fw(sb) t-h5">
                                                        <xsl:choose>
                                                            <xsl:when test="property_value[tag_name='shop_item_goodtype']/informationsystem_item/property_value[tag_name = 'goodtype_goodpagelabel']/value != ''">
                                                                <xsl:value-of select="property_value[tag_name='shop_item_goodtype']/informationsystem_item/property_value[tag_name = 'goodtype_goodpagelabel']/value"/>:&#160;
                                                            </xsl:when>
                                                            <xsl:otherwise>Выбрать:&#160;</xsl:otherwise>
                                                        </xsl:choose>
                                                    </div>
                                                </div>
                                                <div class="goods-data-info__right">
                                                    <div class="goods-data-info-item__field">
                                                        <div class="g-select__cont">
                                                            <select class="g-select g-select_sm choose_mod" name="choose_mods" id="modifications" data-shopurl="{/shop/url}">
                                                                <xsl:for-each select="modifications/shop_item">
                                                                    <xsl:variable name="rest_txt">
                                                                        <xsl:choose>
                                                                            <xsl:when test="rest = 0">нет в наличии</xsl:when>
                                                                            <xsl:when test="rest &#62; 0 and rest &#60; 4">мало</xsl:when>
                                                                            <xsl:when test="rest &#62; 3">достаточно</xsl:when>
                                                                        </xsl:choose>
                                                                    </xsl:variable>
                                                                    <xsl:variable name="bonus_mod">
                                                                        <xsl:choose>
                                                                            <xsl:when test="count(shop_bonuses/shop_bonus) &#62; 0">
                                                                                <xsl:value-of select="shop_bonuses/total"/>
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                <xsl:choose>
                                                                                    <xsl:when test="$main_shop_item_bonus &#62; 0">
                                                                                        <xsl:value-of select="$main_shop_item_bonus"/>
                                                                                    </xsl:when>
                                                                                    <xsl:otherwise>
                                                                                        <xsl:variable name="percent_bonus" select="$main_shop_item_bonus_percent"/>
                                                                                        <xsl:value-of select="round(($percent_bonus * price) div 100)"/>
                                                                                    </xsl:otherwise>
                                                                                </xsl:choose>
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
                                                                    </xsl:variable>
                                                                    <xsl:variable name="bonus_txt">
                                                                        <xsl:call-template name="declension_bonus">
                                                                            <xsl:with-param name="number" select="$bonus_mod"/>
                                                                        </xsl:call-template>
                                                                    </xsl:variable>
                                                                    <option id="modification_{@id}" value="{@id}" data-id="{@id}" data-price="{format-number(price, '#####0', 'my')}" data-discount="{format-number(discount, '#####0', 'my')}" data-fullprice="{format-number(price+discount, '#####0', 'my')}"  data-bonus="{format-number($bonus_mod, '#####0', 'my')}" data-bonus_txt="{$bonus_txt}" data-rest="{format-number(rest, '#####0', 'my')}" data-mainitem="{modification_id}">
                                                                        <xsl:value-of select="name"/>&#160;(<xsl:value-of select="$rest_txt"/>)
                                                                    </option>
                                                                </xsl:for-each>
                                                            </select>
                                                            <svg role="presentation" class="g-select__ico g-abs g-abs_r">
                                                                <use xlink:href="#symbol-arrow_text-down"></use>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </xsl:if>
                                        <div class="goods-data-info-item">
                                            <div class="goods-data-info__left">
                                                <div class="goods-data-info-item__name Fw(sb) t-h5">На складе:</div>
                                            </div>
                                            <div class="goods-data-info__right">
                                                <div class="goods-data-info-item__val Fw(sb) t-h5" id="rest_txt_{@id}">
                                                    <xsl:choose>
                                                        <xsl:when test="$item_to_cart_rest &#62; 3"><xsl:attribute name="class">goods-data-info-item__val Fw(sb) t-h5 g-green</xsl:attribute>Достаточно</xsl:when>
                                                        <xsl:when test="$item_to_cart_rest &#60; 4 and $item_to_cart_rest &#62; 0"><xsl:attribute name="class">goods-data-info-item__val Fw(sb) t-h5 g-orange</xsl:attribute>Мало</xsl:when>
                                                        <xsl:otherwise><xsl:attribute name="class">goods-data-info-item__val Fw(sb) t-h5 g-red</xsl:attribute>Нет в наличии</xsl:otherwise>
                                                    </xsl:choose>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="goods-data-bonus">
                                        <div class="g-bonus">
                                            <div class="g-bonus-text">
                                                <div class="g-editor g-editor_sm g-editor_desc">
                                                    При покупке этого товара вы получите:
                                                    <br/>
                                                    <span class="g-bonus-text-value">
                                                        <svg role="presentation" class="g-bonus-text__ico">
                                                            <use xlink:href="#symbol-bonus"></use>
                                                        </svg>
                                                        <span class="Fw(sb)" id="bonus_value"><xsl:value-of select="format-number($bonus_for_show_first, '#####0', 'my')"/></span>&#160;
                                                        <span id="bonus_txt">
                                                            <xsl:call-template name="declension_bonus">
                                                                <xsl:with-param name="number" select="$bonus_for_show_first"/>
                                                            </xsl:call-template>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="goods-data-ctrl">
                                        <div class="goods-data-ctrl__btn">
                                            <div class="goods-data-ctrl__left">
                                                <button class="g-btn g-btn_link g-btn_p_off g-btn_orange js-tingle-buy-click">
                                                    <span class="g-btn__txt g-btn__txt_dashed Fw(sb)">Купить в один клик</span>
                                                </button>
                                            </div>
                                            <div class="goods-data-ctrl__right">
                                                <button class="g-btn g-btn_p_off goods-data-ctrl-favorite" onclick="return $.addFavorite('{/shop/url}', {@id}, this);">
                                                    <xsl:if test="/shop/favorite/shop_item[@id = $shop_item_id]/node()">
                                                        <xsl:attribute name="class">g-btn g-btn_p_off goods-data-ctrl-favorite is-favorite</xsl:attribute>
                                                    </xsl:if>
                                                    <span class="goods-data-ctrl-favorite__in_on" test="/shop/favorite/shop_item[@id = $shop_item_id]/node()">
                                                        <svg role="presentation" class="g-btn__ico g-btn__ico_l">
                                                            <use xlink:href="#symbol-star_fill"></use>
                                                        </svg>
                                                        <span class="g-btn__txt Fw(sb)">В избранном</span>
                                                    </span>
                                                    <span class="goods-data-ctrl-favorite__in_off">
                                                        <svg role="presentation" class="g-btn__ico g-btn__ico_l">
                                                            <use xlink:href="#symbol-star"></use>
                                                        </svg>
                                                        <span class="g-btn__txt Fw(sb)">В избранное</span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="goods-data-ctrl-soc">
                                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                                            <script src="//yastatic.net/share2/share.js"></script>

                                            <div class="goods-data-ctrl-soc-title">
                                                <div class="goods-data-ctrl-soc-title__txt Fw(sb) t-h5">Поделиться:</div>
                                            </div>
                                            <div class="ya-share2 goods-data-ctrl-soc__list" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter"></div>
                                        </div>
                                    </div>
                                    <div class="goods-data-bottom">
                                        <div class="goods-data-bottom__left">
                                            <div class="goods-data-price g-vac">
                                                <div class="goods-data-price__in g-vac__el">
                                                    <div class="goods-data-price__val goods-data-price__val_old" id="shop_item_old_price">
                                                        <xsl:if test="$item_to_cart_discount = 0">
                                                            <xsl:attribute name="style">display: none;</xsl:attribute>
                                                        </xsl:if>
                                                        <span class="goods-data-price__val-num">
                                                            <xsl:value-of select="format-number($item_to_cart_price + $item_to_cart_discount, '### ##0', 'my')"/>
                                                        </span>
                                                    </div>
                                                    <div class="goods-data-price__val goods-data-price__val_new Fw(sb)" id="shop_item_price">
                                                        <xsl:if test="$item_to_cart_discount = 0">
                                                            <xsl:attribute name="class">goods-data-price__val Fw(sb)</xsl:attribute>
                                                        </xsl:if>
                                                        <span><xsl:value-of select="format-number($item_to_cart_price, '### ##0', 'my')"/></span>&#160;
                                                        <svg role="presentation" class="g-currency g-rub">
                                                            <use xlink:href="#symbol-rub"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods-data-bottom__right">
                                            <div class="goods-data-quan g-vac">
                                                <div class="g-quan js-quan">
                                                    <xsl:choose>
                                                            <xsl:when test="count(modifications/shop_item) &#62; 0">
                                                                <xsl:if test="modifications/shop_item[position() = 1]/rest = 0">
                                                                    <xsl:attribute name="class">g-quan g-quan_disabled</xsl:attribute>
                                                                </xsl:if>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:if test="rest = 0">
                                                                    <xsl:attribute name="class">g-quan g-quan_disabled</xsl:attribute>
                                                                </xsl:if>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    <button type="button" class="g-quan__btn g-quan__btn_minus  g-abs g-abs_l  js-quan__minus">
                                                        <i class="g-quan__btn-ico g-abs">
                                                            <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                                        </i>
                                                    </button>
                                                    <input name="quantity" class="g-quan__num js-quan__num" autocomplete="off" data-max="10" type="tel" value="1">
                                                        <xsl:choose>
                                                            <xsl:when test="count(modifications/shop_item) &#62; 0">
                                                                <xsl:if test="modifications/shop_item[position() = 1]/rest = 0">
                                                                    <xsl:attribute name="value">0</xsl:attribute>
                                                                    <xsl:attribute name="data-max">0</xsl:attribute>
                                                                </xsl:if>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:if test="rest = 0">
                                                                    <xsl:attribute name="value">0</xsl:attribute>
                                                                    <xsl:attribute name="data-max">0</xsl:attribute>
                                                                </xsl:if>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </input>
                                                    <button type="button" class="g-quan__btn g-quan__btn_plus g-abs g-abs_r is-active  js-quan__plus">
                                                        <i class="g-quan__btn-ico g-abs">
                                                            <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                                            <i class="g-quan__btn-ico-line g-quan__btn-ico-line_v g-abs"></i>
                                                        </i>
                                                    </button>
                                                </div>

                                            </div>
                                            <div class="goods-data-btn">
                                                <button type="button" class="g-btn g-btn_big g-btn_bg_red" onclick="return $.addIntoCart('{/shop/url}cart/', {$item_to_cart_id}, 1)" id="button_to_cart">
                                                    <xsl:choose>
                                                        <xsl:when test="count(modifications/shop_item) &#62; 0">
                                                            <xsl:if test="modifications/shop_item[position() = 1]/rest = 0">
                                                                <xsl:attribute name="disabled" select="" />
                                                                <xsl:attribute name="onclick" select=""/>
                                                            </xsl:if>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:if test="rest = 0">
                                                                <xsl:attribute name="disabled" select=""/>
                                                                <xsl:attribute name="onclick" select=""/>
                                                            </xsl:if>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                    <svg role="presentation" class="g-btn__ico g-btn__ico_l g-btn__ico_cart">
                                                        <use xlink:href="#symbol-cart"></use>
                                                    </svg>
                                                    <span class="g-btn__txt">В корзину</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="goods-in-grid-data__text">
                                <div class="goods-in__text">
                                    <div class="goods-in__desc">
                                        <div class="g-editor g-editor_sm">
                                            <xsl:if test="description != ''">
                                                <xsl:value-of disable-output-escaping="yes" select="description" />
                                            </xsl:if>
                                        </div>
                                    </div>
                                    <div class="goods-in__charac">
                                        <div class="g-charac">
                                            <xsl:if test="shop_producer != ''">
                                                <div class="g-charac-item">
                                                    <table class="W(100%)">
                                                        <tr>
                                                            <td class="g-charac-item-td g-charac-item-td_l">
                                                                <div class="g-charac-item__left">
                                                                    <div class="g-charac-item__txt Fw(sb)">Производитель</div>
                                                                </div>
                                                            </td>
                                                            <td class="g-charac-item-td g-charac-item-td_line">
                                                                <div class="g-charac-item__center"></div>
                                                            </td>
                                                            <td class="g-charac-item-td g-charac-item-td_r">
                                                                <div class="g-charac-item__right">
                                                                    <div class="g-charac-item__txt "><xsl:value-of select="shop_producer/name"/></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </xsl:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="g-modal g-modal_buy_click">
                        <div class="g-modal__in">
                            <div class="g-modal-title">
                                <div class="g-modal-title__txt Fw(b) Ff(din) Tt(u) t-h2 g-les1">Купить в один клик</div>
                            </div>

                            <form action="{/shop/url}cart/" name="form_buy_click" class="g-form form_buy_click validate" method="post" id="oneStepCheckout">
                                <input type="hidden" name="shop_item_id" id="onestep_shop_item" value="{$item_to_cart_id}"/>
                                <input type="hidden" name="onestep_checkout" value="shop_item_card"/>
                                <input type="hidden" name="shop_payment_system_id" value="{/shop/shop_payment_systems/shop_payment_system[position() = 1]/@id}"/>
                                <div class="g-form-item g-form-item_wide">
                                    <div class="g-form-item__name">
                                        <label for="form_buy_click-name1" class="g-form-item__name-txt">ФИО *</label>
                                    </div>
                                    <div class="g-form-item__field">
                                        <input id="form_buy_click-name1" type="text" class="g-input required" name="name" value="" title="Заполните поле ФИО"/>
                                    </div>
                                </div>
                                <div class="g-form-item g-form-item_wide">
                                    <div class="g-form-item__name">
                                        <label for="form_buy_click-phone" class="g-form-item__name-txt">Телефон *</label>
                                    </div>
                                    <div class="g-form-item__field">
                                        <input id="form_buy_click-phone" type="tel" class="g-input required" name="phone" value="" title="Заполните поле Телефон"/>
                                    </div>
                                </div>
                                <div class="g-form-item g-form-item_checkbox">
                                    <div class="g-form-item__field">
                                        <div class="g-checkbox-cont">
                                            <div class="g-checkbox">
                                                <label class="g-checkbox__label">
                                                    <input class="g-checkbox__input required" type="checkbox" name="agree" value="agree" checked="checked"/>
                                                    <i class="g-checkbox__diz g-abs g-abs_l">
                                                        <svg role="presentation" class="g-checkbox__ico g-abs">
                                                            <use xlink:href="#symbol-check"></use>
                                                        </svg>
                                                    </i>
                                                    <span class="g-checkbox__txt">Согласие на обработку
                                                        <a href="shablon_obrabotka_personalnih_dannih_internet-magzina.pdf">персональных данных</a>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="g-form__btn g-form__btn_с">
                                    <button class="g-btn g-btn_big g-btn_bg_red" onclick="return SendForm('oneStepCheckout');">
                                        <span class="g-btn__txt">Отправить</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="goods-bottom">
                    <div class="g-tab no-js" data-tabs="true" role="tabpanel">
                        <div class="g-tab-nav" role="tablist">
                            <div class="g-tab-nav__wrapper g-tab-nav__wrapper_x3">
                                <button class="g-tab-nav-el g-vac is-active" role="tab">
                                    <span class="g-tab-nav-el__txt g-vac__el">Описание</span>
                                </button>
                                <button class="g-tab-nav-el g-vac " role="tab">
                                    <span class="g-tab-nav-el__txt g-vac__el">Характеристики</span>
                                </button>
                                <button class="g-tab-nav-el g-vac " role="tab">
                                    <span class="g-tab-nav-el__txt g-vac__el">Отзывы
                                        <span class="g-tab-nav-el__txt-count"><xsl:if test="count(comment) != 0"><xsl:value-of select="count(comment)"/></xsl:if></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <!-- Tab panes -->
                        <div class="g-tab-content">
                            <div class="g-tab-content__pane is-active" role="tabpanel">
                                <div class="g-tab-content-item">
                                    <div class="g-tab-content-desc">
                                        <!--div class="g-tab-content-desc-title">
                                            <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Применение</div>
                                        </div-->
                                        <div class="g-editor g-editor_sm">
                                            <xsl:value-of disable-output-escaping="yes" select="description" />
                                        </div>
                                    </div>
                                </div>
                                <xsl:if test="count(motomodels/motomodel) &#62; 0">
                                    <div class="g-tab-content-item">
                                        <div class="g-tab-content-desc">
                                            <div class="g-tab-content-desc-title">
                                                <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Применимость</div>
                                            </div>
                                            <div class="g-editor g-editor_sm">
                                                <xsl:for-each select="motomodels/motomodel">
                                                    <xsl:sort select="property_value[tag_name='motomodelgood_motomodel_id']/informationsystem_item/property_value[tag_name='motomodel_type']/value" order="ascending" type="text"/>
                                                    <p><xsl:value-of select="property_value[tag_name='motomodelgood_motomodel_id']/informationsystem_item/property_value[tag_name='motomodel_type']/value"/>&#160;<xsl:value-of select="property_value[tag_name='motomodelgood_motomodel_id']/informationsystem_item/property_value[tag_name='motomodel_manufacturer']/value"/>&#160;<xsl:value-of select="property_value[tag_name='motomodelgood_motomodel_id']/informationsystem_item/name"/>&#160;<xsl:value-of select="property_value[tag_name='motomodelgood_yearfrom']/value"/>&#160;-&#160;<xsl:value-of select="property_value[tag_name='motomodelgood_yearto']/value"/></p>
                                                </xsl:for-each>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:if test="(property_value[tag_name = 'tech_doc']/file != '') or (property_value[tag_name = 'safe_pass']/file != '') or (property_value[tag_name = 'declaration']/file != '')">
                                    <div class="g-tab-content-item">
                                        <div class="g-tab-content-desc">
                                            <div class="g-tab-content-desc-title">
                                                <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Документация</div>
                                            </div>
                                            <div class="g-tab-content-doc__list">
                                                <xsl:if test="property_value[tag_name = 'tech_doc']/file != ''">
                                                    <div class="g-tab-content-doc-el">
                                                        <a href="{dir}{property_value[tag_name = 'tech_doc']/file}" rel="noopener noreferrer nofollow" target="_blank" class="g-tab-content-doc-el__link g-vac">
                                                            <span class="g-tab-content-doc-el__ico g-abs g-abs_l">
                                                                <svg role="presentation" class="g-tab-content-doc-el__ico-in g-abs">
                                                                    <use xlink:href="#symbol-file_pdf_big"></use>
                                                                </svg>
                                                            </span>
                                                            <span class="g-tab-content-doc-el__txt t-h5 g-vac__el">Техническое описание</span>
                                                        </a>
                                                    </div>
                                                </xsl:if>
                                                <xsl:if test="property_value[tag_name = 'safe_pass']/file != ''">
                                                    <div class="g-tab-content-doc-el">
                                                        <a href="{dir}{property_value[tag_name = 'safe_pass']/file}" rel="noopener noreferrer nofollow" target="_blank" class="g-tab-content-doc-el__link g-vac">
                                                            <span class="g-tab-content-doc-el__ico g-abs g-abs_l">
                                                                <svg role="presentation" class="g-tab-content-doc-el__ico-in g-abs">
                                                                    <use xlink:href="#symbol-file_pdf_big"></use>
                                                                </svg>
                                                            </span>
                                                            <span class="g-tab-content-doc-el__txt t-h5 g-vac__el">Паспорт безопасности</span>
                                                        </a>
                                                    </div>
                                                </xsl:if>
                                                <xsl:if test="property_value[tag_name = 'declaration']/file != ''">
                                                    <div class="g-tab-content-doc-el">
                                                        <a href="{dir}{property_value[tag_name = 'declaration']/file}" rel="noopener noreferrer nofollow" target="_blank" class="g-tab-content-doc-el__link g-vac">
                                                            <span class="g-tab-content-doc-el__ico g-abs g-abs_l">
                                                                <svg role="presentation" class="g-tab-content-doc-el__ico-in g-abs">
                                                                    <use xlink:href="#symbol-download"></use>
                                                                </svg>
                                                            </span>
                                                            <span class="g-tab-content-doc-el__txt t-h5 g-vac__el">Декларация соответствия</span>
                                                        </a>
                                                    </div>
                                                </xsl:if>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                            </div>
                            <div class="g-tab-content__pane" role="tabpanel">
                                <xsl:if test="count(property_value[tag_name='shop_item_features'][value != ''])">
                                    <div class="g-tab-content-item">
                                        <div class="g-tab-content-desc">
                                            <div class="g-tab-content-desc-title">
                                                <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Особенности</div>
                                            </div>
                                            <div class="g-editor g-editor_sm">
                                                <p>
                                                <xsl:for-each select="property_value[tag_name = 'shop_item_features'][value != '']">
                                                    <xsl:value-of select="value"/>
                                                    <xsl:if test="position() != last()"><br/></xsl:if>
                                                </xsl:for-each>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:if test="count(property_value[tag_name='shop_item_starnote'][value != ''])">
                                    <div class="g-tab-content-item">
                                        <div class="g-tab-content-desc">
                                            <div class="g-tab-content-desc-title">
                                                <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Примечание</div>
                                            </div>
                                            <div class="g-editor g-editor_sm">
                                                <p>
                                                <xsl:for-each select="property_value[tag_name = 'shop_item_starnote'][value != '']">
                                                    <xsl:value-of select="value"/>
                                                    <xsl:if test="position() != last()"><br/></xsl:if>
                                                </xsl:for-each>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:if test="(weight != 0.00) or (length != 0.00) or (width != 0.00)">
                                    <div class="g-tab-content-item">
                                        <div class="g-tab-content-desc">
                                            <div class="g-tab-content-desc-title">
                                                <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Упаковка</div>
                                            </div>
                                            <div class="g-tab-content-charac">
                                                <div class="g-charac">
                                                    <xsl:if test="(length != 0.00) or (width != 0.00) or (height != 0.00)">
                                                        <div class="g-charac-item g-charac-item_mod">
                                                            <table class="W(100%)">
                                                                <tr>
                                                                    <td class="g-charac-item-td g-charac-item-td_l">
                                                                        <div class="g-charac-item__left">
                                                                            <div class="g-charac-item__txt Fw(sb)">Размер упаковки (ДхШхВ), см</div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_line">
                                                                        <div class="g-charac-item__center"></div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_r">
                                                                        <div class="g-charac-item__right">
                                                                            <div class="g-charac-item__txt "><xsl:value-of select="length div 10"/> x <xsl:value-of select="width div 10"/> x <xsl:value-of select="height div 10"/> см</div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </xsl:if>
                                                    <xsl:if test="weight != 0.00">
                                                        <div class="g-charac-item g-charac-item_mod">
                                                            <table class="W(100%)">
                                                                <tr>
                                                                    <td class="g-charac-item-td g-charac-item-td_l">
                                                                        <div class="g-charac-item__left">
                                                                            <div class="g-charac-item__txt Fw(sb)">Вес упаковки, кг</div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_line">
                                                                        <div class="g-charac-item__center"></div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_r">
                                                                        <div class="g-charac-item__right">
                                                                            <div class="g-charac-item__txt "><xsl:value-of select="weight"/> кг</div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </xsl:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                                <xsl:if test="shop_producer">
                                    <div class="g-tab-content-item">
                                        <div class="g-tab-content-desc">
                                            <div class="g-tab-content-desc-title">
                                                <div class="g-tab-content-desc-title__txt t-h5 Fw(sb)">Информация о производителе</div>
                                            </div>
                                            <div class="g-tab-content-charac">
                                                <div class="g-charac">
                                                    <div class="g-charac-item g-charac-item_mod">
                                                        <table class="W(100%)">
                                                            <tr>
                                                                <td class="g-charac-item-td g-charac-item-td_l">
                                                                    <div class="g-charac-item__left">
                                                                        <div class="g-charac-item__txt Fw(sb)">Производитель</div>
                                                                    </div>
                                                                </td>
                                                                <td class="g-charac-item-td g-charac-item-td_line">
                                                                    <div class="g-charac-item__center"></div>
                                                                </td>
                                                                <td class="g-charac-item-td g-charac-item-td_r">
                                                                    <div class="g-charac-item__right">
                                                                        <div class="g-charac-item__txt "><xsl:value-of select="shop_producer/name"/></div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <xsl:if test="shop_producer/address != ''">
                                                        <div class="g-charac-item g-charac-item_mod">
                                                            <table class="W(100%)">
                                                                <tr>
                                                                    <td class="g-charac-item-td g-charac-item-td_l">
                                                                        <div class="g-charac-item__left">
                                                                            <div class="g-charac-item__txt Fw(sb)">Страна производства</div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_line">
                                                                        <div class="g-charac-item__center"></div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_r">
                                                                        <div class="g-charac-item__right">
                                                                            <div class="g-charac-item__txt "><xsl:value-of select="shop_producer/address"/></div>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </xsl:if>
                                                    <xsl:if test="shop_producer/site != ''">
                                                        <div class="g-charac-item g-charac-item_mod">
                                                            <table class="W(100%)">
                                                                <tr>
                                                                    <td class="g-charac-item-td g-charac-item-td_l">
                                                                        <div class="g-charac-item__left">
                                                                            <div class="g-charac-item__txt Fw(sb)">Сайт производителя</div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_line">
                                                                        <div class="g-charac-item__center"></div>
                                                                    </td>
                                                                    <td class="g-charac-item-td g-charac-item-td_r">
                                                                        <div class="g-charac-item__right">
                                                                            <div class="g-charac-item__txt ">
                                                                                <a href="{shop_producer/site}" rel="noopener noreferrer nofollow" target="_blank"><xsl:value-of select="shop_producer/site"/></a>
                                                                            </div>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </xsl:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                            </div>

                            <div class="g-tab-content__pane" role="tabpanel">
                                <div class="g-tab-content-comment">
                                    <div class="g-tab-content-comment__btn">
                                        <button class="g-btn g-btn_ g-btn_bg_red js-tingle-comment">
                                            <span class="g-btn__txt">Добавить отзыв</span>
                                        </button>
                                    </div>
                                    <div class="g-tab-content-comment__list">
                                        <div class="g-comments__list">
                                            <div class="g-content_sm">
                                                <xsl:apply-templates select="comment" mode="proriderz"/>
                                            </div>
                                        </div>
                                    </div>
                                    <!--div class="g-tab-content-comment__pag">
                                        <button class="g-btn g-btn_ g-btn_bg_red">
                                            <span class="g-btn__txt">Больше отзывов</span>
                                        </button>
                                    </div-->
                                    <div class="g-modal g-modal_comments">
                                        <div class="g-modal__in">
                                            <div class="g-modal-title">
                                                <div class="g-modal-title__txt Fw(b) Ff(din) Tt(u) t-h2 g-les1">Оставьте ваш отзыв</div>
                                            </div>
                                            <form action="{url}" class="g-form form_comments validate" name="comment_form_00" method="post" id="comment_form_00">
                                                <input type="hidden" name="parent_id" value="0"/>
                                                <input type="hidden" name="add_comment" value="Опубликовать" />
                                                <xsl:if test="/shop/siteuser_id = 0">
                                                    <div class="g-form-item g-form-item_wide">
                                                        <div class="g-form-item__name">
                                                            <label for="form_comments-name1" class="g-form-item__name-txt">ФИО *</label>
                                                        </div>
                                                        <div class="g-form-item__field">
                                                            <input id="form_comments-name1" type="text" class="g-input required" name="author" value="" title="Заполните поле ФИО"/>
                                                        </div>
                                                    </div>
                                                    <div class="g-form-item g-form-item_wide">
                                                        <div class="g-form-item__name">
                                                            <label for="form_comments-email" class="g-form-item__name-txt">E-mail *</label>
                                                        </div>
                                                        <div class="g-form-item__field">
                                                            <input id="form_comments-email" type="email" class="g-input required" name="email" value="" title="Заполните поле E-mail"/>
                                                        </div>
                                                    </div>
                                                </xsl:if>
                                                <div class="g-form-item g-form-item_wide">
                                                    <div class="g-form-item__name">
                                                        <label for="form_comments-comment" class="g-form-item__name-txt">Достоинства</label>
                                                    </div>
                                                    <div class="g-form-item__field">
                                                        <textarea id="form_comments-comment" type="tel" class="g-textarea required" name="text" placeholder="" title="Заполните поле Достоинства"></textarea>
                                                    </div>
                                                </div>
                                                <div class="g-form-item g-form-item_wide">
                                                    <div class="g-form-item__name">
                                                        <label for="form_comments-comment" class="g-form-item__name-txt">Недостатки</label>
                                                    </div>
                                                    <div class="g-form-item__field">
                                                        <textarea id="form_comments-comment" type="tel" class="g-textarea required" name="text2" placeholder="" title="Заполните поле Недостатки"></textarea>
                                                    </div>
                                                </div>
                                                <div class="g-form-item g-form-item_wide">
                                                    <div class="g-form-item__name">
                                                        <label for="form_comments-comment" class="g-form-item__name-txt">Комментарий</label>
                                                    </div>
                                                    <div class="g-form-item__field">
                                                        <textarea id="form_comments-comment" type="tel" class="g-textarea required" name="text3" placeholder="" title="Заполните поле Комментарий"></textarea>
                                                    </div>
                                                </div>
                                                <div class="g-form__btn g-form__btn_с">
                                                    <div id="error" class="hidden"></div>
                                                    <button class="g-btn g-btn_big g-btn_bg_red" onclick="return SendForm('comment_form_00');">
                                                        <span class="g-btn__txt">Отправить</span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden" id="sucess"></div>
        </div>

        <xsl:if test="count(similar/shop_item) &#62; 0">
            <div class="g-card-slider g-card-slider_bg">
                <div class="g-content">
                    <div class="g-card-slider__in">
                        <div class="g-card-slider-title">
                            <span class="g-card-slider-title__txt t-h2 Ff(din) Fw(b) Tt(u) g-les1">Похожие товары</span>
                        </div>
                        <div class="swiper-wrapper">
                            <xsl:apply-templates select="similar/shop_item"/>
                        </div>
                        <button class="g-card-slider-nav g-card-slider-nav_prev g-abs g-abs_t g-abs_r">
                            <svg role="presentation" aria-hidden="true" class="g-card-slider-nav__ico abs">
                                <use xlink:href="/templates/template13/images/sprites/sprites_label/svg/symbols.svg#symbol-slider-prev"></use>
                            </svg>
                        </button>
                        <button class="g-card-slider-nav g-card-slider-nav_next g-abs g-abs_t g-abs_r">
                            <svg role="presentation" aria-hidden="true" class="g-card-slider-nav__ico abs">
                                <use xlink:href="/templates/template13/images/sprites/sprites_label/svg/symbols.svg#symbol-slider-next"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template match="/shop/shop_item/property_value">

        <xsl:variable name="property_id" select="property_id"/>
        <xsl:variable name="property" select="/shop/shop_item_properties//property[@id=$property_id]" />

        <xsl:if test="($property/type != 2) or ($property/type != 5) or ($property/type != 7) or ($property/type != 12)">
            <div class="g-charac-item g-charac-item_mod">
                <table class="W(100%)">
                    <tr>
                        <td class="g-charac-item-td g-charac-item-td_l">
                            <div class="g-charac-item__left">
                                <div class="g-charac-item__txt Fw(sb)"><xsl:value-of select="$property/name"/></div>
                            </div>
                        </td>
                        <td class="g-charac-item-td g-charac-item-td_line">
                            <div class="g-charac-item__center"></div>
                        </td>
                        <td class="g-charac-item-td g-charac-item-td_r">
                            <div class="g-charac-item__right">
                                <div class="g-charac-item__txt "><xsl:value-of select="value"/></div>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </xsl:if>

    </xsl:template>

    <xsl:template match="comment" mode="proriderz">
        <!-- Отображаем комментарий, если задан текст  -->
        <xsl:if test="text != ''">

            <xsl:variable name="username">
                <xsl:choose>
                    <xsl:when test="count(siteuser) = 0">Данные скрыты</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of disable-output-escaping="yes" select="siteuser/name"/>&#160;<xsl:value-of disable-output-escaping="yes" select="siteuser/surname"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <div class="g-comments-el" id="comment{@id}">
                <div class="g-comments-el__text">
                    <div class="g-comments-el-title">
                        <div class="g-comments-el-title-img g-abs g-abs_l g-abs_t">
                            <xsl:choose>
                                <xsl:when test="count(siteuser) = 0">
                                    <svg role="presentation" class="g-comments-el-title-img__ico g-abs">
                                        <use xlink:href="#symbol-nofoto"></use>
                                    </svg>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="siteuser/siteuser_person[position() = 1]/image != ''">
                                            <span class="g-comments-el-title-img__photo g-abs g-bg_cover" style="background-image: url({siteuser/siteuser_person[position() = 1]/dir}{siteuser/siteuser_person[position() = 1]/image})"></span>
                                        </xsl:when>
                                        <xsl:when test="siteuser/siteuser_company[position() = 1]/image != ''">
                                            <span class="g-comments-el-title-img__photo g-abs g-bg_cover" style="background-image: url({siteuser/siteuser_company[position() = 1]/dir}{siteuser/siteuser_company[position() = 1]/image})"></span>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <svg role="presentation" class="g-comments-el-title-img__ico g-abs">
                                                <use xlink:href="#symbol-nofoto"></use>
                                            </svg>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                        <div class="g-comments-el-title__txt g-vac">
                            <span class="g-comments-el-title__txt-in g-vac__el"><xsl:value-of select="$username"/></span>
                        </div>
                        <div class="g-comments-el-title__date g-vac">
                            <span class="g-comments-el-title__date-txt g-vac__el">
                                <xsl:value-of select="substring-before(date, '.')"/>
                                <xsl:variable name="month_year" select="substring-after(date, '.')"/>
                                <xsl:variable name="month" select="substring-before($month_year, '.')"/>
                                <xsl:choose>
                                    <xsl:when test="$month = 1"> января </xsl:when>
                                    <xsl:when test="$month = 2"> февраля </xsl:when>
                                    <xsl:when test="$month = 3"> марта </xsl:when>
                                    <xsl:when test="$month = 4"> апреля </xsl:when>
                                    <xsl:when test="$month = 5"> мая </xsl:when>
                                    <xsl:when test="$month = 6"> июня </xsl:when>
                                    <xsl:when test="$month = 7"> июля </xsl:when>
                                    <xsl:when test="$month = 8"> августа </xsl:when>
                                    <xsl:when test="$month = 9"> сентября </xsl:when>
                                    <xsl:when test="$month = 10"> октября </xsl:when>
                                    <xsl:when test="$month = 11"> ноября </xsl:when>
                                    <xsl:otherwise> декабря </xsl:otherwise>
                                </xsl:choose>
                                <xsl:value-of select="substring-after($month_year, '.')"/>
                            </span>
                        </div>
                    </div>
                    <div class="g-comments-el__desc">
                        <div class="g-editor g-editor_sm">
                            <xsl:value-of select="text" disable-output-escaping="yes"/>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="show_modifications">
        <xsl:param name="modifications"/>
        <xsl:param name="tag_name"/>

        <xsl:variable name="property" select="/shop/shop_item_properties//property[tag_name=$tag_name]"/>

        <xsl:choose>
            <xsl:when test="$tag_name = 'color'">
                <xsl:if test="$property/type = 3">
                    <div class="goods-data-info-item__color">
                        <div class="g-color-ch__list">
                            <xsl:for-each select="$property/list/list_item">
                                <xsl:variable name="value" select="value"/>
                                <xsl:if test="count(/shop/shop_item/modifications/shop_item[property_value[tag_name=$tag_name][value=$value]]) &#62; 0">

                                    <div class="g-color-ch g-color-ch_big">
                                        <label class="g-color-ch__label">
                                            <input class="g-color-ch__input js-goods-color choose_mod" type="radio" data-property_id="{$property/@id}" name="choose_{$property/@id}" value="{$value}" data-src="{/shop/shop_item/modifications/shop_item[property_value[tag_name=$tag_name][value=$value][position() = 1]]/dir}{/shop/shop_item/modifications/shop_item[property_value[tag_name=$tag_name][value=$value][position() = 1]]/image_large}" >
                                                <xsl:if test="position() = 1">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-color-ch__diz" style="background-color: {description}">
                                                <svg role="presentation" class="g-color-ch__diz-ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                        </label>
                                    </div>
                                </xsl:if>
                            </xsl:for-each>
                        </div>
                    </div>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$property/type = 3"> <!-- список -->
                    <div class="goods-data-info-item__field">
                        <div class="g-select__cont">
                            <select class="g-select g-select_sm choose_mod" name="choose_{$property/@id}" data-property_id="{$property/@id}">
                                <xsl:for-each select="$property/list/list_item">
                                    <xsl:variable name="value" select="value"/>
                                    <xsl:if test="count(/shop/shop_item/modifications/shop_item[property_value[tag_name=$tag_name][value=$value]]) &#62; 0">
                                        <option value="{$value}"><xsl:value-of select="$value"/></option>
                                    </xsl:if>
                                </xsl:for-each>
                            </select>
                            <svg role="presentation" class="g-select__ico g-abs g-abs_r">
                                <use xlink:href="#symbol-arrow_text-down"></use>
                            </svg>
                        </div>
                    </div>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="shop_item">

        <h1 hostcms:id="{@id}" hostcms:field="name" hostcms:entity="shop_item"><xsl:value-of select="name"/></h1>

        <!-- Получаем ID родительской группы и записываем в переменную $group -->
        <xsl:variable name="group" select="/shop/group"/>

        <p>
            <xsl:if test="$group = 0">
                <a href="{/shop/url}" hostcms:id="{/shop/@id}" hostcms:field="name" hostcms:entity="shop">
                    <xsl:value-of select="/shop/name"/>
                </a>
            </xsl:if>

            <!-- Путь к группе -->
            <xsl:apply-templates select="/shop//shop_group[@id=$group]" mode="breadCrumbs"/>

            <!-- Если модификация, выводим в пути родительский товар -->
            <xsl:if test="shop_item/node()">
            <span><xsl:text> → </xsl:text></span>
                <a href="{shop_item/url}">
                    <xsl:value-of disable-output-escaping="yes" select="shop_item/name"/>
                </a>
            </xsl:if>

        <span><xsl:text> → </xsl:text></span>

        <b><a href="{url}" hostcms:id="{@id}" hostcms:field="name" hostcms:entity="shop_item"><xsl:value-of select="name"/></a></b>
        </p>

        <!-- Выводим сообщение -->
        <xsl:if test="/shop/message/node()">
            <xsl:value-of disable-output-escaping="yes" select="/shop/message"/>
        </xsl:if>

        <div>
            <!-- Изображение для товара, если есть -->
            <xsl:if test="image_small != ''">
                <div id="gallery" class="shop_img">
                    <a href="{dir}{image_large}" target="_blank"><img src="{dir}{image_small}" /></a>
                </div>
            </xsl:if>

            <!-- Цена товара -->
            <xsl:if test="price != 0">
                <div class="price">
                <xsl:value-of select="format-number(price, '### ##0,00', 'my')"/><xsl:text> </xsl:text><xsl:value-of select="currency"/><xsl:text> </xsl:text>

                    <!-- Если цена со скидкой - выводим ее -->
                    <xsl:if test="discount != 0">
                        <span class="oldPrice">
                            <xsl:value-of select="format-number(price + discount, '### ##0,00', 'my')"/><xsl:text> </xsl:text><xsl:value-of select="currency" />
                    </span><xsl:text> </xsl:text>
                    </xsl:if>

                    <!-- Ссылку на добавление в корзины выводим, если:
                    type = 0 - простой тип товара
                    type = 1 - электронный товар, при этом остаток на складе больше 0 или -1,
                    что означает неограниченное количество -->
                    <xsl:if test="type = 0 or (type = 1 and (digitals > 0 or digitals = -1)) or type = 2">
                        <a href="{/shop/url}cart/?add={@id}" onclick="return $.addIntoCart('{/shop/url}cart/', {@id}, 1)">
                            <img src="/images/add_to_cart.gif" alt="Добавить в корзину" title="Добавить в корзину" />
                        </a>
                    </xsl:if>
                </div>
            </xsl:if>

            <!-- Cкидки -->
            <xsl:if test="count(shop_discount)">
                <xsl:apply-templates select="shop_discount"/>
            </xsl:if>

            <xsl:if test="marking != ''">
            <div class="shop_property">Артикул: <span hostcms:id="{@id}" hostcms:field="marking" hostcms:entity="shop_item"><xsl:value-of select="marking"/></span></div>
            </xsl:if>

            <xsl:if test="shop_producer/node()">
            <div class="shop_property">Производитель: <span><xsl:value-of select="shop_producer/name"/></span></div>
            </xsl:if>

            <!-- Если указан вес товара -->
            <xsl:if test="weight != 0">
    <div class="shop_property">Вес товара: <span hostcms:id="{@id}" hostcms:field="weight" hostcms:entity="shop_item"><xsl:value-of select="weight"/></span><xsl:text> </xsl:text><span><xsl:value-of select="/shop/shop_measure/name"/></span></div>
            </xsl:if>

            <!-- Количество на складе для не электронного товара -->
            <xsl:if test="rest &gt; 0 and type != 1">
<div class="shop_property">В наличии: <span><xsl:value-of select="rest - reserved"/><xsl:text> </xsl:text><xsl:value-of select="shop_measure/name"/></span><xsl:if test="reserved &gt; 0"> (зарезервировано: <span><xsl:value-of select="reserved"/><xsl:text> </xsl:text><xsl:value-of select="shop_measure/name"/></span>)</xsl:if></div>
            </xsl:if>

            <!-- Если электронный товар, выведим доступное количество -->
            <xsl:if test="type = 1">
                <div class="shop_property">
                    <xsl:choose>
                        <xsl:when test="digitals = 0">
                            Электронный товар закончился.
                        </xsl:when>
                        <xsl:when test="digitals = -1">
                            Электронный товар доступен для заказа.
                        </xsl:when>
                        <xsl:otherwise>
                    На складе осталось: <span><xsl:value-of select="digitals" /><xsl:text> </xsl:text><xsl:value-of select="shop_measure/name" /></span>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
            </xsl:if>

            <div style="clear: both;"></div>

            <!-- Описание товара -->
            <xsl:if test="description != ''">
                <div hostcms:id="{@id}" hostcms:field="description" hostcms:entity="shop_item" hostcms:type="wysiwyg"><xsl:value-of disable-output-escaping="yes" select="description" /></div>
            </xsl:if>

            <!-- Текст товара -->
            <xsl:if test="text != ''">
                <div hostcms:id="{@id}" hostcms:field="text" hostcms:entity="shop_item" hostcms:type="wysiwyg"><xsl:value-of disable-output-escaping="yes" select="text"/></div>
            </xsl:if>

            <!-- Размеры товара -->
            <xsl:if test="length != 0 or width != 0 or height != 0">
                <div class="shop_property">Размеры: <span><xsl:value-of select="length" /><xsl:text> </xsl:text><xsl:value-of select="/shop/size_measure/name" />
                        <xsl:text> × </xsl:text>
                        <xsl:value-of select="width" /><xsl:text> </xsl:text><xsl:value-of select="/shop/size_measure/name" />
                        <xsl:text> × </xsl:text>
                <xsl:value-of select="height" /><xsl:text> </xsl:text><xsl:value-of select="/shop/size_measure/name" /></span></div>
            </xsl:if>

            <xsl:if test="count(property_value)">
                <h2>Атрибуты товара</h2>
                <xsl:apply-templates select="property_value"/>
            </xsl:if>
        </div>

        <!-- Модификации -->
        <xsl:if test="count(modifications/shop_item) &gt; 0">
            <p class="h2">Модификации <xsl:value-of select="name"/></p>
            <ul class="shop_list">
                <xsl:apply-templates select="modifications/shop_item"/>
            </ul>
        </xsl:if>

        <xsl:if test="count(associated/shop_item) &gt; 0">
            <p class="h2">Сопутствующие товары <xsl:value-of select="name"/></p>
            <ul class="shop_list">
                <xsl:apply-templates select="associated/shop_item"/>
            </ul>
        </xsl:if>

        <p class="tags">
            <!-- Средняя оценка элемента -->
            <xsl:if test="comments_average_grade/node() and comments_average_grade != 0">
                <span><xsl:call-template name="show_average_grade">
                        <xsl:with-param name="grade" select="comments_average_grade"/>
                    <xsl:with-param name="const_grade" select="5"/></xsl:call-template></span>
            </xsl:if>

            <!-- Тэги для информационного элемента -->
            <xsl:if test="count(tag) &gt; 0">
                <img src="/images/tag.png" /><span><xsl:apply-templates select="tag"/></span>
            </xsl:if>

            <xsl:if test="count(siteuser) &gt; 0">
            <img src="/images/user.png" /><span><a href="/users/info/{siteuser/login}/"><xsl:value-of select="siteuser/login"/></a></span>
            </xsl:if>

            <xsl:if test="rate/node()">
                <span id="shop_item_id_{@id}" class="thumbs">
                    <xsl:choose>
                        <xsl:when test="/shop/siteuser_id > 0">
                            <xsl:choose>
                                <xsl:when test="vote/value = 1">
                                    <xsl:attribute name="class">thumbs up</xsl:attribute>
                                </xsl:when>
                                <xsl:when test="vote/value = -1">
                                    <xsl:attribute name="class">thumbs down</xsl:attribute>
                                </xsl:when>
                            </xsl:choose>
                            <span id="shop_item_likes_{@id}"><xsl:value-of select="rate/@likes" /></span>
                            <span class="inner_thumbs">
                                <a onclick="return $.sendVote({@id}, 1, 'shop_item')" href="{/shop/url}?id={@id}&amp;vote=1&amp;entity_type=shop_item" alt="Нравится"></a>
                                <span class="rate" id="shop_item_rate_{@id}"><xsl:value-of select="rate" /></span>
                                <a onclick="return $.sendVote({@id}, 0, 'shop_item')" href="{/shop/url}?id={@id}&amp;vote=0&amp;entity_type=shop_item" alt="Не нравится"></a>
                            </span>
                            <span id="shop_item_dislikes_{@id}"><xsl:value-of select="rate/@dislikes" /></span>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class">thumbs inactive</xsl:attribute>
                            <span id="shop_item_likes_{@id}"><xsl:value-of select="rate/@likes" /></span>
                            <span class="inner_thumbs">
                                <a alt="Нравится"></a>
                                <span class="rate" id="shop_item_rate_{@id}"><xsl:value-of select="rate" /></span>
                                <a alt="Не нравится"></a>
                            </span>
                            <span id="shop_item_dislikes_{@id}"><xsl:value-of select="rate/@dislikes" /></span>
                        </xsl:otherwise>
                    </xsl:choose>
                </span>
            </xsl:if>

            <!-- Дата информационного элемента -->
            <img src="/images/calendar.png" /> <xsl:value-of select="date"/>, <span hostcms:id="{@id}" hostcms:field="showed" hostcms:entity="shop_item"><xsl:value-of select="showed"/></span>
            <xsl:text> </xsl:text>
            <xsl:call-template name="declension">
                <xsl:with-param name="number" select="showed"/>
        </xsl:call-template><xsl:text>. </xsl:text>
        </p>

        <!-- Если указано отображать комментарии -->
        <xsl:if test="/shop/show_comments/node() and /shop/show_comments = 1">

            <!-- Отображение комментариев  -->
            <xsl:if test="count(comment) &gt; 0">
            <p class="h1"><a name="comments"></a>Комментарии</p>
                <xsl:apply-templates select="comment"/>
            </xsl:if>
        </xsl:if>

        <!-- Если разрешено отображать формы добавления комментария
        1 - Только авторизированным
        2 - Всем
        -->
        <xsl:if test="/shop/show_add_comments/node() and ((/shop/show_add_comments = 1 and /shop/siteuser_id &gt; 0)  or /shop/show_add_comments = 2)">

            <p class="button" onclick="$('.comment_reply').hide('slow');$('#AddComment').toggle('slow')">
                Добавить комментарий
            </p>

            <div id="AddComment" class="comment_reply">
                <xsl:call-template name="AddCommentForm"></xsl:call-template>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Шаблон для товара просмотренные -->
    <xsl:template match="shop_item" mode="view">
        <div class="shop_item">
            <div class="shop_table_item">
                <div class="image_row">
                    <div class="image_cell">
                        <a href="{url}">
                            <xsl:choose>
                                <xsl:when test="image_small != ''">
                                    <img src="{dir}{image_small}" alt="{name}" title="{name}"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <img src="/images/no-image.png" alt="{name}" title="{name}"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </a>
                    </div>
                </div>
                <div class="description_row">
                    <div class="description_sell">
                        <p>
                            <a href="{url}" title="{name}" hostcms:id="{@id}" hostcms:field="name" hostcms:entity="shop_item">
                                <xsl:value-of select="name"/>
                            </a>
                        </p>
                        <div class="price">
                        <xsl:value-of select="format-number(price, '### ##0,00', 'my')"/><xsl:text> </xsl:text><xsl:value-of select="currency"/><xsl:text> </xsl:text>
                            <!-- Ссылку на добавление в корзины выводим, если:
                            type = 0 - простой тип товара
                            type = 1 - электронный товар, при этом остаток на складе больше 0 или -1,
                            что означает неограниченное количество -->
                            <xsl:if test="type = 0 or (type = 1 and (digitals > 0 or digitals = -1))">
                                <a href="{/shop/url}cart/?add={@id}" onclick="return $.addIntoCart('{/shop/url}cart/', {@id}, 1)">
                                    <img src="/images/add_to_cart.gif" alt="Добавить в корзину" title="Добавить в корзину" />
                                </a>
                            </xsl:if>

                            <!-- Сравнение товаров -->
                            <xsl:variable name="shop_item_id" select="@id" />
                            <div class="compare" onclick="return $.addCompare('{/shop/url}', {@id}, this)">
                                <xsl:if test="/shop/comparing/shop_item[@id = $shop_item_id]/node()">
                                    <xsl:attribute name="class">compare current</xsl:attribute>
                                </xsl:if>
                            </div>
                            <!-- Избранное -->
                            <div class="favorite" onclick="return $.addFavorite('{/shop/url}', {@id}, this)">
                                <xsl:if test="/shop/favorite/shop_item[@id = $shop_item_id]/node()">
                                    <xsl:attribute name="class">favorite favorite_current</xsl:attribute>
                                </xsl:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <xsl:if test="position() mod 3 = 0 and position() != last()">
            <span class="table_row"></span>
        </xsl:if>
    </xsl:template>

    <!-- Вывод строки со значением свойства -->
    <xsl:template match="property_value">
        <xsl:if test="value/node() and value != '' or file/node() and file != ''">
            <div class="shop_property">
                <xsl:variable name="property_id" select="property_id" />
                <xsl:variable name="property" select="/shop/shop_item_properties//property[@id=$property_id]" />

                <xsl:value-of select="$property/name"/><xsl:text>: </xsl:text>
                <span><xsl:choose>
                        <xsl:when test="$property/type = 2">
                            <a href="{../dir}{file}" target="_blank"><xsl:value-of select="file_name"/></a>
                        </xsl:when>
                        <xsl:when test="$property/type = 5">
                            <a href="{informationsystem_item/url}"><xsl:value-of select="informationsystem_item/name"/></a>
                        </xsl:when>
                        <xsl:when test="$property/type = 7">
                            <input type="checkbox" disabled="disabled">
                                <xsl:if test="value = 1">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input>
                        </xsl:when>
                        <xsl:when test="$property/type = 12">
                            <a href="{shop_item/url}"><xsl:value-of select="shop_item/name"/></a>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of disable-output-escaping="yes" select="value"/>
                            <!-- Единица измерения свойства -->
                            <xsl:if test="$property/shop_measure/node()">
                                <xsl:text> </xsl:text><xsl:value-of select="$property/shop_measure/name"/>
                            </xsl:if>
                        </xsl:otherwise>
                </xsl:choose></span>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Метки -->
    <xsl:template match="tag">
        <a href="{/shop/url}tag/{urlencode}/" class="tag">
            <xsl:value-of select="name"/>
        </a>
    <xsl:if test="position() != last()"><xsl:text>, </xsl:text></xsl:if>
    </xsl:template>

    <!-- Шаблон для модификаций -->
    <xsl:template match="modifications/shop_item">
        <li>
            <!-- Название модификации -->
            <a href="{url}"><xsl:value-of select="name"/></a>,
            <!-- Цена модификации -->
            <xsl:value-of select="price"/><xsl:text> </xsl:text><xsl:value-of disable-output-escaping="yes" select="currency"/>
        </li>
    </xsl:template>

    <!-- Шаблон для сопутствующих товаров -->
    <xsl:template match="similar/shop_item">
        <xsl:variable name="group" select="shop_group_id"/>
        <xsl:variable name="group_url" select="/shop/.//shop_group[@id = $group]/url"/>

        <xsl:variable name="similar_to_cart_id">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/@id"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@id"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="similar_to_cart_rest">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="sum(modifications/shop_item/rest)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="rest"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="similar_to_cart_price">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/price"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="price"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="similar_to_cart_discount">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/discount"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="discount"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="g-card-slider__slide swiper-slide">
            <div class="g-card">
                <div class="g-card-cat">
                    <xsl:apply-templates select="/shop//shop_group[@id=$group]" mode="breadCrumbs2"/>
                </div>
                <div class="g-card-img">
                    <div class="g-card-img-label g-abs g-abs_t">
                        <div class="goods-label-el goods-label-el_red">
                            <xsl:if test="$similar_to_cart_discount = 0"><xsl:attribute name="style">display:none;</xsl:attribute></xsl:if>
                            <span class="goods-label-el__txt">-&#160;<xsl:value-of select="format-number($similar_to_cart_discount, '#####0', 'my')"/>&#160;руб.</span>
                        </div>
                        <xsl:if test="property_value[tag_name='sales_hit']/value &#62; 0">
                            <div class="goods-label-el goods-label-red">
                                <span class="goods-label-el__txt">Хит продаж</span>
                            </div>
                        </xsl:if>
                        <xsl:if test="property_value[tag_name='advice']/value &#62; 0">
                            <div class="goods-label-el goods-label-el_green">
                                <span class="goods-label-el__txt">Советуем</span>
                            </div>
                        </xsl:if>
                        <xsl:if test="property_value[tag_name='new_item']/value &#62; 0">
                            <div class="goods-label-el goods-label-el_green">
                                <span class="goods-label-el__txt">Новинка</span>
                            </div>
                        </xsl:if>
                    </div>
                    <a href="{url}" class="g-card-img__wrap g-abs">
                        <xsl:choose>
                            <xsl:when test="image_small != ''">
                                <img class="g-card-img__in g-abs" src="{dir}{image_small}" alt="{name}" />
                            </xsl:when>
                            <xsl:otherwise>
                                <img class="g-card-img__in g-abs" src="/templates/template13/images/default_img_min.jpg" alt="{name}" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </a>
                </div>
                <a href="{url}" class="g-card-title">
                    <span class="g-card-title__txt"><xsl:value-of select="name"/></span>
                </a>

                <div class="g-card-feature">
                    <span class="g-card-feature__txt">
                        <xsl:for-each select="modifications/shop_item">
                            <xsl:value-of select="normalize-space(name)"/><xsl:if test="position() != last()">,&#160;</xsl:if>
                        </xsl:for-each>
                    </span>
                </div>

                <div class="g-card-info">
                    <xsl:choose>
                        <xsl:when test="$similar_to_cart_rest &#60; 4 and $similar_to_cart_rest != 0">
                            <div class="g-card-info-quantity">
                                <span class="g-card-info-quantity__txt g-orange">Мало</span>
                            </div>
                        </xsl:when>
                        <xsl:when test="$similar_to_cart_rest &#62; 3">
                            <div class="g-card-info-quantity">
                                <span class="g-card-info-quantity__txt g-green">Достаточно</span>
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="g-card-info-quantity g-card-info-quantity_red">
                                <span class="g-card-info-quantity__txt g-red">Нет в наличии</span>
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>

                    <a href="{$group_url}?producer_id[]={shop_producer_id}&#38;filter=find" class="g-card-info-brand"> <!-- переход в каталог по производителю -->
                        <span class="g-card-info-brand__txt"><xsl:value-of select="shop_producer/name"/></span>
                    </a>
                </div>
                <div class="g-card-bottom">
                    <div class="g-card-bottom__left">
                        <div class="g-card-cost">
                            <div class="g-card-cost__in">
                                <xsl:if test="discount != 0">
                                    <span class="g-card-cost__txt g-card-cost__txt_old">
                                        <span class="g-card-cost-old__txt-num">
                                            <xsl:value-of select="format-number($similar_to_cart_price + $similar_to_cart_discount, '### ##0', 'my')"/>
                                        </span>
                                    </span>
                                </xsl:if>
                                <span class="g-card-cost__txt">
                                    <xsl:if test="discount != 0">
                                        <xsl:attribute name="class">g-card-cost__txt g-card-cost__txt_new</xsl:attribute>
                                    </xsl:if>
                                    <span class="g-card-cost__txt-num"><xsl:value-of select="format-number($similar_to_cart_price, '### ##0', 'my')"/>&#160;</span>
                                    <svg role="presentation" class="g-card-cost__txt-currency g-rub">
                                        <use xlink:href="#symbol-rub"></use>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="g-card-bottom__right">
                        <!--div class="g-card-add-cont">
                            <div class="g-quan js-quan">
                                <button type="button" class="g-quan__btn g-quan__btn_minus  g-abs g-abs_l  js-quan__minus">
                                    <i class="g-quan__btn-ico g-abs">
                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                    </i>
                                </button>
                                <input name="quantity_{$similar_to_cart_id}" class="g-quan__num js-quan__num" autocomplete="off" data-max="10" type="tel" value="1" />
                                <button type="button" class="g-quan__btn g-quan__btn_plus g-abs g-abs_r is-active  js-quan__plus">
                                    <i class="g-quan__btn-ico g-abs">
                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_v g-abs"></i>
                                    </i>
                                </button>
                            </div>
                            <button type="button" class="g-card-add g-card-add_quan" onclick="return $.addIntoCart('{/shop/url}cart/', {$similar_to_cart_id}, 1)" >
                                <svg role="presentation" class="g-card-add__ico g-abs g-abs_r">
                                    <use xlink:href="#symbol-cart_card"></use>
                                </svg>
                            </button>

                            <div class="g-card-add-mess g-abs g-vac">
                                <div class="g-card-add-mess__in g-vac__el">
                                    <div class="g-card-add-mess__txt t-h5">
                                        Товар в корзине
                                    </div>
                                    <i class="g-card-add-mess__diz g-abs g-abs_r">
                                        <svg role="presentation" class="g-card-add-mess__ico g-abs">
                                            <use xlink:href="#symbol-check"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div-->
                        <xsl:choose>
                            <xsl:when test="$similar_to_cart_rest = 0">
                                <div class="g-card-add-cont">
                                    <a href="{url}" class="g-card-link">
                                        <span class="g-card-add__txt">Смотреть</span>
                                    </a>
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="count(modifications/shop_item) &#62; 0">
                                        <div class="g-card-add-cont">
                                            <a href="{url}" class="g-card-add">
                                                <span class="g-card-add__txt">Выбрать</span>
                                                <svg role="presentation" class="g-card-add__ico g-abs g-abs_r">
                                                    <use xlink:href="#symbol-cart_card"></use>
                                                </svg>
                                            </a>
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <div class="g-card-add-cont">
                                            <div class="g-quan js-quan">
                                                <button type="button" class="g-quan__btn g-quan__btn_minus  g-abs g-abs_l  js-quan__minus">
                                                    <i class="g-quan__btn-ico g-abs">
                                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                                    </i>
                                                </button>
                                                <input id="quantity_{$similar_to_cart_id}" class="g-quan__num js-quan__num" autocomplete="off" data-max="10" type="tel" value="1"/>
                                                <button type="button" class="g-quan__btn g-quan__btn_plus g-abs g-abs_r is-active  js-quan__plus">
                                                    <i class="g-quan__btn-ico g-abs">
                                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                                    <i class="g-quan__btn-ico-line g-quan__btn-ico-line_v g-abs"></i>
                                                    </i>
                                                </button>
                                            </div>

                                            <button type="button" class="g-card-add g-card-add_quan" onclick="return $.addIntoCart('{/shop/url}cart/', {$similar_to_cart_id}, 1)" >
                                                <svg role="presentation" class="g-card-add__ico g-abs g-abs_r">
                                                    <use xlink:href="#symbol-cart_card"></use>
                                                </svg>
                                            </button>

                                            <div class="g-card-add-mess g-abs g-vac">
                                                <div class="g-card-add-mess__in g-vac__el">
                                                    <div class="g-card-add-mess__txt t-h5">
                                                        Товар в корзине
                                                    </div>
                                                    <i class="g-card-add-mess__diz g-abs g-abs_r">
                                                        <svg role="presentation" class="g-card-add-mess__ico g-abs">
                                                            <use xlink:href="#symbol-check"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </div>
            </div>
        </div>


    </xsl:template>

    <!-- Вывод рейтинга -->
    <xsl:template name="show_average_grade">
        <xsl:param name="grade" select="0"/>
        <xsl:param name="const_grade" select="0"/>

        <!-- Чтобы избежать зацикливания -->
        <xsl:variable name="current_grade" select="$grade * 1"/>

        <xsl:choose>
            <!-- Если число целое -->
            <xsl:when test="floor($current_grade) = $current_grade and not($const_grade &gt; ceiling($current_grade))">

                <xsl:if test="$current_grade - 1 &gt; 0">
                    <xsl:call-template name="show_average_grade">
                        <xsl:with-param name="grade" select="$current_grade - 1"/>
                        <xsl:with-param name="const_grade" select="$const_grade - 1"/>
                    </xsl:call-template>
                </xsl:if>

                <xsl:if test="$current_grade != 0">
                    <img src="/images/star-full.png"/>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$current_grade != 0 and not($const_grade &gt; ceiling($current_grade))">

                <xsl:if test="$current_grade - 0.5 &gt; 0">
                    <xsl:call-template name="show_average_grade">

                        <xsl:with-param name="grade" select="$current_grade - 0.5"/>
                        <xsl:with-param name="const_grade" select="$const_grade - 1"/>
                    </xsl:call-template>
                </xsl:if>

                <img src="/images/star-half.png"/>
            </xsl:when>

            <!-- Выводим серые звездочки, пока текущая позиция не дойдет то значения, увеличенного до целого -->
            <xsl:otherwise>
                <xsl:call-template name="show_average_grade">
                    <xsl:with-param name="grade" select="$current_grade"/>
                    <xsl:with-param name="const_grade" select="$const_grade - 1"/>
                </xsl:call-template>
                <img src="/images/star-empty.png"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Шаблон для вывода звездочек (оценки) -->
    <xsl:template name="for">
        <xsl:param name="i" select="0"/>
        <xsl:param name="n"/>

        <input type="radio" name="shop_grade" value="{$i}" id="id_shop_grade_{$i}">
            <xsl:if test="/shop/shop_grade = $i">
                <xsl:attribute name="checked"></xsl:attribute>
            </xsl:if>
    </input><xsl:text> </xsl:text>
        <label for="id_shop_grade_{$i}">
            <xsl:call-template name="show_average_grade">
                <xsl:with-param name="grade" select="$i"/>
                <xsl:with-param name="const_grade" select="5"/>
            </xsl:call-template>
        </label>
        <br/>
        <xsl:if test="$n &gt; $i and $n &gt; 1">
            <xsl:call-template name="for">
                <xsl:with-param name="i" select="$i + 1"/>
                <xsl:with-param name="n" select="$n"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!-- Отображение комментариев -->
    <xsl:template match="comment">
        <!-- Отображаем комментарий, если задан текст или тема комментария -->
        <xsl:if test="text != '' or subject != ''">
            <a name="comment{@id}"></a>
            <div class="comment" id="comment{@id}">
                <xsl:if test="subject != ''">
                    <div class="subject" hostcms:id="{@id}" hostcms:field="subject" hostcms:entity="comment"><xsl:value-of select="subject"/></div>
                </xsl:if>

                <div hostcms:id="{@id}" hostcms:field="text" hostcms:entity="comment" hostcms:type="wysiwyg"><xsl:value-of select="text" disable-output-escaping="yes"/></div>

                <p class="tags">
                    <!-- Оценка комментария -->
                    <xsl:if test="grade != 0">
                        <span><xsl:call-template name="show_average_grade">
                                <xsl:with-param name="grade" select="grade"/>
                                <xsl:with-param name="const_grade" select="5"/>
                        </xsl:call-template></span>
                    </xsl:if>

                    <img src="/images/user.png" />
                    <xsl:choose>
                        <!-- Комментарий добавил авторизированный пользователь -->
                        <xsl:when test="count(siteuser) &gt; 0">
                        <span><a href="/users/info/{siteuser/login}/"><xsl:value-of select="siteuser/login"/></a></span>
                        </xsl:when>
                        <!-- Комментарй добавил неавторизированный пользователь -->
                        <xsl:otherwise>
                            <span><xsl:value-of select="author" /></span>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:if test="rate/node()">
                        <span id="comment_id_{@id}" class="thumbs">
                            <xsl:choose>
                                <xsl:when test="/shop/siteuser_id > 0">
                                    <xsl:choose>
                                        <xsl:when test="vote/value = 1">
                                            <xsl:attribute name="class">thumbs up</xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="vote/value = -1">
                                            <xsl:attribute name="class">thumbs down</xsl:attribute>
                                        </xsl:when>
                                    </xsl:choose>

                                    <span id="comment_likes_{@id}"><xsl:value-of select="rate/@likes" /></span>
                                    <span class="inner_thumbs">
                                        <a onclick="return $.sendVote({@id}, 1, 'comment')" href="{/shop/url}?id={@id}&amp;vote=1&amp;entity_type=comment" alt="Нравится"></a>
                                        <span class="rate" id="comment_rate_{@id}"><xsl:value-of select="rate" /></span>
                                        <a onclick="return $.sendVote({@id}, 0, 'comment')" href="{/shop/url}?id={@id}&amp;vote=0&amp;entity_type=comment" alt="Не нравится"></a>
                                    </span>
                                    <span id="comment_dislikes_{@id}"><xsl:value-of select="rate/@dislikes" /></span>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="class">thumbs inactive</xsl:attribute>
                                    <span id="comment_likes_{@id}"><xsl:value-of select="rate/@likes" /></span>
                                    <span class="inner_thumbs">
                                        <a alt="Нравится"></a>
                                        <span class="rate" id="comment_rate_{@id}"><xsl:value-of select="rate" /></span>
                                        <a alt="Не нравится"></a>
                                    </span>
                                    <span id="comment_dislikes_{@id}"><xsl:value-of select="rate/@dislikes" /></span>
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </xsl:if>

                    <img src="/images/calendar.png" /> <span><xsl:value-of select="datetime"/></span>

                    <xsl:if test="/shop/show_add_comments/node()
                        and ((/shop/show_add_comments = 1 and /shop/siteuser_id > 0)
                        or /shop/show_add_comments = 2)">
                    <span class="red" onclick="$('.comment_reply').hide('slow');$('#cr_{@id}').toggle('slow')">ответить</span></xsl:if>

                <span class="red"><a href="{/shop/shop_item/url}#comment{@id}" title="Ссылка на комментарий">#</a></span>
                </p>
            </div>

            <!-- Отображаем только авторизированным пользователям -->
            <xsl:if test="/shop/show_add_comments/node() and ((/shop/show_add_comments = 1 and /shop/siteuser_id > 0) or /shop/show_add_comments = 2)">
                <div class="comment_reply" id="cr_{@id}">
                    <xsl:call-template name="AddCommentForm">
                        <xsl:with-param name="id" select="@id"/>
                    </xsl:call-template>
                </div>
            </xsl:if>

            <!-- Выбираем дочерние комментарии -->
            <xsl:if test="count(comment)">
                <div class="comment_sub">
                    <xsl:apply-templates select="comment"/>
                </div>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <!-- Шаблон вывода добавления комментария -->
    <xsl:template name="AddCommentForm">
        <xsl:param name="id" select="0"/>

        <!-- Заполняем форму -->
        <xsl:variable name="subject">
            <xsl:if test="/shop/comment/parent_id/node() and /shop/comment/parent_id/node() and /shop/comment/parent_id= $id">
                <xsl:value-of select="/shop/comment/subject"/>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="email">
            <xsl:if test="/shop/comment/email/node() and /shop/comment/parent_id/node() and /shop/comment/parent_id= $id">
                <xsl:value-of select="/shop/comment/email"/>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="phone">
            <xsl:if test="/shop/comment/phone/node() and /shop/comment/parent_id/node() and /shop/comment/parent_id= $id">
                <xsl:value-of select="/shop/comment/phone"/>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="text">
            <xsl:if test="/shop/comment/text/node() and /shop/comment/parent_id/node() and /shop/comment/parent_id= $id">
                <xsl:value-of select="/shop/comment/text"/>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="name">
            <xsl:if test="/shop/comment/author/node() and /shop/comment/parent_id/node() and /shop/comment/parent_id= $id">
                <xsl:value-of select="/shop/comment/author"/>
            </xsl:if>
        </xsl:variable>

        <div class="comment">
            <!--Отображение формы добавления комментария-->
            <form action="{/shop/shop_item/url}" name="comment_form_0{$id}" method="post">
                <!-- Авторизированным не показываем -->
                <xsl:if test="/shop/siteuser_id = 0">

                    <div class="row">
                        <div class="caption">Имя</div>
                        <div class="field">
                            <input type="text" size="70" name="author" value="{$name}"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="caption">E-mail</div>
                        <div class="field">
                            <input id="email{$id}" type="text" size="70" name="email" value="{$email}" />
                            <div id="error_email{$id}"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="caption">Телефон</div>
                        <div class="field">
                            <input type="text" size="70" name="phone" value="{$phone}"/>
                        </div>
                    </div>
                </xsl:if>

                <div class="row">
                    <div class="caption">Тема</div>
                    <div class="field">
                        <input type="text" size="70" name="subject" value="{$subject}"/>
                    </div>
                </div>

                <div class="row">
                    <div class="caption">Комментарий</div>
                    <div class="field">
                        <textarea name="text" cols="68" rows="5" class="mceEditor"><xsl:value-of select="$text"/></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="caption">Оценка</div>
                    <div class="field stars">
                        <select name="grade">
                            <option value="1">Poor</option>
                            <option value="2">Fair</option>
                            <option value="3">Average</option>
                            <option value="4">Good</option>
                            <option value="5">Excellent</option>
                        </select>
                    </div>
                </div>

                <!-- Обработка CAPTCHA -->
                <xsl:if test="//captcha_id != 0 and /shop/siteuser_id = 0">
                    <div class="row">
                        <div class="caption"></div>
                        <div class="field">
                            <img id="comment_{$id}" class="captcha" src="/captcha.php?id={//captcha_id}{$id}&amp;height=30&amp;width=100" title="Контрольное число" name="captcha"/>

                            <div class="captcha">
                                <img src="/images/refresh.png" /> <span onclick="$('#comment_{$id}').updateCaptcha('{//captcha_id}{$id}', 30); return false">Показать другое число</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="caption">
                    Контрольное число<sup><font color="red">*</font></sup>
                        </div>
                        <div class="field">
                            <input type="hidden" name="captcha_id" value="{//captcha_id}{$id}"/>
                            <input type="text" name="captcha" size="15"/>
                        </div>
                    </div>
                </xsl:if>

                <xsl:if test="$id != 0">
                    <input type="hidden" name="parent_id" value="{$id}"/>
                </xsl:if>

                <div class="row">
                    <div class="caption"></div>
                    <div class="field">
                        <input id="submit_email{$id}" type="submit" name="add_comment" value="Опубликовать" class="button" />
                    </div>
                </div>
            </form>
        </div>
    </xsl:template>

    <!-- Шаблон для скидки -->
    <xsl:template match="shop_discount">
        <div class="shop_discount">
            <xsl:value-of select="name"/><xsl:text> </xsl:text>
            <span>
                <xsl:choose>
                    <xsl:when test="type = 0">
                        <xsl:value-of select="percent"/>%
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="amount"/><xsl:text> </xsl:text><xsl:value-of select="/shop/shop_currency/name"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </div>
    </xsl:template>

    <!-- Шаблон выводит хлебные крошки -->
    <xsl:template match="shop_group" mode="breadCrumbs">
        <xsl:variable name="parent_id" select="parent_id"/>

        <!-- Выбираем рекурсивно вышестоящую группу -->
        <xsl:apply-templates select="//shop_group[@id=$parent_id]" mode="breadCrumbs"/>

        <a href="{url}" class="goods-cat-el g-vac">
             <span class="goods-cat-el__txt g-vac__el"><xsl:value-of select="name"/></span>
        </a>

    </xsl:template>

    <xsl:template match="shop_group" mode="breadCrumbs2">
        <xsl:variable name="parent_id" select="parent_id"/>

        <!-- Выбираем рекурсивно вышестоящую группу -->
        <xsl:apply-templates select="//shop_group[@id=$parent_id]" mode="breadCrumbs2"/>

        <a href="{url}" class="g-card-cat-el">
             <span class="g-card-cat-el__txt"><xsl:value-of select="name"/></span>
        </a>

    </xsl:template>

    <!-- Склонение после числительных -->
    <xsl:template name="declension">

        <xsl:param name="number" select="number"/>

        <!-- Именительный падеж -->
    <xsl:variable name="nominative"><xsl:text>просмотр</xsl:text></xsl:variable>

        <!-- Родительный падеж, единственное число -->
    <xsl:variable name="genitive_singular"><xsl:text>просмотра</xsl:text></xsl:variable>

    <xsl:variable name="genitive_plural"><xsl:text>просмотров</xsl:text></xsl:variable>
        <xsl:variable name="last_digit"><xsl:value-of select="$number mod 10"/></xsl:variable>
        <xsl:variable name="last_two_digits"><xsl:value-of select="$number mod 100"/></xsl:variable>

        <xsl:choose>
            <xsl:when test="$last_digit = 1 and $last_two_digits != 11">
                <xsl:value-of select="$nominative"/>
            </xsl:when>
            <xsl:when test="$last_digit = 2 and $last_two_digits != 12
                or $last_digit = 3 and $last_two_digits != 13
                or $last_digit = 4 and $last_two_digits != 14">
                <xsl:value-of select="$genitive_singular"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$genitive_plural"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="declension_bonus">

        <xsl:param name="number" select="number"/>

        <!-- Именительный падеж -->
        <xsl:variable name="nominative"><xsl:text>бонус</xsl:text></xsl:variable>

            <!-- Родительный падеж, единственное число -->
        <xsl:variable name="genitive_singular"><xsl:text>бонуса</xsl:text></xsl:variable>

        <xsl:variable name="genitive_plural"><xsl:text>бонусов</xsl:text></xsl:variable>
        <xsl:variable name="last_digit"><xsl:value-of select="$number mod 10"/></xsl:variable>
        <xsl:variable name="last_two_digits"><xsl:value-of select="$number mod 100"/></xsl:variable>

        <xsl:choose>
            <xsl:when test="$last_digit = 1 and $last_two_digits != 11">
                <xsl:value-of select="$nominative"/>
            </xsl:when>
            <xsl:when test="$last_digit = 2 and $last_two_digits != 12
                or $last_digit = 3 and $last_two_digits != 13
                or $last_digit = 4 and $last_two_digits != 14">
                <xsl:value-of select="$genitive_singular"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$genitive_plural"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>