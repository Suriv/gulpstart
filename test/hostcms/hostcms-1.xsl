<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:hostcms="http://www.hostcms.ru/"
    exclude-result-prefixes="hostcms">
    <xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

    <xsl:decimal-format name="my" decimal-separator="," grouping-separator=" "/>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="/shop/ajax = 1">
                <xsl:apply-templates select="/shop" mode="proriderz_ajax"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="/shop" mode="proriderz"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:variable name="n" select="number(3)"/>

    <xsl:template match="/shop" mode="proriderz">
        <!-- Получаем ID родительской группы и записываем в переменную $group -->
        <xsl:variable name="group" select="group"/>

        <!-- дополнение пути для action, если выбрана метка -->
        <xsl:variable name="form_tag_url"><xsl:if test="count(tag) = 1">tag/<xsl:value-of select="tag/urlencode"/>/</xsl:if></xsl:variable>

        <xsl:variable name="path">
            <xsl:choose>
                <xsl:when test="/shop//shop_group[@id=$group]/node()"><xsl:value-of select="/shop//shop_group[@id=$group]/url"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="/shop/url"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="parent_group">
            <xsl:choose>
                <xsl:when test="$group = 0">0</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=".//shop_group[@id=$group]/parent_id"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="back_name">
            <xsl:choose>
                <xsl:when test="$group = 0"></xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$parent_group = 0">Каталог</xsl:when>
                        <xsl:otherwise><xsl:value-of select=".//shop_group[@id = $parent_group]/name"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="back_url">
            <xsl:choose>
                <xsl:when test="$group = 0"></xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$parent_group = 0"><xsl:value-of select="url"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select=".//shop_group[@id = $parent_group]/url"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="g-inner-page__heading">
            <div class="g-content">
                <h1 class="g-inner-page__heading-txt Ff(din)">
                    <xsl:choose>
                        <xsl:when test="$group = 0">
                            Каталог
                            <!-- Описание выводится при отсутствии фильтрации по тэгам -->
                            <!--xsl:if test="count(tag) = 0 and page = 0 and description != ''">
                                <div hostcms:id="{@id}" hostcms:field="description" hostcms:entity="shop" hostcms:type="wysiwyg"><xsl:value-of disable-output-escaping="yes" select="description"/></div>
                            </xsl:if-->
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select=".//shop_group[@id=$group]/name"/>
                            <!-- Описание выводим только на первой странице -->
                            <!--xsl:if test="page = 0 and .//shop_group[@id=$group]/description != ''">
                                <div hostcms:id="{$group}" hostcms:field="description" hostcms:entity="shop_group" hostcms:type="wysiwyg"><xsl:value-of disable-output-escaping="yes" select=".//shop_group[@id=$group]/description"/></div>
                            </xsl:if-->
                        </xsl:otherwise>
                    </xsl:choose>
                </h1>
            </div>
        </div>

        <div class="g-content">
            <div class="g-grid-cat">
                <form method="get" action="{$path}{$form_tag_url}" class="g-sidebar-filter__form" id="catalog_form">
                    <xsl:if test="$group &#62; 0">
                        <div class="g-grid-cat-back">
                            <a href="{$back_url}" class="g-grid-cat-back-el g-vac">
                                <span class="g-grid-cat-back-el-in g-vac__el">
                                    <svg role="presentation" class="g-grid-cat-back-el-ico g-abs g-abs_l">
                                        <use xlink:href="#symbol-slider-prev"></use>
                                    </svg>
                                    <span class="g-grid-cat-back-el-txt">назад в&#160;<xsl:value-of select="$back_name"/></span>
                                </span>
                            </a>
                        </div>
                    </xsl:if>
                    <div class="g-grid-cat__sidebar">
                        <xsl:if test="$group &#62; 0">
                            <div class="g-grid-cat__sidebar-close">
                                <button class="g-grid-cat__sidebar-close-el js-sidebar-filter_close">
                                    <span class="g-grid-cat__sidebar-close-el-in">
                                        <svg role="presentation" class="g-grid-cat__sidebar-close-el-ico g-abs g-abs_l">
                                            <use xlink:href="#symbol-cross"></use>
                                        </svg>
                                        <span class="g-grid-cat__sidebar-close-el-txt">закрыть</span>
                                    </span>
                                </button>
                            </div>
                        </xsl:if>
                        <div class="g-grid-cat__sidebar-wrap">
                            <xsl:if test="count(//shop_group[parent_id = $group]) &#62; 0">
                                <div class="g-sidebar">
                                    <xsl:choose>
                                        <xsl:when test="$group = 0">
                                            <xsl:apply-templates select=".//shop_group[parent_id=$group]" mode="root"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <div class="g-sidebar-gr">
                                                <div class="g-sidebar-gr__list">
                                                    <xsl:apply-templates select="/shop//shop_group[parent_id=$group]" mode="subgroups">
                                                        <xsl:sort select="subgroups_total_count" order="descending" data-type="number"/>
                                                    </xsl:apply-templates>
                                                </div>
                                            </div>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                            </xsl:if>

                            <div class="g-sidebar-filter">
                                <!-- Фильтр по дополнительным свойствам товара: -->
                                <xsl:if test="count(shop_item_properties//property[filter != 0 and (type = 0 or type = 1 or type = 3 or type = 4 or type = 7 or type = 11)])">
                                    <xsl:apply-templates select="shop_item_properties//property[filter != 0 and (type = 0 or type = 1 or type = 3 or type = 4 or type = 7 or type = 11)]" mode="propertyList"/>
                                </xsl:if>

                                <xsl:if test="(count(producers/shop_producer) &#62; 0) and ($group &#62; 0)">
                                    <div class="g-sidebar-filter-gr">
                                        <div class="g-sidebar-filter-gr__title">
                                            <span class="g-sidebar-filter-gr__title-txt">Производитель</span>
                                        </div>
                                        <div class="g-sidebar-filter-gr__list">
                                            <div class="g-sidebar-filter-gr__wrap">
                                                <div class="g-checkbox-cont g-checkbox-cont_block">
                                                    <xsl:apply-templates select="producers/shop_producer"  mode="proriderz"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>

                                <div class="g-sidebar-filter-btn">
                                    <button class="g-btn g-btn_bg_red" name="filter" type="submit" value="find">
                                        <span class="g-btn__txt">Применить</span>
                                    </button>
                                    <a href="" class="g-btn g-btn_link" >
                                        <xsl:attribute name="href">
                                            <xsl:choose>
                                                <xsl:when test="$group = 0">
                                                    <xsl:value-of select="/shop/url"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="//shop_group[@id = $group]/url"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:attribute>
                                        <span class="g-btn__txt">Сбросить</span>
                                    </a>
                                </div>
                            </div>

                            <!-- блок с рекламой после фильтра!!! -->
                            <div class="g-sidebar-offer" id="advertisement_wrap"></div>
                        </div>
                    </div>
                    <div class="g-grid-cat__content">
                        <div class="cat-sorting cat-sorting_filter">
                            <div class="cat-sorting-base">
                                <div class="cat-sorting-base__name">
                                    <span class="cat-sorting-base__name-txt">Сортировать :</span>
                                </div>
                                <div class="cat-sorting-base__list">
                                    <div class="cat-sorting-item-cont">
                                        <div class="cat-sorting-item">
                                            <input id="cat_sorting_pop" class="cat-sorting-item__input sorting" type="radio" value="" data-value="4" name="cat_sorting">
                                                <xsl:if test="sorting = 4">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <label for="cat_sorting_pop" class="cat-sorting-item__label">
                                                <span class="cat-sorting-item__txt">по популярности</span>
                                            </label>
                                        </div>
                                        <div class="cat-sorting-item cat-sorting-item_price">
                                            <input id="cat_sorting_price" class="cat-sorting-item__input cat-sorting-item__input_price price_sorting" type="radio" value="" data-value="1" name="cat_sorting">
                                                <xsl:if test="sorting = 1 or sorting = 2">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <input id="cat_sorting_price_low" class="cat-sorting-item__input_price_low price_sorting" type="checkbox" value="" data-value="2" name="cat_sorting_price_low">
                                                <xsl:if test="sorting = 2">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <label for="cat_sorting_price_low" class="cat-sorting-item__label cat-sorting-item__label_dop"></label>
                                            <label for="cat_sorting_price" class="cat-sorting-item__label">
                                                <span class="cat-sorting-item__txt">по цене</span>
                                                <i class="cat-sorting-item__diz g-abs g-abs_r">
                                                    <i class="cat-sorting-item__diz-line g-abs g-abs_t"></i>
                                                <i class="cat-sorting-item__diz-line g-abs g-abs_c"></i>
                                                <i class="cat-sorting-item__diz-line g-abs g-abs_b"></i>
                                                </i>
                                            </label>
                                        </div>
                                        <div class="cat-sorting-item">
                                            <input id="cat_sorting_name" class="cat-sorting-item__input sorting" type="radio" value="" data-value="3" name="cat_sorting">
                                                <xsl:if test="sorting = 3">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <label for="cat_sorting_name" class="cat-sorting-item__label ">
                                                <span class="cat-sorting-item__txt">по названию</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cat-sorting-amount">
                                <div class="cat-sorting-amount__name">
                                    <span class="cat-sorting-amount__name-txt">Товаров на странице</span>
                                </div>
                                <div class="cat-sorting-amount__field">
                                    <div class="g-select__cont">
                                        <select class="g-select g-select_sm" name="on_page">
                                            <option value="9">
                                                <xsl:if test="on_page = 9">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                9
                                            </option>
                                            <option value="12">
                                                <xsl:if test="on_page = 12">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                12
                                            </option>
                                            <option value="15">
                                                <xsl:if test="on_page = 15">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                15
                                            </option>
                                            <option value="21">
                                                <xsl:if test="on_page = 21">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                21
                                            </option>
                                            <option value="27">
                                                <xsl:if test="on_page = 27">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                27
                                            </option>
                                            <option value="33">
                                                <xsl:if test="on_page = 33">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                33
                                            </option>
                                            <option value="66">
                                                <xsl:if test="on_page = 66">
                                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                                </xsl:if>
                                                66
                                            </option>
                                        </select>
                                        <svg role="presentation" class="g-select__ico g-abs g-abs_r">
                                            <use xlink:href="#symbol-arrow_text-down"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>

                            <button type="button" class="cat-sorting-filter g-abs g-abs_t g-abs_r js-sidebar-filter">
                                <svg role="presentation" class="cat-sorting-filter__ico">
                                    <use xlink:href="#symbol-filter"></use>
                                </svg>
                            </button>
                        </div>

                        <div class="cat-filter">
                            <div class="g-checkbox-cont">
                                <div class="g-checkbox">
                                        <label class="g-checkbox__label">
                                            <input class="g-checkbox__input checkbox_filter" type="checkbox" name="in_stock" value="yes">
                                                <xsl:if test="/shop/*[name()= 'in_stock'] = 'yes'">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-checkbox__diz g-abs g-abs_l">
                                                <svg role="presentation" class="g-checkbox__ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                            <span class="g-checkbox__txt">В наличии</span>
                                        </label>
                                    </div>
                                <xsl:if test="discounts_count">
                                    <div class="g-checkbox">
                                        <label class="g-checkbox__label">
                                            <input class="g-checkbox__input checkbox_filter" type="checkbox" name="discounts" value="all">
                                                <xsl:if test="/shop/*[name()= 'discounts'] != ''">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-checkbox__diz g-abs g-abs_l">
                                                <svg role="presentation" class="g-checkbox__ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                            <span class="g-checkbox__txt">Распродажа</span>
                                        </label>
                                    </div>
                                </xsl:if>
                                <xsl:if test="shop_item_properties//property[tag_name='sales_hit']">
                                    <xsl:variable name="nodename_hit">property_<xsl:value-of select="shop_item_properties//property[tag_name='sales_hit']/@id"/></xsl:variable>
                                    <div class="g-checkbox">
                                        <label class="g-checkbox__label">
                                            <input class="g-checkbox__input checkbox_filter" type="checkbox" name="property_{shop_item_properties//property[tag_name='sales_hit']/@id}" value="1" >
                                                <xsl:if test="/shop/*[name()= $nodename_hit] != ''">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-checkbox__diz g-abs g-abs_l">
                                                <svg role="presentation" class="g-checkbox__ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                            <span class="g-checkbox__txt">Хит продаж</span>
                                        </label>
                                    </div>
                                </xsl:if>
                                <xsl:if test="shop_item_properties//property[tag_name='new_item']">
                                    <xsl:variable name="nodename_new">property_<xsl:value-of select="shop_item_properties//property[tag_name='new_item']/@id"/></xsl:variable>
                                    <div class="g-checkbox">
                                        <label class="g-checkbox__label">
                                            <input class="g-checkbox__input checkbox_filter" type="checkbox" name="property_{shop_item_properties//property[tag_name='new_item']/@id}" value="1" >
                                                <xsl:if test="/shop/*[name()= $nodename_new] != ''">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-checkbox__diz g-abs g-abs_l">
                                                <svg role="presentation" class="g-checkbox__ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                            <span class="g-checkbox__txt">Новинки</span>
                                        </label>
                                    </div>
                                </xsl:if>
                                <xsl:if test="shop_item_properties//property[tag_name='advice']">
                                    <xsl:variable name="nodename_advice">property_<xsl:value-of select="shop_item_properties//property[tag_name='advice']/@id"/></xsl:variable>
                                    <div class="g-checkbox">
                                        <label class="g-checkbox__label">
                                            <input class="g-checkbox__input checkbox_filter" type="checkbox" name="property_{shop_item_properties//property[tag_name='advice']/@id}" value="1" >
                                                <xsl:if test="/shop/*[name()= $nodename_advice] != ''">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-checkbox__diz g-abs g-abs_l">
                                                <svg role="presentation" class="g-checkbox__ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                            <span class="g-checkbox__txt">Советуем</span>
                                        </label>
                                    </div>
                                </xsl:if>
                                <xsl:if test="shop_item_properties//property[tag_name='shop_item_offer' and filter = 5]">
                                    <div class="g-checkbox">
                                        <label class="g-checkbox__label">
                                            <input class="g-checkbox__input checkbox_filter" type="checkbox" name="actual_offer" value="all" >
                                                <xsl:if test="/shop/*[name()= 'actual_offer'] != ''">
                                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <i class="g-checkbox__diz g-abs g-abs_l">
                                                <svg role="presentation" class="g-checkbox__ico g-abs">
                                                    <use xlink:href="#symbol-check"></use>
                                                </svg>
                                            </i>
                                            <span class="g-checkbox__txt">Акция</span>
                                        </label>
                                    </div>
                                </xsl:if>
                            </div>
                        </div>
                        <div id="catalog_page_for_ajax">
                            <div class="cat-list">
                                <div class="g-grid g-grid_x3">
                                    <xsl:apply-templates select="shop_item" mode="proriderz"/>
                                </div>
                            </div>

                            <div class="cat-pag">
                                <xsl:if test="total &gt; 0 and limit &gt; 0">

                                    <xsl:variable name="count_pages" select="ceiling(total div limit)"/>

                                    <xsl:variable name="visible_pages" select="5"/>

                                    <xsl:variable name="real_visible_pages"><xsl:choose>
                                            <xsl:when test="$count_pages &lt; $visible_pages"><xsl:value-of select="$count_pages"/></xsl:when>
                                            <xsl:otherwise><xsl:value-of select="$visible_pages"/></xsl:otherwise>
                                    </xsl:choose></xsl:variable>

                                    <!-- Считаем количество выводимых ссылок перед текущим элементом -->
                                    <xsl:variable name="pre_count_page">
                                        <xsl:choose>
                                            <xsl:when test="page - (floor($real_visible_pages div 2)) &lt; 0">
                                                <xsl:value-of select="page"/>
                                            </xsl:when>
                                            <xsl:when test="($count_pages - page - 1) &lt; floor($real_visible_pages div 2)">
                                                <xsl:value-of select="$real_visible_pages - ($count_pages - page - 1) - 1"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="round($real_visible_pages div 2) = $real_visible_pages div 2">
                                                        <xsl:value-of select="floor($real_visible_pages div 2) - 1"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="floor($real_visible_pages div 2)"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>

                                    <!-- Считаем количество выводимых ссылок после текущего элемента -->
                                    <xsl:variable name="post_count_page">
                                        <xsl:choose>
                                            <xsl:when test="0 &gt; page - (floor($real_visible_pages div 2) - 1)">
                                                <xsl:value-of select="$real_visible_pages - page - 1"/>
                                            </xsl:when>
                                            <xsl:when test="($count_pages - page - 1) &lt; floor($real_visible_pages div 2)">
                                                <xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>

                                    <xsl:variable name="i">
                                        <xsl:choose>
                                            <xsl:when test="page + 1 = $count_pages"><xsl:value-of select="page - $real_visible_pages + 1"/></xsl:when>
                                            <xsl:when test="page - $pre_count_page &gt; 0"><xsl:value-of select="page - $pre_count_page"/></xsl:when>
                                            <xsl:otherwise>0</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>

                                    <div class="g-pag">
                                        <div class="g-content">
                                            <div class="g-pag__in">
                                                <div class="g-pag__list">
                                                    <xsl:call-template name="for">
                                                        <xsl:with-param name="limit" select="limit"/>
                                                        <xsl:with-param name="page" select="page"/>
                                                        <xsl:with-param name="items_count" select="total"/>
                                                        <xsl:with-param name="i" select="$i"/>
                                                        <xsl:with-param name="post_count_page" select="$post_count_page"/>
                                                        <xsl:with-param name="pre_count_page" select="$pre_count_page"/>
                                                        <xsl:with-param name="visible_pages" select="$real_visible_pages"/>
                                                    </xsl:call-template>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:if>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/shop" mode="proriderz_ajax">
        <!-- Получаем ID родительской группы и записываем в переменную $group -->
        <xsl:variable name="group" select="group"/>

        <!-- дополнение пути для action, если выбрана метка -->
        <xsl:variable name="form_tag_url"><xsl:if test="count(tag) = 1">tag/<xsl:value-of select="tag/urlencode"/>/</xsl:if></xsl:variable>

        <xsl:variable name="path">
            <xsl:choose>
                <xsl:when test="/shop//shop_group[@id=$group]/node()"><xsl:value-of select="/shop//shop_group[@id=$group]/url"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="/shop/url"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="cat-list">
            <div class="g-grid g-grid_x3">
                <xsl:apply-templates select="shop_item" mode="proriderz"/>
                <xsl:if test="sorting != ''">
                    <input type="hidden" name="sorting" value="{sorting}"/>
                </xsl:if>
            </div>
        </div>

        <div class="cat-pag">
            <xsl:if test="total &gt; 0 and limit &gt; 0">

                <xsl:variable name="count_pages" select="ceiling(total div limit)"/>

                <xsl:variable name="visible_pages" select="5"/>

                <xsl:variable name="real_visible_pages"><xsl:choose>
                        <xsl:when test="$count_pages &lt; $visible_pages"><xsl:value-of select="$count_pages"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$visible_pages"/></xsl:otherwise>
                </xsl:choose></xsl:variable>

                <!-- Считаем количество выводимых ссылок перед текущим элементом -->
                <xsl:variable name="pre_count_page">
                    <xsl:choose>
                        <xsl:when test="page - (floor($real_visible_pages div 2)) &lt; 0">
                            <xsl:value-of select="page"/>
                        </xsl:when>
                        <xsl:when test="($count_pages - page - 1) &lt; floor($real_visible_pages div 2)">
                            <xsl:value-of select="$real_visible_pages - ($count_pages - page - 1) - 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="round($real_visible_pages div 2) = $real_visible_pages div 2">
                                    <xsl:value-of select="floor($real_visible_pages div 2) - 1"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="floor($real_visible_pages div 2)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <!-- Считаем количество выводимых ссылок после текущего элемента -->
                <xsl:variable name="post_count_page">
                    <xsl:choose>
                        <xsl:when test="0 &gt; page - (floor($real_visible_pages div 2) - 1)">
                            <xsl:value-of select="$real_visible_pages - page - 1"/>
                        </xsl:when>
                        <xsl:when test="($count_pages - page - 1) &lt; floor($real_visible_pages div 2)">
                            <xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:variable name="i">
                    <xsl:choose>
                        <xsl:when test="page + 1 = $count_pages"><xsl:value-of select="page - $real_visible_pages + 1"/></xsl:when>
                        <xsl:when test="page - $pre_count_page &gt; 0"><xsl:value-of select="page - $pre_count_page"/></xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <div class="g-pag">
                    <div class="g-content">
                        <div class="g-pag__in">
                            <div class="g-pag__list">
                                <xsl:call-template name="for">
                                    <xsl:with-param name="limit" select="limit"/>
                                    <xsl:with-param name="page" select="page"/>
                                    <xsl:with-param name="items_count" select="total"/>
                                    <xsl:with-param name="i" select="$i"/>
                                    <xsl:with-param name="post_count_page" select="$post_count_page"/>
                                    <xsl:with-param name="pre_count_page" select="$pre_count_page"/>
                                    <xsl:with-param name="visible_pages" select="$real_visible_pages"/>
                                </xsl:call-template>
                            </div>
                        </div>
                    </div>
                </div>
            </xsl:if>
        </div>


    </xsl:template>

    <xsl:template match="shop_group" mode="root">
        <xsl:variable name="group_id" select="@id"/>
        <div class="g-sidebar-gr">
            <div class="g-sidebar-gr__title">
                <a href="{url}" class="g-sidebar-gr__title-link">
                    <span class="g-sidebar-gr__title-txt Ff(din) Fw(b)"><xsl:value-of select="name"/></span>
                </a>
            </div>
            <xsl:if test="count(shop_group) &#62; 0">
                <div class="g-sidebar-gr__list">
                    <xsl:apply-templates select="/shop//shop_group[parent_id=$group_id]" mode="subgroups_root">
                        <xsl:sort select="subgroups_total_count" order="descending" data-type="number"/>
                    </xsl:apply-templates>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="/shop//shop_group" mode="subgroups_root">
        <xsl:if test="position() = 10">
            <xsl:text disable-output-escaping="yes" >&#60;div class="g-sidebar-gr-more js-accordion"&#62;</xsl:text>
            <button class="g-sidebar-gr-more__title js-accordion__btn" type="button">
                <div class="g-sidebar-gr-more__title-in">
                    <span class="g-sidebar-gr-more__title-txt" data-open="ЕЩЁ" data-close="Скрыть"></span>
                    <svg role="presentation" class="g-sidebar-gr-more__title-ico g-abs g-abs_r">
                        <use xlink:href="#symbol-arrow_text-down"></use>
                    </svg>
                </div>
            </button>
            <xsl:text disable-output-escaping="yes" >&#60;div class="g-sidebar-gr-more__list js-accordion__panel"&#62;</xsl:text>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="count(shop_group) = 0">
                <div class="g-sidebar-item">
                    <div class="g-sidebar-item__title ">
                        <a href="{url}" class="g-sidebar-item__title-link" title="{name}">
                            <span class="g-sidebar-item__title-txt"><xsl:value-of select="name"/></span>
                        </a>
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="g-sidebar-item js-accordion">
                    <div class="g-sidebar-item__title g-sidebar-item__title_arrow">
                        <a href="{url}" class="g-sidebar-item__title-link" title="{name}">
                            <span class="g-sidebar-item__title-txt"><xsl:value-of select="name"/></span>
                        </a>
                        <button class="g-sidebar-item__title-arrow g-abs g-abs_t g-abs_r js-accordion__btn" type="button">
                            <span class="g-sidebar-item__title-arrow-ico g-abs"></span>
                        </button>
                    </div>
                    <div class="g-sidebar-item-dop js-accordion__panel">
                        <div class="g-sidebar-item-dop__wrap">
                            <xsl:for-each select="shop_group">
                                <a href="{url}" class="g-sidebar-item-dop__link ">
                                    <span class="g-sidebar-item-dop__txt "><xsl:value-of select="name"/></span>
                                </a>
                            </xsl:for-each>
                        </div>
                    </div>
                </div>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if test="position() = last()">
            <xsl:text disable-output-escaping="yes" >&#60;/div&#62;</xsl:text>
            <xsl:text disable-output-escaping="yes" >&#60;/div&#62;</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="/shop//shop_group" mode="subgroups">
        <xsl:choose>
            <xsl:when test="count(shop_group) = 0">
                <div class="g-sidebar-item">
                    <div class="g-sidebar-item__title ">
                        <a href="{url}" class="g-sidebar-item__title-link" title="{name}">
                            <span class="g-sidebar-item__title-txt"><xsl:value-of select="name"/></span>
                        </a>
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="g-sidebar-item js-accordion">
                    <div class="g-sidebar-item__title g-sidebar-item__title_arrow">
                        <a href="{url}" class="g-sidebar-item__title-link" title="{name}">
                            <span class="g-sidebar-item__title-txt"><xsl:value-of select="name"/></span>
                        </a>
                        <button class="g-sidebar-item__title-arrow g-abs g-abs_t g-abs_r js-accordion__btn" type="button">
                            <span class="g-sidebar-item__title-arrow-ico g-abs"></span>
                        </button>
                    </div>
                    <div class="g-sidebar-item-dop js-accordion__panel">
                        <div class="g-sidebar-item-dop__wrap">
                            <xsl:for-each select="shop_group">
                                <a href="{url}" class="g-sidebar-item-dop__link ">
                                    <span class="g-sidebar-item-dop__txt "><xsl:value-of select="name"/></span>
                                </a>
                            </xsl:for-each>
                        </div>
                    </div>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="shop_group" mode="breadCrumbs">
        <xsl:variable name="parent_id" select="parent_id"/>

        <!-- Выбираем рекурсивно вышестоящую группу -->
        <xsl:apply-templates select="//shop_group[@id=$parent_id]" mode="breadCrumbs"/>

        <a href="{url}" class="g-card-cat-el">
            <span class="g-card-cat-el__txt"><xsl:value-of select="name"/></span>
        </a>

    </xsl:template>

    <xsl:template match="shop_item" mode="proriderz">
        <xsl:variable name="group" select="shop_group_id"/>
        <xsl:variable name="shop_item_id" select="@id"/>

        <xsl:variable name="item_to_cart_id">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="modifications/shop_item[position() = 1]/@id"/>
                    <!--xsl:for-each select="modifications/shop_item">
                        <xsl:sort select="id" order="ascending" data-type="number" />
                        <xsl:if test="position() = 1">
                            <xsl:value-of select="@id"/>
                        </xsl:if>
                    </xsl:for-each-->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@id"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_to_cart_rest">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <xsl:value-of select="sum(modifications/shop_item/rest)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="rest"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_to_cart_price">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <!--xsl:value-of select="modifications/shop_item[position() = 1]/price"/-->
                    <xsl:value-of select="modifications/shop_item[@id = $item_to_cart_id]/price"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="price"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_to_cart_discount">
            <xsl:choose>
                <xsl:when test="count(modifications/shop_item) &#62; 0">
                    <!--xsl:value-of select="modifications/shop_item[position() = 1]/discount"/-->
                    <xsl:value-of select="modifications/shop_item[@id = $item_to_cart_id]/discount"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="discount"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="g-grid__el">
            <div class="g-card">
                <div class="g-card-cat">
                    <xsl:apply-templates select="/shop//shop_group[@id=$group]" mode="breadCrumbs"/>
                    <!-- вывести тип мотоцикла, если есть в группе это свойство -->
                    <xsl:for-each select="property_value[tag_name='mototype_flag']">
                        <xsl:variable name="value" select="value"/>
                        <xsl:variable name="property_id" select="property_id"/>
                        <xsl:variable name="property_val_id" select="/shop/shop_item_properties/.//property[@id = $property_id]/list/list_item[value = $value]/@id"/>

                        <a href="{/shop/.//shop_group[@id = $group]/url}?property_{property_id}[]={$property_val_id}&#38;filter=1" class="g-card-cat-el">
                            <span class="g-card-cat-el__txt"><xsl:value-of select="value"/></span>
                        </a>
                    </xsl:for-each>
                </div>
                <div class="g-card-img">
                    <div class="g-card-img-label g-abs g-abs_t">
                        <xsl:if test="$item_to_cart_discount &#62; 0">
                            <div class="goods-label-el goods-label-el_red">
                                <span class="goods-label-el__txt">-&#160;<xsl:value-of select="format-number($item_to_cart_discount, '#####0', 'my')"/>&#160;руб.</span>
                            </div>
                        </xsl:if>
                        <xsl:if test="property_value[tag_name='sales_hit']/value &#62; 0">
                            <div class="goods-label-el goods-label-el_green">
                                <span class="goods-label-el__txt">Хит продаж</span>
                            </div>
                        </xsl:if>
                        <xsl:if test="property_value[tag_name='advice']/value &#62; 0">
                            <div class="goods-label-el goods-label-el_green">
                                <span class="goods-label-el__txt">Советуем</span>
                            </div>
                        </xsl:if>
                        <xsl:if test="property_value[tag_name='new_item']/value &#62; 0">
                            <div class="goods-label-el goods-label-el_green">
                                <span class="goods-label-el__txt">Новинка</span>
                            </div>
                        </xsl:if>
                        <xsl:if test="count(property_value[tag_name='shop_item_offer']/informationsystem_item[actual_offer = 1]) &#62; 0">
                            <div class="goods-label-el goods-label-el_green">
                                <span class="goods-label-el__txt">Акция</span>
                            </div>
                        </xsl:if>
                    </div>
                    <a href="{url}" class="g-card-img__wrap g-abs">
                        <xsl:choose>
                            <xsl:when test="image_small != ''">
                                <img class="g-card-img__in g-abs" src="{dir}{image_small}" alt="{name}" />
                            </xsl:when>
                            <xsl:otherwise>
                                <img class="g-card-img__in g-abs" src="/templates/template13/images/default_img_min.jpg" alt="{name}" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </a>

                    <xsl:if test="count(property_value[tag_name='color' and value != '']) &#62; 0">
                        <div class="g-card-img-color g-abs g-abs_b">
                            <div class="g-card-img-color__list g-color-ch__list">

                                <xsl:for-each select="property_value[tag_name='color'][value != '']">
                                    <xsl:variable name="current_value" select="value"/>
                                    <div class="g-color-ch">
                                        <label class="g-color-ch__label">
                                            <input class="g-color-ch__input js-card-color" type="radio" name="g-color-ch-id_4" value="" data-src="" />
                                            <i class="g-color-ch__diz" style="background-color: {description}"></i>
                                        </label>
                                    </div>
                                </xsl:for-each>
                            </div>
                        </div>
                    </xsl:if>
                </div>
                <a href="{url}" class="g-card-title">
                    <span class="g-card-title__txt"><xsl:value-of select="name"/></span>
                </a>

                <div class="g-card-feature">
                    <span class="g-card-feature__txt">
                        <xsl:for-each select="modifications/shop_item">
                            <xsl:sort select="price" order="ascending" data-type="number" />
                            <xsl:value-of select="normalize-space(name)"/><xsl:if test="position() != last()">,&#160;</xsl:if>
                        </xsl:for-each>
                    </span>
                </div>

                <div class="g-card-info">
                    <xsl:choose>
                        <xsl:when test="$item_to_cart_rest &#60; 4 and $item_to_cart_rest != 0">
                            <div class="g-card-info-quantity">
                                <span class="g-card-info-quantity__txt g-orange">Мало</span>
                            </div>
                        </xsl:when>
                        <xsl:when test="$item_to_cart_rest &#62; 3">
                            <div class="g-card-info-quantity">
                                <span class="g-card-info-quantity__txt g-green">Достаточно</span>
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="g-card-info-quantity g-card-info-quantity_red">
                                <span class="g-card-info-quantity__txt g-red">Нет в наличии</span>
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>
                    <a href="{/shop/.//shop_group[@id = $group]/url}?producer_id={shop_producer_id}&#38;filter=find" class="g-card-info-brand"> <!-- переход в каталог по производителю -->
                        <span class="g-card-info-brand__txt"><xsl:value-of select="shop_producer/name"/></span>
                    </a>
                </div>
                <div class="g-card-bottom">
                    <div class="g-card-bottom__left">
                        <div class="g-card-cost">
                            <div class="g-card-cost__in">
                                <xsl:if test="$item_to_cart_discount != 0">
                                    <span class="g-card-cost__txt g-card-cost__txt_old">
                                        <span class="g-card-cost-old__txt-num">
                                            <xsl:value-of select="format-number($item_to_cart_price + $item_to_cart_discount, '### ##0', 'my')"/>
                                        </span>
                                    </span>
                                </xsl:if>
                                <span class="g-card-cost__txt">
                                    <xsl:if test="$item_to_cart_discount != 0">
                                        <xsl:attribute name="class">g-card-cost__txt g-card-cost__txt_new</xsl:attribute>
                                    </xsl:if>
                                    <span class="g-card-cost__txt-num"><xsl:value-of select="format-number($item_to_cart_price, '### ##0', 'my')"/>&#160;</span>
                                    <svg role="presentation" class="g-card-cost__txt-currency g-rub">
                                        <use xlink:href="#symbol-rub"></use>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="g-card-bottom__right">
                        <xsl:choose>
                            <xsl:when test="$item_to_cart_rest = 0">
                                <div class="g-card-add-cont">
                                    <a href="{url}" class="g-card-link">
                                        <span class="g-card-add__txt">Смотреть</span>
                                    </a>
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="count(modifications/shop_item) &#62; 0">
                                        <div class="g-card-add-cont">
                                            <a href="{url}" class="g-card-add">
                                                <span class="g-card-add__txt">Выбрать</span>
                                                <svg role="presentation" class="g-card-add__ico g-abs g-abs_r">
                                                    <use xlink:href="#symbol-cart_card"></use>
                                                </svg>
                                            </a>
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <div class="g-card-add-cont">
                                            <div class="g-quan js-quan">
                                                <button type="button" class="g-quan__btn g-quan__btn_minus  g-abs g-abs_l  js-quan__minus">
                                                    <i class="g-quan__btn-ico g-abs">
                                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                                    </i>
                                                </button>
                                                <input id="quantity_{$item_to_cart_id}" class="g-quan__num js-quan__num" autocomplete="off" data-max="10" type="tel" value="1"/>
                                                <button type="button" class="g-quan__btn g-quan__btn_plus g-abs g-abs_r is-active  js-quan__plus">
                                                    <i class="g-quan__btn-ico g-abs">
                                                        <i class="g-quan__btn-ico-line g-quan__btn-ico-line_h g-abs"></i>
                                                    <i class="g-quan__btn-ico-line g-quan__btn-ico-line_v g-abs"></i>
                                                    </i>
                                                </button>
                                            </div>

                                            <button type="button" class="g-card-add g-card-add_quan" onclick="return $.addIntoCart('{/shop/url}cart/', {$item_to_cart_id}, 1)" >
                                                <svg role="presentation" class="g-card-add__ico g-abs g-abs_r">
                                                    <use xlink:href="#symbol-cart_card"></use>
                                                </svg>
                                            </button>

                                            <div class="g-card-add-mess g-abs g-vac">
                                                <div class="g-card-add-mess__in g-vac__el">
                                                    <div class="g-card-add-mess__txt t-h5">
                                                        Товар в корзине
                                                    </div>
                                                    <i class="g-card-add-mess__diz g-abs g-abs_r">
                                                        <svg role="presentation" class="g-card-add-mess__ico g-abs">
                                                            <use xlink:href="#symbol-check"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="producers/shop_producer" mode="proriderz">
        <div class="g-checkbox">
            <label class="g-checkbox__label">
                <input class="g-checkbox__input checkbox_producer" type="checkbox" name="producer_id[]" value="{@id}" >
                    <xsl:if test="/shop/producer_id = @id">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
                <i class="g-checkbox__diz g-abs g-abs_l">
                    <svg role="presentation" class="g-checkbox__ico g-abs">
                        <use xlink:href="#symbol-check"></use>
                    </svg>
                </i>
                <span class="g-checkbox__txt"><xsl:value-of select="name"/></span>
            </label>
        </div>
    </xsl:template>

    <!-- Шаблон для списка товаров для сравнения -->
    <xsl:template match="compare_items/compare_item">
        <xsl:variable name="var_compare_id" select="."/>
        <tr>
            <td>
                <input type="checkbox" name="del_compare_id_{compare_item_id}" id="id_del_compare_id_{compare_item_id}"/>
            </td>
            <td>
                <a href="{/shop/url}{compare_item_url}{compare_url}/">
                    <xsl:value-of select="compare_name"/>
                </a>
            </td>
        </tr>
    </xsl:template>

    <!-- Шаблон для фильтра по дополнительным свойствам -->
    <xsl:template match="property" mode="propertyList">
        <xsl:variable name="nodename">property_<xsl:value-of select="@id"/></xsl:variable>
        <xsl:variable name="nodename_from">property_<xsl:value-of select="@id"/>_from</xsl:variable>
        <xsl:variable name="nodename_to">property_<xsl:value-of select="@id"/>_to</xsl:variable>
        <!-- Не флажок -->
            <xsl:if test="filter != 5">
                <div class="g-sidebar-filter-gr">

                    <div class="g-sidebar-filter-gr__title">
                        <span class="g-sidebar-filter-gr__title-txt"><xsl:value-of select="name"/></span>
                    </div>

                    <div class="g-sidebar-filter-gr__list">
                        <div class="g-sidebar-filter-gr__wrap">
                            <xsl:choose>
                                <xsl:when test="tag_name='color'">
                                    <div class="g-sidebar-filter__color">
                                        <div class="g-color-ch__list">
                                            <xsl:apply-templates select="list/list_item" mode="color"/>
                                        </div>
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <!-- Отображаем поле ввода -->
                                        <xsl:when test="filter = 1">
                                            <!--br/>
                                            <input type="text" name="property_{@id}">
                                                <xsl:if test="/shop/*[name()=$nodename] != ''">
                                                    <xsl:attribute name="value"><xsl:value-of select="/shop/*[name()=$nodename]"/></xsl:attribute>
                                                </xsl:if>
                                            </input-->
                                        </xsl:when>
                                        <!-- Отображаем список -->
                                        <xsl:when test="filter = 2">
                                            <!-- <br/> -->
                                            <!--select class="form-control property-list-select" name="property_{@id}">
                                                <option value="0">...</option>
                                                <xsl:apply-templates select="list/list_item"/>
                                            </select-->
                                        </xsl:when>
                                        <!-- Отображаем переключатели -->
                                        <xsl:when test="filter = 3">
                                            <!--br/>
                                            <div class="propertyInput">
                                                <input type="radio" name="property_{@id}" value="0" id="id_prop_radio_{@id}_0"></input>
                                                <label for="id_prop_radio_{@id}_0">Любой вариант</label>
                                                <xsl:apply-templates select="list/list_item"/>
                                            </div-->
                                        </xsl:when>
                                        <!-- Отображаем флажки -->
                                        <xsl:when test="filter = 4">
                                            <div class="g-checkbox-cont g-checkbox-cont_block">
                                                <xsl:apply-templates select="list/list_item"/>
                                            </div>
                                        </xsl:when>
                                        <!-- Отображаем флажок -->
                                        <xsl:when test="filter = 5">
                                            <!--input type="checkbox" name="property_{@id}" id="property_{@id}" style="padding-top:4px">
                                                <xsl:if test="/shop/*[name()=$nodename] != ''">
                                                    <xsl:attribute name="checked"><xsl:value-of select="/shop/*[name()=$nodename]"/></xsl:attribute>
                                                </xsl:if>
                                            </input>
                                            <label for="property_{@id}">
                                                <xsl:value-of select="name"/><xsl:text> </xsl:text>
                                            </label-->
                                        </xsl:when>
                                        <!-- Отображение полей "от и до" -->
                                        <xsl:when test="filter = 6">
                                            <div class="g-grid g-grid_x2">
                                                <input name="property_{@id}_from_original" value="{/shop/*[name()=$nodename_from]}" hidden="hidden" />
                                                <input name="property_{@id}_to_original" value="{/shop/*[name()=$nodename_to]}" hidden="hidden" />
                                                <div class="g-grid__el">
                                                    <div class="g-form-item__field">
                                                        <input class="g-input" type="tel" placeholder="от 0.5" name="property_{@id}_from" size="5" value="{/shop/*[name()=$nodename_from]}" />
                                                    </div>
                                                </div>
                                                <div class="g-grid__el">
                                                    <div class="g-form-item__field">
                                                        <input class="g-input" type="tel" placeholder="до 1000" name="property_{@id}_to" size="5" value="{/shop/*[name()=$nodename_to]}"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </xsl:when>
                                        <!-- Отображаем список с множественным выбором-->
                                        <xsl:when test="filter = 7">
                                            <!--br/>
                                            <select name="property_{@id}[]" multiple="multiple">
                                                <xsl:apply-templates select="list/list_item"/>
                                            </select-->
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>

                        </div>
                    </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template match="list/list_item" mode="color">
        <xsl:variable name="nodename">property_<xsl:value-of select="../../@id"/></xsl:variable>

        <div class="g-color-ch g-color-ch_big">
            <label class="g-color-ch__label">
                <input class="g-color-ch__input checkbox_color" type="checkbox" name="property_{../../@id}[]" id="property_{../../@id}_{@id}" value="{@id}" title="{value}">
                    <xsl:if test="/shop/*[name()=$nodename] = @id">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
                <i class="g-color-ch__diz" style="background-color: {description}">
                    <svg role="presentation" class="g-color-ch__diz-ico g-abs">
                        <use xlink:href="#symbol-check"></use>
                    </svg>
                </i>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="list/list_item">
        <xsl:if test="../../filter = 2">
            <!-- Отображаем список -->
            <xsl:variable name="nodename">property_<xsl:value-of select="../../@id"/></xsl:variable>
            <option value="{@id}">
            <xsl:if test="/shop/*[name()=$nodename] = @id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                <xsl:value-of disable-output-escaping="yes" select="value"/>
            </option>
        </xsl:if>
        <xsl:if test="../../filter = 3">
            <!-- Отображаем переключатели -->
            <xsl:variable name="nodename">property_<xsl:value-of select="../../@id"/></xsl:variable>
            <br/>
            <input type="radio" name="property_{../../@id}" value="{@id}" id="id_property_{../../@id}_{@id}">
                <xsl:if test="/shop/*[name()=$nodename] = @id">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
                <label for="id_property_{../../@id}_{@id}">
                    <xsl:value-of disable-output-escaping="yes" select="value"/>
                </label>
            </input>
        </xsl:if>
        <xsl:if test="../../filter = 4">
            <!-- Отображаем флажки -->
            <xsl:variable name="nodename">property_<xsl:value-of select="../../@id"/></xsl:variable>

            <div class="g-checkbox">
                <label class="g-checkbox__label">
                    <input class="g-checkbox__input checkbox_for_group" type="checkbox" value="{@id}" name="property_{../../@id}[]" id="property_{../../@id}_{@id}">
                        <xsl:if test="/shop/*[name()=$nodename] = @id">
                            <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                    </input>
                    <i class="g-checkbox__diz g-abs g-abs_l">
                        <svg role="presentation" class="g-checkbox__ico g-abs">
                            <use xlink:href="#symbol-check"></use>
                        </svg>
                    </i>
                    <span class="g-checkbox__txt"><xsl:value-of disable-output-escaping="yes" select="value"/></span>
                </label>
            </div>
        </xsl:if>
        <xsl:if test="../../filter = 7">
            <!-- Отображаем список -->
            <xsl:variable name="nodename">property_<xsl:value-of select="../../@id"/></xsl:variable>
            <option value="{@id}">
                <xsl:if test="/shop/*[name()=$nodename] = @id">
                    <xsl:attribute name="selected">
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of disable-output-escaping="yes" select="value"/>
            </option>
        </xsl:if>
    </xsl:template>

    <!-- Метки для товаров -->
    <xsl:template match="tag">
        <a href="{/shop/url}tag/{urlencode}/" class="tag">
            <xsl:value-of select="tag_name"/>
        </a>
    <xsl:if test="position() != last()"><xsl:text>, </xsl:text></xsl:if>
    </xsl:template>

    <!-- Цикл для вывода строк ссылок -->
    <xsl:template name="for">

        <xsl:param name="limit"/>
        <xsl:param name="page"/>
        <xsl:param name="pre_count_page"/>
        <xsl:param name="post_count_page"/>
        <xsl:param name="i" select="0"/>
        <xsl:param name="items_count"/>
        <xsl:param name="visible_pages"/>

        <xsl:variable name="n" select="ceiling($items_count div $limit)"/>

        <xsl:variable name="start_page">
            <xsl:choose>
                <xsl:when test="$page + 1 = $n"><xsl:value-of select="$page - $visible_pages + 1"/></xsl:when>
                <xsl:when test="$page - $pre_count_page &gt; 0"><xsl:value-of select="$page - $pre_count_page"/></xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="advice_property_name">property_<xsl:value-of select="/shop/shop_item_properties//property[tag_name='advice']/@id"/></xsl:variable>
        <xsl:variable name="hit_property_name">property_<xsl:value-of select="/shop/shop_item_properties//property[tag_name='sales_hit']/@id"/></xsl:variable>
        <xsl:variable name="new_property_name">property_<xsl:value-of select="/shop/shop_item_properties//property[tag_name='new_item']/@id"/></xsl:variable>

        <!-- Передаем фильтр -->
        <xsl:variable name="filter">
            <xsl:if test="/shop/filter/node()">
                ?filter=find&amp;
                <xsl:if test="/shop/sorting &#62; 0">sorting=<xsl:value-of select="/shop/sorting"/>&amp;</xsl:if>
                <xsl:if test="/shop/price_from != ''">price_from=<xsl:value-of select="/shop/price_from"/>&amp;</xsl:if>
                <xsl:if test="/shop/price_to != ''">price_to=<xsl:value-of select="/shop/price_to"/>&amp;</xsl:if>
                <xsl:for-each select="/shop/*">
                    <xsl:variable name="name" select="name()"/>
                    <xsl:if test="starts-with($name, 'property_')">
                        <xsl:choose>
                            <xsl:when test="$name = $advice_property_name">
                                <xsl:value-of select="$name"/>=<xsl:value-of select="."/>&amp;
                            </xsl:when>
                            <xsl:when test="$name = $hit_property_name">
                                <xsl:value-of select="$name"/>=<xsl:value-of select="."/>&amp;
                            </xsl:when>
                            <xsl:when test="$name = $new_property_name">
                                <xsl:value-of select="$name"/>=<xsl:value-of select="."/>&amp;
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$name"/>[]=<xsl:value-of select="."/>&amp;
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:for-each>
                <xsl:for-each select="/shop/*">
                    <xsl:if test="starts-with(name(), 'producer_id')"><xsl:value-of select="name()"/>[]=<xsl:value-of select="."/>&amp;</xsl:if>
                </xsl:for-each>
                <xsl:if test="/shop/discounts != ''">discounts=all&amp;</xsl:if>
                <xsl:if test="/shop/in_stock != ''">in_stock=yes&amp;</xsl:if>
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="on_page">
            <xsl:if test="/shop/on_page/node() and /shop/on_page > 0">
                <xsl:choose>
                    <xsl:when test="/shop/filter/node()"></xsl:when>
                    <xsl:otherwise>?</xsl:otherwise>
                </xsl:choose>
                on_page=<xsl:value-of select="/shop/on_page"/>
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$items_count &gt; $limit and ($page + $post_count_page + 1) &gt; $i">

            <!-- Заносим в переменную $group идентификатор текущей группы -->
            <xsl:variable name="group" select="/shop/group"/>

            <!-- Путь для тэга -->
            <xsl:variable name="tag_path"><xsl:if test="count(/shop/tag) != 0">tag/<xsl:value-of select="/shop/tag/urlencode"/>/</xsl:if></xsl:variable>

            <!-- Путь для сравнения товара -->
            <xsl:variable name="shop_producer_path"><xsl:if test="count(/shop/shop_producer)">producer-<xsl:value-of select="/shop/shop_producer/@id"/>/</xsl:if></xsl:variable>

            <!-- Определяем группу для формирования адреса ссылки -->
            <xsl:variable name="group_link">
                <xsl:choose>
                    <xsl:when test="$group != 0"><xsl:value-of select="/shop//shop_group[@id=$group]/url"/></xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="/shop/url"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!-- Определяем адрес ссылки -->
            <xsl:variable name="number_link">
                <xsl:if test="$i != 0">page-<xsl:value-of select="$i + 1"/>/</xsl:if>
            </xsl:variable>

            <!-- Выводим ссылку на первую страницу -->
            <xsl:if test="$page - $pre_count_page &gt; 0 and $i = $start_page">
                <a href="{$group_link}{$tag_path}{$shop_producer_path}{$filter}{$on_page}" class="g-pag__link g-vac">
                    <span class="g-pag__txt g-vac__el">1</span>
                </a>
                <div class="g-pag__split g-vac">
                    <span class="g-pag__txt g-vac__el">…</span>
                </div>
            </xsl:if>

            <!-- Ссылка на предыдущую страницу для Ctrl + влево -->
            <xsl:if test="$page != 0 and $i = $page">
                <xsl:variable name="prev_number_link">
                    <xsl:if test="$page &gt; 1">page-<xsl:value-of select="$i"/>/</xsl:if>
                </xsl:variable>
                <a href="{$group_link}{$prev_number_link}{$tag_path}{$shop_producer_path}{$filter}{$on_page}" id="id_prev"></a>
            </xsl:if>

            <!-- Ставим ссылку на страницу-->
            <xsl:if test="$i != $page">
                <xsl:if test="($page - $pre_count_page) &lt;= $i and $i &lt; $n">
                    <!-- Выводим ссылки на видимые страницы -->
                    <a href="{$group_link}{$number_link}{$tag_path}{$shop_producer_path}{$filter}{$on_page}" class="g-pag__link g-vac">
                        <span class="g-pag__txt g-vac__el"><xsl:value-of select="$i + 1"/></span>
                    </a>
                </xsl:if>

                <!-- Выводим ссылку на последнюю страницу -->
                <xsl:if test="$i+1 &gt;= ($page + $post_count_page + 1) and $n &gt; ($page + 1 + $post_count_page)">
                    <!-- Выводим ссылку на последнюю страницу -->
                    <div class="g-pag__split g-vac">
                        <span class="g-pag__txt g-vac__el">…</span>
                    </div>
                    <a href="{$group_link}page-{$n}/{$tag_path}{$shop_producer_path}{$filter}{$on_page}" class="g-pag__link g-vac">
                        <span class="g-pag__txt g-vac__el"><xsl:value-of select="$n"/></span>
                    </a>
                    <a href="{$group_link}page-{$page+2}/{$tag_path}{$shop_producer_path}{$filter}{$on_page}" id="id_next" class="g-pag__next g-vac">
                        <span class="g-pag__txt g-vac__el">Дальше</span>
                        <svg role="presentation" aria-hidden="true" class="g-pag__next-ico g-abs g-abs_r">
                            <use xlink:href="#symbol-slider-next"></use>
                        </svg>
                    </a>
                </xsl:if>
            </xsl:if>

            <!-- Не ставим ссылку на страницу-->
            <xsl:if test="$i = $page">
                <div class="g-pag__current g-vac">
                    <span class="g-pag__txt g-vac__el"><xsl:value-of select="$i+1"/></span>
                </div>
            </xsl:if>

            <!-- Рекурсивный вызов шаблона. НЕОБХОДИМО ПЕРЕДАВАТЬ ВСЕ НЕОБХОДИМЫЕ ПАРАМЕТРЫ! -->
            <xsl:call-template name="for">
                <xsl:with-param name="i" select="$i + 1"/>
                <xsl:with-param name="limit" select="$limit"/>
                <xsl:with-param name="page" select="$page"/>
                <xsl:with-param name="items_count" select="$items_count"/>
                <xsl:with-param name="pre_count_page" select="$pre_count_page"/>
                <xsl:with-param name="post_count_page" select="$post_count_page"/>
                <xsl:with-param name="visible_pages" select="$visible_pages"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>