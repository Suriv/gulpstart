<?php
///////////////
// constants //
///////////////

$iInformationsystemId = 29;

if ($_POST) {

  $aResult = array(
    'status' => false,
    'error' => false
  );

  // get form data
  $name = Core_Array::getPost('name', false);
  $email = Core_Array::getPost('email', false);
  $phone = Core_Array::getPost('phone', false);

  if ($name && $phone) {
    if (Core_Array::getPost('recaptcha')) {
      // g-captcha
      $gRecaptchaResponse = Core_Array::getPost('recaptcha');
      if($curl = curl_init()) {
        curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "secret=" . RECAPTCHA_SECRET_KEY . "&response=" . $gRecaptchaResponse);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $gRecaptchaResult = json_decode($out); //success
        curl_close($curl);
      }
      if (!$gRecaptchaResult->success) {
        $aResult['error'] = 'Проверка данных капчи вернула отрицательный результат!';
        $aResult['status'] = false;
        echo json_encode($aResult);
        exit();
      } else {
        $text = "<p><b>Имя:</b> $name</p><p><b>E-mail:</b> $email</p><p><b>Телефон:</b> $phone</p>";
        $new_item = Core_Entity::factory('Informationsystem_Item');
        $new_item->informationsystem_id = $iInformationsystemId;
        $new_item->name = date('Y-m-d H:i:s');



        $new_item->description = $text;
        $new_item->save();
        $new_item_id = $new_item->id;
        if (!$new_item_id) {
          $aResult['error'] = "Ошибка сохранения элемента инфосистемы";
          $aResult['status'] = false;
          echo json_encode($aResult);
          exit();
        } else {
          // set property values
          $oPropertyName = Core_Entity::factory('Property')->getByTag_name('feedback_name');
          $oPropertyEmail = Core_Entity::factory('Property')->getByTag_name('feedback_email');
          $oPropertyPhone = Core_Entity::factory('Property')->getByTag_name('feedback_phone');

          $oPropertyValueName = $oPropertyName->createNewValue($new_item_id);
          $oPropertyValueName->value = $name;
          $oPropertyValueName->save();

          $oPropertyValueEmail = $oPropertyEmail->createNewValue($new_item_id);
          $oPropertyValueEmail->value = $email;
          $oPropertyValueEmail->save();

          $oPropertyValuePhone = $oPropertyPhone->createNewValue($new_item_id);
          $oPropertyValuePhone->value = $name;
          $oPropertyValuePhone->save();
          //mail
          Core_Mail::instance()
            ->to(FEEDBACK_EMAIL)
            ->from($email)
            ->subject("Новая заявка на просмотр квартиры")
            ->message($text)
            ->contentType('text/html')
            ->header('X-HostCMS-Reason', 'Alert')
            ->header('Precedence', 'bulk')
            // ->attach(array(
            //  'filepath' => $include_file,
            //  'filename' => $file,
            //  ))
            ->send();
          $aResult['error'] = false;
          $aResult['status'] = true;
          echo json_encode($aResult);
          exit();
        }

      }
    } else {
      $aResult['error'] = 'Отсутствуют данные каптчи!';
      $aResult['status'] = false;
      echo json_encode($aResult);
      exit();
    }

  } else {
    $aResult['error'] = 'Заполните все обязательные поля!';
    $aResult['status'] = false;
    echo json_encode($aResult);
    exit();
  }
} else {
  // exit();
}