/*
 * aa-form-verification.js
 *
 * Верификация форм
 *
 * зависимости:
 *    /js/jquery.validate.js
 *    /js/localization/messages_ru.min.js
 *
 */

$(function(){
    // Привязка к кнопкам отправки форм, у которых нет собственного обработчика
    // onclick() стандартного обработчика плагина jquery.form
    // $("input[type='submit']:not([onclick]), button[type='submit']:not([onclick])").on("click", function(){
    //  $(this).closest('form').submit();
    //  return false;
    // });

    // Установка дефолтовых значений валидатора
    $.validator.setDefaults({
        debug: true,
        // submitHandler: function(form) {
        //  $.loadingScreen('show');
        //  $form = $(form);
        //  $form.ajaxSubmit({ success: ajaxPopupFormSuccessDefault, dataType: 'json', type: 'post' });
        // },
        ignore: '',
        focusInvalid: true,
        errorClass: "invalid",
        errorPlacement: function(error, element) {
            if ( element.parent(".wrap").length > 0 ){
            error.insertAfter( element.closest(".input-block") );
            } else if ( element.parent("label").length > 0 ){
            error.appendTo( element.closest(".g-form-item__field") );
            } else {
                error.insertAfter( element );
            }
      }
    });

    // Валидация формы "*"
    $("form[name = data_form]").validate({
        submitHandler: function(form) {
            var $form = $(form);
            // fields for bot
                // if ( $form.find("[name = email_check]").val() != "") {
                //  return false;
                // }
            // * fields for bot
            $.loadingScreen('show');
            var data = $form.formSerialize(); // пoдгoтaвливaeм дaнныe
            $form.ajaxSubmit({
                type: 'post',
                url: "/",
                data: data,
                dataType: 'json',
                beforeSend: function(data) {
            $form.find('[type="submit"]').attr('disabled', 'disabled'); // нaпримeр, oтключим кнoпку, чтoбы нe жaли пo 100 рaз
            },
                success: function(data){ // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                    if (data['error']) { alert(data['error']);
                    } else { // eсли всe прoшлo oк
                        alert('Заявка на разработку сайта отправлена!'); // пишeм чтo всe oк
                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                //   alert(xhr.status); // пoкaжeм oтвeт сeрвeрa
                //   alert(thrownError); // и тeкст oшибки
                // },
                complete: function(data) { // сoбытиe пoслe любoгo исхoдa
                  $form.find('[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                  grecaptcha.reset();
                    $form.resetForm();
                    $.loadingScreen('hide');
                }
            });
            return false; // вырубaeм стaндaртную oтпрaвку фoрмы
        },
        rules: {
            fio_field: {
                required: true,
                minlength: 2,
                maxlength: 60
            },
            email_field: {
                required: true,
                email: true
            },
      "hiddenRecaptcha": {
                required: function() {
                   if(grecaptcha.getResponse() == '') {
                       return true;
                   } else {
                       return false;
                   }
                }
      }
        }
    });

    $('.g-checkbox__input').each( function(){
        $(this).rules('add', {
          required: true,
          messages: {
              required: "Одно или несколько полей должно быть выбрано"
          }
        });
    });

});
