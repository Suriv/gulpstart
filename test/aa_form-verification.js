/*
 * aa-form-verification.js
 *
 * Верификация форм
 *
 * зависимости:
 *    /js/jquery.validate.js
 *    /js/localization/messages_ru.min.js
 *
 */

///////////////
// reCAPTCHA //
///////////////
var recaptchaResultFeedback = '';
var verifyCallbackFeedback = function(response) {
    recaptchaResultFeedback = response;
}
var onloadCallback = function() {
    if ($("div").is("#recaptcha_feedback")) {
        grecaptcha.render('recaptcha_feedback', {
            'sitekey' : '6LdANEEUAAAAAI7w0mbvw7ozeysaSLPymA2Bkj9v',
            callback: verifyCallbackFeedback
        });
    }
    // grecaptcha.render('recaptcha2', {
    //  'sitekey' : mysitekey,
    //  'theme' : 'dark', //default - light
    //  'type' : 'audio', //default - image
    //  'size' : 'compact', //default - normal
    //  'tabindex' : 1, //default - 0
    //  'callback' : , //function on success
    //  'expired-callback' : //function when response expires
    // });
};

$(function(){
    // Привязка к кнопкам отправки форм, у которых нет собственного обработчика
    // onclick() стандартного обработчика плагина jquery.form
    // $("input[type='submit']:not([onclick]), button[type='submit']:not([onclick])").on("click", function(){
    //  $(this).closest('form').submit();
    //  return false;
    // });

    // Установка дефолтовых значений валидатора
    $.validator.setDefaults({
        // debug: true,
        ignore: [],
        focusInvalid: true,
        errorPlacement: function(error, element) {
            if ( element.parent(".g-form-item__field").length > 0 ){
                error.appendTo( element.parent(".g-form-item__field") );
            } else if ( element.parent("label").length > 0 ){
                error.appendTo( element.closest(".g-form-item__field") );
            } else {
                error.insertAfter( element );
            }
        }
    });

    // метод для года с маской
    $.validator.addMethod("required_mask_date", function (value, element) {
        var arr = value.split("."),
                year = new Date().getFullYear();
            if(year === "" || year == "undefined" ||  year == "null" ){
                return true;
            } else if( (year + 1) - arr[2] < 0  || arr[2] - (year - 1) < 0 ){
                return false;
            }
            return true;
    },
    "Заполните год правильно");
    //** метод для года с маской **/
    //
    //// метод для телефона с маской
    $.validator.addMethod("required_mask_phone", function (value, element) {
        if (value != "") {
            return value.replace(/\D+/g, '').length == element.getAttribute("data-mask-phone").replace(/\D+/g, '').length;
        } else return true;
    },
    "Заполните это поле полностью");
    //** метод для телефона с маской **/


    // Валидация формы "see"
    $(".js-form_see").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true,
                required_mask_phone: true
            },
            mail: {
                required: true,
                email: true
            },
            one_from_group: {
                require_from_group: [1, ".js-form_feedback-group"],
                required_phone_mask: true
            },
            one_from_group: {
                require_from_group: [1, ".js-form_feedback-group"],
                email: true
            },
            form_agree: {
                required: true
            },
            "hiddenRecaptcha": {
                required: function() {
                     if(grecaptcha.getResponse() == '') {
                             return true;
                     } else {
                             return false;
                     }
                }
            }
        }
    });

    // welldone message
    $(".js-welldone__btn").on("click", function (e) {
        e.preventDefault;
        var $this = $(this),
                $form = $this.closest("form"),
                $modal = $this.closest(".modal"),
                $header = $modal.find(".modal-header"),
                $body = $modal.find(".modal-body"),
                $answer = $modal.find(".modal-answer"),
                data = {};
        // validation
        if($form.valid()){
            $header.hide();
            $body.hide();
            $answer.show();
            // set data
            data.name = $form.find('input[name=name]').val();
            data.email = $form.find('input[name=email]').val();
            data.phone = $form.find('input[name=phone]').val();
            data.recaptcha = recaptchaResultFeedback;
            // send ajax
            $.ajax({
                url: "/",
                method: "post",
                data: data,
                dataType: "json"
            }).then(function (response) {
                if (response.status == true) {
                    setTimeout(function(){
                        $modal.modal('hide');
                    }, 2500);
                } else {
                    alert(response.error);
                }
            });
            $modal.on("hidden.bs.modal", function(){
                $header.show();
                $body.show();
                $answer.hide();
                grecaptcha.reset();
                $form[0].reset();
            });
            return false;
        }
    });// /*  welldone message

});
